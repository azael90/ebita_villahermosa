package com.latinofuneral.bitacoras2020Villahermosa.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.latinofuneral.bitacoras2020Villahermosa.Database.EmbalsamadoEvents;
import com.latinofuneral.bitacoras2020Villahermosa.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AdapterEmbalsamado extends RecyclerView.Adapter<AdapterEmbalsamado.ProductViewHolder> {
    private static final String TAG = "AdapterBitacorasActivas";
    private Context mCtx;
    private List<EmbalsamadoEvents> embalsamadoEventsList;

    public AdapterEmbalsamado(Context mCtx, List<EmbalsamadoEvents> embalsamadoEventsList) {
        this.mCtx = mCtx;
        this.embalsamadoEventsList = embalsamadoEventsList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.item_embalsamado_layout, null);
        ProductViewHolder holder = new ProductViewHolder(view);
        return holder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final ProductViewHolder holder, final int position)
    {

        if(position % 2 == 0) {
            holder.contentLayout.setBackgroundColor(Color.parseColor("#efefef"));
        }
        else {
            holder.contentLayout.setBackgroundColor(Color.parseColor("#ffffff"));//#ffffff
        }

        //holder.tvNumeroBitacora.setTypeface(ApplicationResourcesProvider.bold);
        holder.tvNumeroBitacora.setText(embalsamadoEventsList.get(position).getBitacora());
        holder.tvEvent.setText(embalsamadoEventsList.get(position).getNombreevento());
        try {
            final String OLD_FORMAT = "yyyy-MM-dd HH:mm:ss";
            final String NEW_FORMAT = "HH:mm dd/MM/yy";
            String newDateString;

            SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
            Date d = sdf.parse(embalsamadoEventsList.get(position).getFechaevento());
            sdf.applyPattern(NEW_FORMAT);
            newDateString = sdf.format(d);

            holder.tvDate.setText(newDateString);
        } catch (ParseException e) {
            holder.tvDate.setText("");
        }

    }





    @Override
    public int getItemCount() {
        return embalsamadoEventsList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvNumeroBitacora, tvEvent, tvDate;
        LinearLayout contentLayout;

        public ProductViewHolder(View itemView)
        {
            super(itemView);
            contentLayout= itemView.findViewById(R.id.contentLayout);
            tvNumeroBitacora= itemView.findViewById(R.id.tvNumeroBitacora);
            tvEvent= itemView.findViewById(R.id.tvEvent);
            tvDate= itemView.findViewById(R.id.tvDate);

        }
    }


}
