package com.latinofuneral.bitacoras2020Villahermosa.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.viewpager.widget.PagerAdapter;

import com.latinofuneral.bitacoras2020Villahermosa.R;

/**
 * Created by luki2 on 09/04/2018.
 */

public class SliderAdapter extends PagerAdapter {


    Context context;
    LayoutInflater layoutInflater;

    public SliderAdapter(Context context)
    {
        this.context=context;
    }

    //Arrays

    public int[] slide_images= {

            R.drawable.action_done,
            R.drawable.ic_add_black,
            R.drawable.ic_arrow_right
    };

    public String[] slide_headings ={
            "TU ASISTENTE DE BELLEZA Y SERVICIOS WELLNESS",
            "CUALQUIER SERVICIO",
            "SERVICIOS PREMIUM"
    };

    public String[] slide_desc ={
            "Pide cita, recibe recordatorios y contacta a tus clientes.",
            "Ofrece cualquier servicio que realices y llega a mas lugares y clientes",
            "Con más y mejores servicios de calidad para tus clientes"
    };

    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (RelativeLayout) object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);


        ImageView slideImageView = (ImageView) view.findViewById(R.id.slide_image);
        TextView slideHeading = (TextView) view.findViewById(R.id.slide_heading);
        TextView slideDescription=(TextView)view.findViewById(R.id.slide_desc);

        slideImageView.setImageResource(slide_images[position]);
        slideHeading.setText(slide_headings[position]);
        slideDescription.setText(slide_desc[position]);

        container.addView(view);

    return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout)object);
    }
}
