package com.latinofuneral.bitacoras2020Villahermosa.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.latinofuneral.bitacoras2020Villahermosa.Models.ModelFotografiasTomadas;
import com.latinofuneral.bitacoras2020Villahermosa.R;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

public class AdapterFotografiasTomadas extends RecyclerView.Adapter<AdapterFotografiasTomadas.ProductViewHolder> {
    private static final String TAG = "AdapterDocumentos";
    private Context mCtx;
    private List<ModelFotografiasTomadas> productList;


    public AdapterFotografiasTomadas(Context mCtx, List<ModelFotografiasTomadas> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.post_item_container, null);
        ProductViewHolder holder = new ProductViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, final int position)
    {
        final ModelFotografiasTomadas product= productList.get(position);

        /*if(position % 2 == 0) {
            holder.viewLateral.setBackgroundColor(Color.parseColor("#dcb467"));
        }
        else {
            holder.viewLateral.setBackgroundColor(Color.parseColor("#a63fff"));
        }*/

        //holder.tvNumeroDeDocumento.setText(String.valueOf(position + 1));

        holder.imagePost.setImageBitmap(product.getFoto());
    }





    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder
    {
        RoundedImageView imagePost;

        public ProductViewHolder(View itemView)
        {
            super(itemView);
            imagePost= itemView.findViewById(R.id.imagePost);
        }
    }
}
