package com.latinofuneral.bitacoras2020Villahermosa.Adapters;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.latinofuneral.bitacoras2020Villahermosa.Callbacks.CancelarArticuloRecoleccion;
import com.latinofuneral.bitacoras2020Villahermosa.Database.EquipoRecoleccion;
import com.latinofuneral.bitacoras2020Villahermosa.R;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.ApplicationResourcesProvider;

import java.util.List;

public class AdapterEquiposRecoleccion extends RecyclerView.Adapter<AdapterEquiposRecoleccion.ProductViewHolder> {
    private static final String TAG = "AdapterDocumentos";
    private Context mCtx;
    private List<EquipoRecoleccion> productList;
    private CancelarArticuloRecoleccion cancelarArticuloRecoleccion;


    public AdapterEquiposRecoleccion(Context mCtx, List<EquipoRecoleccion> productList, CancelarArticuloRecoleccion cancelarArticuloRecoleccion) {
        this.mCtx = mCtx;
        this.productList = productList;
        this.cancelarArticuloRecoleccion = cancelarArticuloRecoleccion;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.item_documento_extra, null);
        ProductViewHolder holder = new ProductViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, final int position)
    {
        final EquipoRecoleccion product= productList.get(position);

        /*if(position % 2 == 0) {
            holder.viewLateral.setBackgroundColor(Color.parseColor("#dcb467"));
        }
        else {
            holder.viewLateral.setBackgroundColor(Color.parseColor("#a63fff"));
        }*/

        holder.tvNombre.setTypeface(ApplicationResourcesProvider.bold);
        holder.tvSerie.setTypeface(ApplicationResourcesProvider.regular);
        holder.tvEntradaSalida.setTypeface(ApplicationResourcesProvider.light);


        holder.tvNumeroDeDocumento.setText(String.valueOf(position + 1));
        holder.tvNombre.setText(product.getNombre());
        holder.tvSerie.setText(product.getSerie());
        holder.btBorrarArticulo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelarArticuloRecoleccion.onClickCancelarArticuloRecoleccion(position, "" + product.getSerie(), "" + product.getFechacaptura(), product.getBitacora());
            }
        });

        holder.layoutGeneral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelarArticuloRecoleccion.onClickCancelarArticuloRecoleccion(position, "" + product.getSerie(), "" + product.getFechacaptura(), product.getBitacora());
            }
        });

        /*if(product.getSerie().contains("CL") ||product.getSerie().contains("CB") )
            holder.tvEntradaSalida.setVisibility(View.VISIBLE);
        else
            holder.tvEntradaSalida.setVisibility(View.GONE);*/

        holder.tvEntradaSalida.setVisibility(View.VISIBLE);

        if (product.getTipo().equals("0")){
            holder.tvEntradaSalida.setTextColor(Color.parseColor("#9a0007"));
            holder.tvEntradaSalida.setText("Salida de inventario");
        }
        else if(product.getTipo().equals("1")) {
            holder.tvEntradaSalida.setText("Entrada de inventario");
            holder.tvEntradaSalida.setTextColor(Color.parseColor("#ff669900"));
        }
    }





    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvNumeroDeDocumento, tvNombre, tvSerie, tvEntradaSalida;
        ImageButton btBorrarArticulo;
        LinearLayout layoutGeneral;
        public ProductViewHolder(View itemView)
        {
            super(itemView);
            tvNumeroDeDocumento= itemView.findViewById(R.id.tvNumeroDeDocumento);
            tvNombre = itemView.findViewById(R.id.tvNombre);
            tvSerie = itemView.findViewById(R.id.tvSerie);
            layoutGeneral = itemView.findViewById(R.id.layoutGeneral);
            btBorrarArticulo = itemView.findViewById(R.id.btBorrarArticulo);
            tvEntradaSalida = itemView.findViewById(R.id.tvEntradaSalida);
        }
    }


}
