package com.latinofuneral.bitacoras2020Villahermosa.Callbacks;

public interface CancelarArticuloCortejo {
    public void onClickCancelarArticuloCortejo(int position, String serie, String fecha, String bitacora);
}
