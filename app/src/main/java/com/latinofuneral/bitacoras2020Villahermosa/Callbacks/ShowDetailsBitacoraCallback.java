package com.latinofuneral.bitacoras2020Villahermosa.Callbacks;

public interface ShowDetailsBitacoraCallback {
    public void onClickShowDetails(int position, String bitacora);
}
