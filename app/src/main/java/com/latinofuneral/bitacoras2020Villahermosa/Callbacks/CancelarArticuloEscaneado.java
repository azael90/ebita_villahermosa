package com.latinofuneral.bitacoras2020Villahermosa.Callbacks;

public interface CancelarArticuloEscaneado {
    public void onClickCancelarArticulo(int position, String id);
}
