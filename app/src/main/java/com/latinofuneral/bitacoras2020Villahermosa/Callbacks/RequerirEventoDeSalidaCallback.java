package com.latinofuneral.bitacoras2020Villahermosa.Callbacks;

public interface RequerirEventoDeSalidaCallback {
    public void onRequeriedEventoSalida(int position, String bitacora);
}
