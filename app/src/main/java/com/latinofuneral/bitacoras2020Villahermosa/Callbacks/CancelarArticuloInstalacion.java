package com.latinofuneral.bitacoras2020Villahermosa.Callbacks;

public interface CancelarArticuloInstalacion {
    public void onClickCancelarArticuloInstalacion(int position, String serie, String fecha, String bitacora);
}
