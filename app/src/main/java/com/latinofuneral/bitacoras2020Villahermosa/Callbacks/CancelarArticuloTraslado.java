package com.latinofuneral.bitacoras2020Villahermosa.Callbacks;

public interface CancelarArticuloTraslado {
    public void onClickCancelarArticuloTraslado(int position, String serie, String fecha, String bitacora);
}
