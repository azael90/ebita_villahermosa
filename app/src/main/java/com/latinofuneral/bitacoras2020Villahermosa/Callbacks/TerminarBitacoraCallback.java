package com.latinofuneral.bitacoras2020Villahermosa.Callbacks;

public interface TerminarBitacoraCallback {
    public void onClickTerminarBitacora(int position, String bitacora);
}
