package com.latinofuneral.bitacoras2020Villahermosa.Callbacks;

public interface CancelarArticuloRecoleccion {
    public void onClickCancelarArticuloRecoleccion(int position, String serie, String fecha, String bitacora);
}
