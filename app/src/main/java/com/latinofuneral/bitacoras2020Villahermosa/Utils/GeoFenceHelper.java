package com.latinofuneral.bitacoras2020Villahermosa.Utils;

import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.maps.model.LatLng;

public class GeoFenceHelper extends ContextWrapper
{
    private static final String TAG="GeoFenceHelper";
    PendingIntent pendingIntent;

    public GeoFenceHelper(Context base)
    {
        super(base);
    }

    public GeofencingRequest getGeoFencingRequest(Geofence geofence){
        Log.i(TAG, "getGeoFencingRequest: CREADA: "+ geofence);
        return new GeofencingRequest.Builder()
                .addGeofence(geofence)
                .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
                .build();

    }

    public Geofence getGeoFence(String ID, LatLng latLng, float radius, int transitionsTrypes){
        return new Geofence.Builder()
                .setCircularRegion(latLng.latitude, latLng.longitude, radius)
                .setRequestId(ID)
                .setTransitionTypes(transitionsTrypes)
                .setLoiteringDelay(5000)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .build();
    }

    //public PendingIntent pendingIntent;
    public PendingIntent getPendingIntent() {
        //if(pendingIntent != null){
        //  return pendingIntent;
        //}
        Intent intent =  new Intent(this, GeofenceBroadcastReceiver.class);
        //intent.setAction(Intent.ACTION_DEFAULT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            try {
                return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_IMMUTABLE);
            }catch (Throwable e){
                Log.e("TAG", "getPendingIntent: " );
                return PendingIntent.getBroadcast(this, 0, intent,  PendingIntent.FLAG_MUTABLE);
            }
        } else {
            return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        //return pendingIntent;
    }


    public String getErrorString(Exception e){
        if(e instanceof ApiException){
            ApiException apiException = (ApiException) e;
            switch (apiException.getStatusCode()){
                case GeofenceStatusCodes
                        .GEOFENCE_NOT_AVAILABLE:
                    return "GEOFENCE NO DISPONIBLE";

                case GeofenceStatusCodes
                        .GEOFENCE_TOO_MANY_GEOFENCES:
                    return "MUCHAS GEOFENCE ACTIVAS";

                case GeofenceStatusCodes
                        .GEOFENCE_TOO_MANY_PENDING_INTENTS:
                    return "GEOFENCE PENDIENTE";
            }
        }
        return e.getLocalizedMessage();
    }
}
