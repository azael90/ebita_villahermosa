 package com.latinofuneral.bitacoras2020Villahermosa.Utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.FirebaseApp;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Adicional;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Asistencia;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Cancelados;
import com.latinofuneral.bitacoras2020Villahermosa.Database.CatalogoArticulos;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Codigos;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Comentarios;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Cremacionevents;
import com.latinofuneral.bitacoras2020Villahermosa.Database.DatabaseAssistant;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Descansos;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Documentos;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Ediciones;
import com.latinofuneral.bitacoras2020Villahermosa.Database.EmbalsamadoEvents;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Encuesta;
import com.latinofuneral.bitacoras2020Villahermosa.Database.EquipoRecoleccion;
import com.latinofuneral.bitacoras2020Villahermosa.Database.EquipoTraslado;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Equipocortejo;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Equipoinstalacion;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Eventos;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Fotografias;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Inventario;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Laboratorios;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Movimientos;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Sesiones;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Ubicaciones;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.latinofuneral.bitacoras2020Villahermosa.R;
import com.orm.SugarRecord;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import www.sanju.motiontoast.MotionToast;
import www.sanju.motiontoast.MotionToastStyle;


 public class ApplicationResourcesProvider extends com.orm.SugarApp {
    private static final String TAG = "ApplicationResourceProvider";
    private static Context sContext;
    ApplicationResourcesProvider instance;
    private static Date date;
    private static String timeLocation = "0";
    public static double latitud = 0, longitud = 0;
    public static LocationRequest locationRequest;
    public static FusedLocationProviderClient fusedLocationProviderClient;
    public static Typeface bold, light, regular;


    public ApplicationResourcesProvider getInstance() {
        return instance;
    }


    @SuppressLint("LongLogTag")
    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
        instance = this;
        Preferences.setGeofenceActual(sContext, "", Preferences.PREFERENCE_GEOFENCE_ACTUAL);


        bold = Typeface.createFromAsset(getAssets(), "fonts/bold.ttf");
        light = Typeface.createFromAsset(getAssets(), "fonts/light.ttf");
        regular = Typeface.createFromAsset(getAssets(), "fonts/regular.ttf");


        try {
            FirebaseApp.initializeApp(sContext);
        }catch (Throwable e){
            Log.e(TAG, "onCreate: adasda");
        }

        //timerForInsertLocations();
        //timerForSyncData();
        timerCallLocations();
        //timerRefreshLocation();
        updateLocation();

        //if(checkInternetConnection())
        //  createJsonForSync();

        Preferences.setWithinTheZone(sContext, false, Preferences.PREFERENCE_WITHIN_THE_ZONE_TO_LOGIN);
    }

    /*@SuppressLint("LongLogTag")
    private void downloadCatalogoArticulos()
    {
        try {
            String token = FirebaseInstanceId.getInstance().getToken();
            Log.d("FIREBASE", "Refresh Token: " + token);
            if (token != null)
                DatabaseAssistant.insertarToken(token);
            else
                DatabaseAssistant.insertarToken("Unknown");
        }catch (Throwable e){
            Log.e(TAG, "downloadBitacoras: " + e.getMessage());
        }

        JSONObject params = new JSONObject();
        try {
            params.put("usuario", DatabaseAssistant.getUserNameFromSesiones() );
            params.put("token_device", DatabaseAssistant.getTokenDeUsuario());
            params.put("isProveedor", DatabaseAssistant.getIsProveedor());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_DOWNLOAD_ARTICULOS_CATALOGO, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                JSONArray bitacorasArray = new JSONArray();

                try
                {
                    bitacorasArray = response.getJSONArray("catalogo");

                    if(bitacorasArray.length()>0){
                        CatalogoArticulos.deleteAll(CatalogoArticulos.class);
                        for (int i = 0; i <= bitacorasArray.length() - 1; i++)
                        {
                            DatabaseAssistant.insertarCatalogoArticulos(
                                    bitacorasArray.getJSONObject(i).getString("name"),
                                    bitacorasArray.getJSONObject(i).getString("name")
                            );
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "onResponse: Error en descarga de catalogo de articulos: " + e.getMessage());
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e(TAG, "onResponse: Error en descarga de catalogo de articulos: " + error.getMessage());
                    }
                }) {

        };
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
    }*/

    public static Context getContext() {
        return sContext;
    }


    /*@SuppressLint("LongLogTag")
    public static boolean createJsonForSync() {
        Toast.makeText(sContext, "createJsonForSync Inicial()", Toast.LENGTH_SHORT).show();
        boolean falloAlgunRequestDeSincronizacion = false;
        try {
            @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(sContext.getContentResolver(), Settings.Secure.ANDROID_ID);
            String query="SELECT * FROM EVENTOS where estatus = '0' ORDER BY id ASC";
            List<Eventos> listaMovimientos = Eventos.findWithQuery(Eventos.class, query);
            if(listaMovimientos.size()>0)
            {
                JSONObject jsonGeneralSalidasYLlegadas = new JSONObject();
                JSONArray jsonArraySalidasYLlegadas = new JSONArray();
                for (int i = 0; i <= listaMovimientos.size() - 1; i++)
                {
                    try
                    {
                        JSONObject jsonEventos = new JSONObject();
                        jsonEventos.put("s", "registrarSalida");
                        jsonEventos.put("evento_id", listaMovimientos.get(i).getId());
                        jsonEventos.put("fecha", listaMovimientos.get(i).getFecha());
                        jsonEventos.put("hora", listaMovimientos.get(i).getHora());
                        jsonEventos.put("bitacora", listaMovimientos.get(i).getBitacora());
                        jsonEventos.put("id_chofer", DatabaseAssistant.getidFromNombreChofer(listaMovimientos.get(i).getChofer()));
                        jsonEventos.put("id_ayudante",  DatabaseAssistant.getidFromNombreChofer(listaMovimientos.get(i).getAyudante()));
                        jsonEventos.put("id_lugar",  DatabaseAssistant.getidFromNombreLugar(listaMovimientos.get(i).getLugar()));
                        jsonEventos.put("id_lugar_destino",  DatabaseAssistant.getidFromNombreLugar(listaMovimientos.get(i).getDestino()));
                        jsonEventos.put("id_vehiculo",  DatabaseAssistant.getidFromNombreCarro(listaMovimientos.get(i).getCarro()));
                        jsonEventos.put("dispositivo", listaMovimientos.get(i).getDispositivo());
                        jsonEventos.put("lat", listaMovimientos.get(i).getLatitud());
                        jsonEventos.put("lng", listaMovimientos.get(i).getLongitud());
                        jsonEventos.put("evento", listaMovimientos.get(i).getTipo());
                        jsonEventos.put("automatico", listaMovimientos.get(i).getAutomatico());


                        jsonEventos.put("nombre_chofer", listaMovimientos.get(i).getChofer());
                        jsonEventos.put("nombre_ayudante", listaMovimientos.get(i).getAyudante());
                        jsonEventos.put("nombre_lugar", listaMovimientos.get(i).getLugar());
                        jsonEventos.put("nombre_destino", listaMovimientos.get(i).getDestino());
                        jsonEventos.put("nombre_vehiculo", listaMovimientos.get(i).getCarro());
                        jsonEventos.put("token_device", DatabaseAssistant.getTokenDeUsuario());

                        jsonEventos.put("tipo_movimiento", listaMovimientos.get(i).getMovimiento());
                        jsonEventos.put("usuario", listaMovimientos.get(i).getUsuario());
                        jsonEventos.put("checkInDriver", listaMovimientos.get(i).getCheckInDriver());
                        jsonEventos.put("isProveedor", Preferences.getIsProveedor(sContext, Preferences.PREFERENCE_IS_PROVEEDOR) ? "1" : "0");

                        jsonArraySalidasYLlegadas.put(jsonEventos);
                        /*
                        try{
                            List<Codigos> listaCodigos = Codigos.findWithQuery(Codigos.class, "SELECT * FROM CODIGOS where bitacora = '"+ listaMovimientos.get(i).getBitacora() +"'");
                            if(listaCodigos.size()>0)
                            {
                                for(int y=0; y<=listaCodigos.size()-1; y++) {
                                    JSONObject jsonCodigos = new JSONObject();
                                    jsonCodigos.put("evento_id_barras", listaCodigos.get(y).getId());
                                    jsonCodigos.put("name", listaCodigos.get(y).getCodigobarras());
                                    jsonCodigos.put("id", listaCodigos.get(y).getId());
                                    jsonActivos.put(jsonCodigos);
                                }
                                jsonEventos.put("activos", jsonActivos);
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }



                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }

                }
                jsonGeneralSalidasYLlegadas.put("salidas_and_llegadas", jsonArraySalidasYLlegadas);
                if(checkInternetConnection())
                    cargaDeDatosAServidor(jsonGeneralSalidasYLlegadas);
            }
            else
                Log.i(TAG, "NO hay registros de Eventos");

*/

           /* List<Descansos> descansosList = Descansos.findWithQuery(Descansos.class, "SELECT * FROM DESCANSOS where sync = '0'");
            if (descansosList.size() > 0) {
                JSONObject cremacion_eventos = new JSONObject();
                JSONArray cremacionesArray = new JSONArray();
                for (int i = 0; i <= descansosList.size() - 1; i++) {
                    try {
                        JSONObject jsonEventoCremacion = new JSONObject();
                        jsonEventoCremacion.put("usuario", descansosList.get(i).getUsuario());
                        jsonEventoCremacion.put("concepto", descansosList.get(i).getConcepto());
                        jsonEventoCremacion.put("fecha_captura", descansosList.get(i).getFecha());
                        jsonEventoCremacion.put("tipo_evento", descansosList.get(i).getTipo());
                        jsonEventoCremacion.put("latitud", descansosList.get(i).getLatitud());
                        jsonEventoCremacion.put("longitud", descansosList.get(i).getLongitud());
                        jsonEventoCremacion.put("isBunker", descansosList.get(i).getIsBunker());
                        jsonEventoCremacion.put("geofence_actual", descansosList.get(i).getGeofenceActual());
                        cremacionesArray.put(jsonEventoCremacion);
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                        falloAlgunRequestDeSincronizacion = true;
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar Descansos:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
                cremacion_eventos.put("descansos_eventos", cremacionesArray);
                if(checkInternetConnection())
                    cargaDeEventosDeDescansos(cremacion_eventos);
            } else
                Log.i(TAG, "NO hay registros de Descansos");


            cargarImagenes();


            //*********************************** JSON ENCUESTAS ***************************************
            String queryEncuestas = "SELECT * FROM ENCUESTA where sync='0'";
            List<Encuesta> encuestaList = Encuesta.findWithQuery(Encuesta.class, queryEncuestas);
            if (encuestaList.size() > 0) {
                JSONObject encuestaJSON = new JSONObject();
                JSONArray preguntasArray = new JSONArray();

                for (int i = 0; i <= encuestaList.size() - 1; i++) {
                    try {
                        JSONObject pregunta = new JSONObject();
                        pregunta.put("bitacora", encuestaList.get(i).getBitacora());
                        pregunta.put("encuesta", encuestaList.get(i).getEncuesta());
                        pregunta.put("fecha_captura", encuestaList.get(i).getFecha());
                        pregunta.put("tipo_encuesta", encuestaList.get(i).getLugar());
                        pregunta.put("nombre_familiar", encuestaList.get(i).getNombrefamiliar());
                        pregunta.put("parentesco", encuestaList.get(i).getParentesco());
                        pregunta.put("usuario", encuestaList.get(i).getUsuario());
                        pregunta.put("latitud", encuestaList.get(i).getLatitud());
                        pregunta.put("longitud", encuestaList.get(i).getLongitud());
                        preguntasArray.put(pregunta);
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar ENCUESTA:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
                        falloAlgunRequestDeSincronizacion = true;
                    }
                }

                encuestaJSON.put("encuestas", preguntasArray);
                cargaDeEncuestas(encuestaJSON);
            } else
                Log.i(TAG, "NO hay registros de Encuestas");
            //***********************


            //*********************************** JSON CREMACIONES EVENTOS ***************************************
            String queryEventosCremacion = "SELECT * FROM CREMACIONEVENTS where sync='0'";
            List<Cremacionevents> cremacioneventsList = Cremacionevents.findWithQuery(Cremacionevents.class, queryEventosCremacion);
            if (cremacioneventsList.size() > 0) {
                JSONObject cremacion_eventos = new JSONObject();
                JSONArray cremacionesArray = new JSONArray();
                for (int i = 0; i <= cremacioneventsList.size() - 1; i++) {
                    try {
                        JSONObject jsonEventoCremacion = new JSONObject();
                        jsonEventoCremacion.put("bitacora", cremacioneventsList.get(i).getBitacora());
                        jsonEventoCremacion.put("tipo_evento", cremacioneventsList.get(i).getTipoevento());
                        jsonEventoCremacion.put("fecha_evento", cremacioneventsList.get(i).getFechaevento());
                        jsonEventoCremacion.put("usuario", cremacioneventsList.get(i).getUsuario());
                        jsonEventoCremacion.put("quien_recibe_cenizas", cremacioneventsList.get(i).getRecibecenizas());
                        cremacionesArray.put(jsonEventoCremacion);
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                        falloAlgunRequestDeSincronizacion = true;
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar CREMACIONEVENTS:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
                    }

                }
                cremacion_eventos.put("cremacion_eventos", cremacionesArray);
                cargaDeEventosDeCremacion(cremacion_eventos);
            } else
                Log.i(TAG, "NO hay registros de Ubicaciones");
            //***********************

//*********************************** JSON Embalsamado EVENTOS ***************************************
            String queryEventosEmbalsamado = "SELECT * FROM EMBALSAMADO_EVENTS where sync='0'";
            List<EmbalsamadoEvents> embalsamadoEvents = EmbalsamadoEvents.findWithQuery(EmbalsamadoEvents.class, queryEventosEmbalsamado);
            if (embalsamadoEvents.size() > 0) {
                JSONObject emb_eventos = new JSONObject();
                JSONArray embArray = new JSONArray();
                for (int i = 0; i <= embalsamadoEvents.size() - 1; i++) {
                    try {
                        JSONObject jsonEventoEmb = new JSONObject();
                        jsonEventoEmb.put("bitacora", embalsamadoEvents.get(i).getBitacora());
                        jsonEventoEmb.put("tipo_evento", embalsamadoEvents.get(i).getTipoevento());
                        jsonEventoEmb.put("fecha_evento", embalsamadoEvents.get(i).getFechaevento());
                        jsonEventoEmb.put("usuario", embalsamadoEvents.get(i).getUsuario());
                        jsonEventoEmb.put("quien_recibe_cenizas", embalsamadoEvents.get(i).getRecibecenizas());
                        embArray.put(jsonEventoEmb);
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar EMBALSAMADO_EVENTS:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
                    }

                }
                emb_eventos.put("recoleccion_proveedor_eventos", embArray);
                if(checkInternetConnection())
                    cargaDeEventosDeEmbalsamado(emb_eventos);
            } else
                Log.i(TAG, "NO hay registros de Ubicaciones");
            //***********************

            //****************************************EDICIONES************************************************


            List<Ediciones> edicionesList = Ediciones.findWithQuery(Ediciones.class, "SELECT * FROM EDICIONES WHERE sync = '0'");
            if (edicionesList.size() > 0) {
                JSONArray jsonArrayEdiciones = new JSONArray();
                JSONObject jsonGeneralParaEdiciones = new JSONObject();

                for (int i = 0; i <= edicionesList.size() - 1; i++) {
                    try {
                        JSONObject jsonParams = new JSONObject();
                        jsonParams.put("bitacora", edicionesList.get(i).getBitacora());
                        jsonParams.put("fecha_evento", edicionesList.get(i).getFechaevento());
                        jsonParams.put("fecha_captura", edicionesList.get(i).getFechacaptura());
                        jsonParams.put("descripcion", edicionesList.get(i).getDescripcion());
                        jsonParams.put("id_lugar", DatabaseAssistant.getidFromNombreLugar(edicionesList.get(i).getDescripcion()));
                        jsonArrayEdiciones.put(jsonParams);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "onResponse: Ocurrio un error al sincronizar las edicones: " + e.getMessage());
                        falloAlgunRequestDeSincronizacion = true;
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar EDICIONES:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
                jsonGeneralParaEdiciones.put("Ediciones_array", jsonArrayEdiciones);
                requestToSyncEdicionesDeDestinosParaBitacoras(jsonGeneralParaEdiciones);
            } else
                Log.i(TAG, "NO hay registros de ediciones");

            //******************************************************************************************

            //******** sesiones ******

            List<Sesiones> listaSesiones = Sesiones.findWithQuery(Sesiones.class, "SELECT * FROM SESIONES WHERE statos = '0' ORDER BY fecha ASC");
            if (listaSesiones.size() > 0) {
                Log.d(TAG, "createJsonForSync: Comienza creación de JSON SESIONES");
                for (int i = 0; i <= listaSesiones.size() - 1; i++) {
                    try {
                        JSONObject jsonParams = new JSONObject();
                        jsonParams.put("id_sync", listaSesiones.get(i).getId());
                        jsonParams.put("usuario", listaSesiones.get(i).getUsuario());
                        jsonParams.put("pass", listaSesiones.get(i).getContrasena());
                        jsonParams.put("fecha", listaSesiones.get(i).getFecha());
                        jsonParams.put("hora", listaSesiones.get(i).getHora());
                        jsonParams.put("lat", listaSesiones.get(i).getLatitud());
                        jsonParams.put("lng", listaSesiones.get(i).getLongitud());
                        jsonParams.put("tipo", listaSesiones.get(i).getEstatus());
                        jsonParams.put("isBunker", listaSesiones.get(i).getIsBunker());
                        jsonParams.put("geoFenceActual", listaSesiones.get(i).getGeofence());
                        jsonParams.put("token_device", DatabaseAssistant.getTokenDeUsuario());
                        jsonParams.put("isProveedor", Preferences.getIsProveedor(sContext, Preferences.PREFERENCE_IS_PROVEEDOR) ? "1" : "0");
                        requestLoginOnline(jsonParams);
                        //Log.i("SESIONES", "createJsonForSync: json de sesiones: " + jsonParams.toString());
                    } catch (Exception e) {
                        Log.e("SESIONES", "createJsonForSync: Error en JSON de SESIONES: " + e.getMessage());
                        e.printStackTrace();
                        falloAlgunRequestDeSincronizacion = true;
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar SESIONES:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            } else
                Log.i(TAG, "NO hay registros de Sesiones");


            //*********************** Comentarios ********
            List<Comentarios> articuloscanList = Comentarios.findWithQuery(Comentarios.class, "SELECT * FROM COMENTARIOS WHERE sync='0'");
            if (articuloscanList.size() > 0) {
                JSONArray jsonComentarioIndividual = new JSONArray();
                JSONObject jsonGeneralParaComentarios = new JSONObject();
                for (int x = 0; x <= articuloscanList.size() - 1; x++) {
                    try {
                        JSONObject jsonComentario = new JSONObject();
                        jsonComentario.put("bitacora", articuloscanList.get(x).getBitacora());
                        jsonComentario.put("comentario", articuloscanList.get(x).getComentario());
                        jsonComentario.put("usuario", articuloscanList.get(x).getUsuario());
                        jsonComentario.put("fecha_captura", articuloscanList.get(x).getFecha());
                        jsonComentarioIndividual.put(jsonComentario);
                    } catch (Exception e) {
                        e.printStackTrace();
                        falloAlgunRequestDeSincronizacion = true;
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar COMENTARIOS:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
                jsonGeneralParaComentarios.put("Comentarios_Array", jsonComentarioIndividual);
                requestSaveCommentariosToServer(jsonGeneralParaComentarios);
            } else
                Log.i(TAG, "NO hay registros de Comentarios");
            //****************************


            //******** sesiones ******

            List<Asistencia> asistenciaList = Asistencia.findWithQuery(Asistencia.class, "SELECT * FROM ASISTENCIA WHERE statos = '0'");
            if (asistenciaList.size() > 0) {
                for (int i = 0; i <= asistenciaList.size() - 1; i++) {
                    try {
                        JSONObject jsonParams = new JSONObject();
                        jsonParams.put("id_sync", asistenciaList.get(i).getId());
                        jsonParams.put("usuario", asistenciaList.get(i).getUsuario());
                        jsonParams.put("pass", asistenciaList.get(i).getContrasena());
                        jsonParams.put("fecha", asistenciaList.get(i).getFecha());
                        jsonParams.put("hora", asistenciaList.get(i).getHora());
                        jsonParams.put("lat", asistenciaList.get(i).getLatitud());
                        jsonParams.put("lng", asistenciaList.get(i).getLongitud());
                        jsonParams.put("tipo", asistenciaList.get(i).getEstatus());
                        jsonParams.put("is_bunker", asistenciaList.get(i).getIsBunker());
                        jsonParams.put("geoFenceActual", asistenciaList.get(i).getGeofence());
                        jsonParams.put("isProveedor", asistenciaList.get(i).getIsProveedor());
                        jsonParams.put("horario_seleccionado", asistenciaList.get(i).getHoraSeleccionada());
                        requestdoCheckINAndCheckOut(jsonParams);
                    } catch (Exception e) {
                        e.printStackTrace();
                        falloAlgunRequestDeSincronizacion = true;
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar ASISTENCIA:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            } else
                Log.i(TAG, "NO hay registros de Sesiones");


            //*********************************** JSON Informacion adicionaladicional ***************************************
            List<Adicional> adicionalList = Adicional.findWithQuery(Adicional.class, "SELECT * FROM ADICIONAL WHERE sync ='0'");
            if (adicionalList.size() > 0) {
                for (int i = 0; i <= adicionalList.size() - 1; i++) {
                    try {
                        JSONObject jsonInformacionAdicionalBitacora = new JSONObject();
                        JSONArray jsonActivos = new JSONArray();
                        JSONArray jsonEquiposInstalacion = new JSONArray();
                        JSONArray jsonEquiposCortejo = new JSONArray();
                        JSONArray jsonEquiposRecoleccion = new JSONArray();
                        JSONArray jsonEquiposTraslado = new JSONArray();
                        JSONArray jsonAtaudesAndUrnas = new JSONArray();
                        JSONArray jsonAtaudesAndUrnasEliminadas = new JSONArray();
                        jsonInformacionAdicionalBitacora.put("bitacora", adicionalList.get(i).getBitacora());
                        jsonInformacionAdicionalBitacora.put("fecha", adicionalList.get(i).getFecha());
                        jsonInformacionAdicionalBitacora.put("hora_instalacion", adicionalList.get(i).getHoraRecoleccion());
                        jsonInformacionAdicionalBitacora.put("documentos_seleccion", adicionalList.get(i).getJsonAdicionalInfo());
                        jsonInformacionAdicionalBitacora.put("lugar_de_velacion", adicionalList.get(i).getLugarDeVelacion());
                        jsonInformacionAdicionalBitacora.put("observaciones_instalacion", adicionalList.get(i).getObservacionesInstalacion());
                        jsonInformacionAdicionalBitacora.put("observaciones_cortejo", adicionalList.get(i).getObservacionesCortejo());
                        jsonInformacionAdicionalBitacora.put("ropa_entregada", adicionalList.get(i).getRopaEntregada());
                        jsonInformacionAdicionalBitacora.put("tipo_servicio", adicionalList.get(i).getTipoDeServicio());
                        jsonInformacionAdicionalBitacora.put("encapsulado", adicionalList.get(i).getEncapsulado());
                        jsonInformacionAdicionalBitacora.put("observaciones_recoleccion", adicionalList.get(i).getObservacionesrecoleccion());
                        jsonInformacionAdicionalBitacora.put("observaciones_traslado", adicionalList.get(i).getObservacionestraslado());
                        jsonInformacionAdicionalBitacora.put("embalsamado_o_arreglo", adicionalList.get(i).getProcedimiento());
                        jsonInformacionAdicionalBitacora.put("laboratorio", adicionalList.get(i).getLaboratorio());
                        jsonInformacionAdicionalBitacora.put("idLaboratorio", DatabaseAssistant.getIDFromLaboratorioString(adicionalList.get(i).getLaboratorio()));
                        jsonInformacionAdicionalBitacora.put("isProveedor", Preferences.getIsProveedor(sContext, Preferences.PREFERENCE_IS_PROVEEDOR) ? "1" : "0");
                        jsonInformacionAdicionalBitacora.put("usuario", DatabaseAssistant.getUserNameFromSesiones());


                        try {
                            List<Documentos> documentosList = Documentos.findWithQuery(Documentos.class, "SELECT * FROM DOCUMENTOS where bitacora = '" + adicionalList.get(i).getBitacora() + "' and sync ='0'");
                            if (documentosList.size() > 0) {
                                for (int y = 0; y <= documentosList.size() - 1; y++) {
                                    JSONObject jsonDocumentos = new JSONObject();
                                    jsonDocumentos.put("name_documento", documentosList.get(y).getDocumento());
                                    jsonDocumentos.put("fecha", documentosList.get(y).getFecha());
                                    jsonDocumentos.put("quien_registro", documentosList.get(y).getQuien());
                                    jsonActivos.put(jsonDocumentos);
                                }
                                jsonInformacionAdicionalBitacora.put("Documentos_extras", jsonActivos);
                            } else
                                Log.d(TAG, "createJsonForSync: No hay datos de documentos");
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            falloAlgunRequestDeSincronizacion = true;
                            Toast.makeText(sContext, "Ocurrio un error al sincronizar DOCUMENTOS:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
                        }


                        try {
                            List<Equipoinstalacion> equipoinstalacionList = Equipoinstalacion.findWithQuery(Equipoinstalacion.class, "SELECT * FROM EQUIPOINSTALACION where bitacora = '" + adicionalList.get(i).getBitacora() + "' and sync ='0'");
                            if (equipoinstalacionList.size() > 0) {
                                for (int y = 0; y <= equipoinstalacionList.size() - 1; y++) {
                                    JSONObject jsonEquiposDeInstalacion = new JSONObject();
                                    jsonEquiposDeInstalacion.put("nombre", equipoinstalacionList.get(y).getNombre());
                                    jsonEquiposDeInstalacion.put("serie", equipoinstalacionList.get(y).getSerie());
                                    jsonEquiposDeInstalacion.put("fecha", equipoinstalacionList.get(y).getFechaCaptura());
                                    jsonEquiposDeInstalacion.put("latitud", equipoinstalacionList.get(y).getLatitud());
                                    jsonEquiposDeInstalacion.put("longitud", equipoinstalacionList.get(y).getLongitud());
                                    jsonEquiposDeInstalacion.put("isBunker", equipoinstalacionList.get(y).getIsBunker());
                                    jsonEquiposDeInstalacion.put("isProveedor", Preferences.getIsProveedor(sContext, Preferences.PREFERENCE_IS_PROVEEDOR) ? "1" : "0");
                                    jsonEquiposDeInstalacion.put("usuario", equipoinstalacionList.get(y).getUsuario());
                                    jsonEquiposInstalacion.put(jsonEquiposDeInstalacion);
                                }
                                jsonInformacionAdicionalBitacora.put("Equipos_instalacion", jsonEquiposInstalacion);
                            } else
                                Log.d(TAG, "createJsonForSync: No hay datos de equipos de instalacion");
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            falloAlgunRequestDeSincronizacion = true;
                            Toast.makeText(sContext, "Ocurrio un error al sincronizar EQUIPOINSTALACION:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
                        }


                        try {
                            List<Equipocortejo> equipocortejoList = Equipocortejo.findWithQuery(Equipocortejo.class, "SELECT * FROM EQUIPOCORTEJO where bitacora = '" + adicionalList.get(i).getBitacora() + "' and sync ='0'");
                            if (equipocortejoList.size() > 0) {
                                for (int y = 0; y <= equipocortejoList.size() - 1; y++) {
                                    JSONObject jsonEquiposDeCortejo = new JSONObject();
                                    jsonEquiposDeCortejo.put("nombre", equipocortejoList.get(y).getNombre());
                                    jsonEquiposDeCortejo.put("serie", equipocortejoList.get(y).getSerie());
                                    jsonEquiposDeCortejo.put("fecha", equipocortejoList.get(y).getFechacaptura());
                                    jsonEquiposDeCortejo.put("latitud", equipocortejoList.get(y).getLatitud());
                                    jsonEquiposDeCortejo.put("longitud", equipocortejoList.get(y).getLongitud());
                                    jsonEquiposDeCortejo.put("isBunker", equipocortejoList.get(y).getIsBunker());
                                    jsonEquiposDeCortejo.put("isProveedor", Preferences.getIsProveedor(sContext, Preferences.PREFERENCE_IS_PROVEEDOR) ? "1" : "0");
                                    jsonEquiposDeCortejo.put("usuario", equipocortejoList.get(y).getUsuario());
                                    jsonEquiposCortejo.put(jsonEquiposDeCortejo);
                                }
                                jsonInformacionAdicionalBitacora.put("Equipos_cortejo", jsonEquiposCortejo);
                            } else
                                Log.d(TAG, "createJsonForSync: No hay datos de equipos de cortejo");
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            falloAlgunRequestDeSincronizacion = true;
                            Toast.makeText(sContext, "Ocurrio un error al sincronizar EQUIPOCORTEJO:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
                        }


                        try {
                            List<EquipoRecoleccion> equipoRecoleccions = EquipoRecoleccion.findWithQuery(EquipoRecoleccion.class, "SELECT * FROM EQUIPO_RECOLECCION where bitacora = '" + adicionalList.get(i).getBitacora() + "' and sync ='0'");
                            if (equipoRecoleccions.size() > 0) {
                                for (int y = 0; y <= equipoRecoleccions.size() - 1; y++) {
                                    JSONObject jsonEquiposDeRecoleccion = new JSONObject();
                                    jsonEquiposDeRecoleccion.put("nombre", equipoRecoleccions.get(y).getNombre());
                                    jsonEquiposDeRecoleccion.put("serie", equipoRecoleccions.get(y).getSerie());
                                    jsonEquiposDeRecoleccion.put("fecha", equipoRecoleccions.get(y).getFechacaptura());
                                    jsonEquiposDeRecoleccion.put("latitud", equipoRecoleccions.get(y).getLatitud());
                                    jsonEquiposDeRecoleccion.put("longitud", equipoRecoleccions.get(y).getLongitud());
                                    jsonEquiposDeRecoleccion.put("isBunker", equipoRecoleccions.get(y).getIsBunker());
                                    jsonEquiposDeRecoleccion.put("isProveedor", Preferences.getIsProveedor(sContext, Preferences.PREFERENCE_IS_PROVEEDOR) ? "1" : "0");
                                    jsonEquiposDeRecoleccion.put("usuario", equipoRecoleccions.get(y).getUsuario());
                                    jsonEquiposDeRecoleccion.put("tipo", equipoRecoleccions.get(y).getTipo());
                                    jsonEquiposRecoleccion.put(jsonEquiposDeRecoleccion);
                                }
                                jsonInformacionAdicionalBitacora.put("Equipos_recoleccion", jsonEquiposRecoleccion);
                            }
                            Log.d(TAG, "createJsonForSync: No hay datos de equipo de recoleccion");
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            falloAlgunRequestDeSincronizacion = true;
                            Toast.makeText(sContext, "Ocurrio un error al sincronizar EQUIPO_RECOLECCION:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
                        }


                        //FALRA EQUIPOS DE TRASLADO
                        try {
                            List<EquipoTraslado> equipoTraslados = EquipoTraslado.findWithQuery(EquipoTraslado.class, "SELECT * FROM EQUIPO_TRASLADO where bitacora = '" + adicionalList.get(i).getBitacora() + "' and sync ='0'");
                            if (equipoTraslados.size() > 0) {
                                for (int y = 0; y <= equipoTraslados.size() - 1; y++) {
                                    JSONObject jsonEquiposDeTraslado = new JSONObject();
                                    jsonEquiposDeTraslado.put("nombre", equipoTraslados.get(y).getNombre());
                                    jsonEquiposDeTraslado.put("serie", equipoTraslados.get(y).getSerie());
                                    jsonEquiposDeTraslado.put("fecha", equipoTraslados.get(y).getFechacaptura());
                                    jsonEquiposDeTraslado.put("latitud", equipoTraslados.get(y).getLatitud());
                                    jsonEquiposDeTraslado.put("longitud", equipoTraslados.get(y).getLongitud());
                                    jsonEquiposDeTraslado.put("isBunker", equipoTraslados.get(y).getIsBunker());
                                    jsonEquiposDeTraslado.put("isProveedor", Preferences.getIsProveedor(sContext, Preferences.PREFERENCE_IS_PROVEEDOR) ? "1" : "0");
                                    jsonEquiposDeTraslado.put("usuario", equipoTraslados.get(y).getUsuario());
                                    jsonEquiposDeTraslado.put("tipo", equipoTraslados.get(y).getTipo());
                                    jsonEquiposTraslado.put(jsonEquiposDeTraslado);
                                }
                                jsonInformacionAdicionalBitacora.put("Equipos_traslado", jsonEquiposTraslado);
                            }
                            Log.d(TAG, "createJsonForSync: No hay datos de equipo de traslado");
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            falloAlgunRequestDeSincronizacion = true;
                            Toast.makeText(sContext, "Ocurrio un error al sincronizar EQUIPO_TRASLADO:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
                        }


                        requestToSyncInfoAdicionalPorBitacora(jsonInformacionAdicionalBitacora);
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                        falloAlgunRequestDeSincronizacion = true;
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar ADICIONAL:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
                    }

                }
            } else
                Log.i(TAG, "NO hay registros de Ubicaciones");
            //***********************


            //*************** ATAUDES y URNAS AGREGADAS Y ELIMINADAS********************
            try {
                JSONObject jsonInformacionAdicionalBitacora = new JSONObject();
                JSONArray jsonAtaudesAndUrnas = new JSONArray();
                JSONArray jsonAtaudesAndUrnasEliminadas = new JSONArray();

                try {
                    String queryFromInventario = "SELECT * FROM INVENTARIO where sync ='0'";
                    List<Inventario> inventarioList = Inventario.findWithQuery(Inventario.class, queryFromInventario);
                    if (inventarioList.size() > 0) {
                        for (int y = 0; y <= inventarioList.size() - 1; y++) {
                            JSONObject jsonAtaurnas = new JSONObject();
                            jsonAtaurnas.put("codigo_ataurna", inventarioList.get(y).getCodigo());
                            jsonAtaurnas.put("item_ataurna", inventarioList.get(y).getDescripcion());
                            jsonAtaurnas.put("serie_ataurna", inventarioList.get(y).getSerie());
                            jsonAtaurnas.put("proveedor_ataurna", inventarioList.get(y).getProveedor());
                            jsonAtaurnas.put("fecha_ataurna", inventarioList.get(y).getFechaAtaurna().replace("/", "-"));
                            jsonAtaurnas.put("bitacora", inventarioList.get(y).getBitacora());
                            jsonAtaurnas.put("fecha_captura", inventarioList.get(y).getFechaCaptura());
                            jsonAtaurnas.put("latitud", inventarioList.get(y).getLatitud());
                            jsonAtaurnas.put("longitud", inventarioList.get(y).getLongitud());
                            jsonAtaurnas.put("isProveedor", Preferences.getIsProveedor(sContext, Preferences.PREFERENCE_IS_PROVEEDOR) ? "1" : "0");
                            jsonAtaurnas.put("usuario", DatabaseAssistant.getUserNameFromSesiones());
                            jsonAtaurnas.put("tipo_movimiento", inventarioList.get(y).getTipomovimiento());
                            jsonAtaurnas.put("serie_completa", inventarioList.get(y).getSerieataurna());
                            jsonAtaudesAndUrnas.put(jsonAtaurnas);
                        }
                        jsonInformacionAdicionalBitacora.put("Ataurnas_registradas", jsonAtaudesAndUrnas);
                    } else
                        Log.d(TAG, "onResponse: No se hay datos de ataudes y urnas");
                } catch (JSONException ex) {
                    ex.printStackTrace();
                    falloAlgunRequestDeSincronizacion = true;
                    Toast.makeText(sContext, "Ocurrio un error al sincronizar INVENTARIO:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
                }


                try {
                    String queryFromCancelados = "SELECT * FROM CANCELADOS where sync ='0'";
                    List<Cancelados> canceladosList = Cancelados.findWithQuery(Cancelados.class, queryFromCancelados);
                    if (canceladosList.size() > 0) {
                        for (int y = 0; y <= canceladosList.size() - 1; y++) {
                            JSONObject jsonAtaurnasCanceladas = new JSONObject();
                            jsonAtaurnasCanceladas.put("codigo_ataurna", canceladosList.get(y).getCodigo());
                            jsonAtaurnasCanceladas.put("item_ataurna", canceladosList.get(y).getDescripcion());
                            jsonAtaurnasCanceladas.put("serie_ataurna", canceladosList.get(y).getSerie());
                            jsonAtaurnasCanceladas.put("proveedor_ataurna", canceladosList.get(y).getProveedor());
                            jsonAtaurnasCanceladas.put("fecha_ataurna", canceladosList.get(y).getFechaAtaurna().replace("/", "-"));
                            jsonAtaurnasCanceladas.put("bitacora", canceladosList.get(y).getBitacora());
                            jsonAtaurnasCanceladas.put("fecha_cancelacion", canceladosList.get(y).getFechaCancelacion());
                            jsonAtaurnasCanceladas.put("usuario", DatabaseAssistant.getUserNameFromSesiones());
                            jsonAtaurnasCanceladas.put("fecha_captura", canceladosList.get(y).getFechaCaptura());
                            jsonAtaudesAndUrnasEliminadas.put(jsonAtaurnasCanceladas);
                        }
                        jsonInformacionAdicionalBitacora.put("Ataurnas_eliminadas", jsonAtaudesAndUrnasEliminadas);
                    } else
                        Log.d(TAG, "onResponse: No se hay datos de Eliminados");
                } catch (JSONException ex) {
                    ex.printStackTrace();
                    falloAlgunRequestDeSincronizacion = true;
                    Toast.makeText(sContext, "Ocurrio un error al sincronizar CANCELADOS:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
                }

                if (jsonInformacionAdicionalBitacora.length() > 0)
                    requestToSyncAtaurnas(jsonInformacionAdicionalBitacora);
            } catch (Throwable ex) {
                ex.printStackTrace();
                falloAlgunRequestDeSincronizacion = true;
                Toast.makeText(sContext, "Ocurrio un error al sincronizar:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
            }



            JSONObject jsonArticulosDeVelacioGeneral = new JSONObject();
            try {
                List<Equipoinstalacion> equipoinstalacionList = Equipoinstalacion.findWithQuery(Equipoinstalacion.class, "SELECT * FROM EQUIPOINSTALACION where sync ='0'");
                if (equipoinstalacionList.size() > 0) {
                    JSONArray jsonEquiposInstalacion = new JSONArray();
                    for (int y = 0; y <= equipoinstalacionList.size() - 1; y++) {
                        JSONObject jsonEquiposDeInstalacion = new JSONObject();
                        jsonEquiposDeInstalacion.put("bitacora", equipoinstalacionList.get(y).getBitacora());
                        jsonEquiposDeInstalacion.put("nombre", equipoinstalacionList.get(y).getNombre());
                        jsonEquiposDeInstalacion.put("serie", equipoinstalacionList.get(y).getSerie());
                        jsonEquiposDeInstalacion.put("fecha", equipoinstalacionList.get(y).getFechaCaptura());
                        jsonEquiposDeInstalacion.put("latitud", equipoinstalacionList.get(y).getLatitud());
                        jsonEquiposDeInstalacion.put("longitud", equipoinstalacionList.get(y).getLongitud());
                        jsonEquiposDeInstalacion.put("isBunker", equipoinstalacionList.get(y).getIsBunker());
                        jsonEquiposDeInstalacion.put("isProveedor", Preferences.getIsProveedor(sContext, Preferences.PREFERENCE_IS_PROVEEDOR) ? "1" : "0");
                        jsonEquiposDeInstalacion.put("usuario", equipoinstalacionList.get(y).getUsuario());
                        jsonEquiposInstalacion.put(jsonEquiposDeInstalacion);
                    }
                    jsonArticulosDeVelacioGeneral.put("Equipos_instalacion", jsonEquiposInstalacion);
                } else
                    Log.d(TAG, "createJsonForSync: No hay datos de equipos de instalacion");
            } catch (JSONException ex) {
                ex.printStackTrace();
                falloAlgunRequestDeSincronizacion = true;
                Toast.makeText(sContext, "Ocurrio un error al sincronizar EQUIPOINSTALACION:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
            }


            try {
                List<Equipocortejo> equipocortejoList = Equipocortejo.findWithQuery(Equipocortejo.class, "SELECT * FROM EQUIPOCORTEJO where sync ='0'");
                if (equipocortejoList.size() > 0) {
                    JSONArray jsonEquiposCortejo = new JSONArray();
                    for (int y = 0; y <= equipocortejoList.size() - 1; y++) {
                        JSONObject jsonEquiposDeCortejo = new JSONObject();
                        jsonEquiposDeCortejo.put("bitacora", equipocortejoList.get(y).getBitacora());
                        jsonEquiposDeCortejo.put("nombre", equipocortejoList.get(y).getNombre());
                        jsonEquiposDeCortejo.put("serie", equipocortejoList.get(y).getSerie());
                        jsonEquiposDeCortejo.put("fecha", equipocortejoList.get(y).getFechacaptura());
                        jsonEquiposDeCortejo.put("latitud", equipocortejoList.get(y).getLatitud());
                        jsonEquiposDeCortejo.put("longitud", equipocortejoList.get(y).getLongitud());
                        jsonEquiposDeCortejo.put("isBunker", equipocortejoList.get(y).getIsBunker());
                        jsonEquiposDeCortejo.put("isProveedor", Preferences.getIsProveedor(sContext, Preferences.PREFERENCE_IS_PROVEEDOR) ? "1" : "0");
                        jsonEquiposDeCortejo.put("usuario", equipocortejoList.get(y).getUsuario());
                        jsonEquiposCortejo.put(jsonEquiposDeCortejo);
                    }
                    jsonArticulosDeVelacioGeneral.put("Equipos_cortejo", jsonEquiposCortejo);
                } else
                    Log.d(TAG, "createJsonForSync: No hay datos de equipos de cortejo");
            } catch (JSONException ex) {
                ex.printStackTrace();
                falloAlgunRequestDeSincronizacion = true;
                Toast.makeText(sContext, "Ocurrio un error al sincronizar EQUIPOCORTEJO:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
            }


            try {
                List<EquipoRecoleccion> equipoRecoleccions = EquipoRecoleccion.findWithQuery(EquipoRecoleccion.class, "SELECT * FROM EQUIPO_RECOLECCION where sync = '0'");
                if (equipoRecoleccions.size() > 0) {
                    JSONArray jsonEquiposRecoleccion = new JSONArray();
                    for (int y = 0; y <= equipoRecoleccions.size() - 1; y++) {
                        JSONObject jsonEquiposDeRecoleccion = new JSONObject();
                        jsonEquiposDeRecoleccion.put("bitacora", equipoRecoleccions.get(y).getBitacora());
                        jsonEquiposDeRecoleccion.put("nombre", equipoRecoleccions.get(y).getNombre());
                        jsonEquiposDeRecoleccion.put("serie", equipoRecoleccions.get(y).getSerie());
                        jsonEquiposDeRecoleccion.put("fecha", equipoRecoleccions.get(y).getFechacaptura());
                        jsonEquiposDeRecoleccion.put("latitud", equipoRecoleccions.get(y).getLatitud());
                        jsonEquiposDeRecoleccion.put("longitud", equipoRecoleccions.get(y).getLongitud());
                        jsonEquiposDeRecoleccion.put("isBunker", equipoRecoleccions.get(y).getIsBunker());
                        jsonEquiposDeRecoleccion.put("isProveedor", Preferences.getIsProveedor(sContext, Preferences.PREFERENCE_IS_PROVEEDOR) ? "1" : "0");
                        jsonEquiposDeRecoleccion.put("usuario", equipoRecoleccions.get(y).getUsuario());
                        jsonEquiposDeRecoleccion.put("tipo", equipoRecoleccions.get(y).getTipo());
                        jsonEquiposRecoleccion.put(jsonEquiposDeRecoleccion);
                    }
                    jsonArticulosDeVelacioGeneral.put("Equipos_recoleccion", jsonEquiposRecoleccion);
                }
                Log.d(TAG, "createJsonForSync: No hay datos de equipo de recoleccion");
            } catch (JSONException ex) {
                ex.printStackTrace();
                falloAlgunRequestDeSincronizacion = true;
                Toast.makeText(sContext, "Ocurrio un error al sincronizar EQUIPO_RECOLECCION:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
            }


            //FALRA EQUIPOS DE TRASLADO
            try {
                List<EquipoTraslado> equipoTraslados = EquipoTraslado.findWithQuery(EquipoTraslado.class, "SELECT * FROM EQUIPO_TRASLADO where sync ='0'");
                if (equipoTraslados.size() > 0) {
                    JSONArray jsonEquiposTraslado = new JSONArray();
                    for (int y = 0; y <= equipoTraslados.size() - 1; y++) {
                        JSONObject jsonEquiposDeTraslado = new JSONObject();
                        jsonEquiposDeTraslado.put("bitacora", equipoTraslados.get(y).getBitacora());
                        jsonEquiposDeTraslado.put("nombre", equipoTraslados.get(y).getNombre());
                        jsonEquiposDeTraslado.put("serie", equipoTraslados.get(y).getSerie());
                        jsonEquiposDeTraslado.put("fecha", equipoTraslados.get(y).getFechacaptura());
                        jsonEquiposDeTraslado.put("latitud", equipoTraslados.get(y).getLatitud());
                        jsonEquiposDeTraslado.put("longitud", equipoTraslados.get(y).getLongitud());
                        jsonEquiposDeTraslado.put("isBunker", equipoTraslados.get(y).getIsBunker());
                        jsonEquiposDeTraslado.put("isProveedor", Preferences.getIsProveedor(sContext, Preferences.PREFERENCE_IS_PROVEEDOR) ? "1" : "0");
                        jsonEquiposDeTraslado.put("usuario", equipoTraslados.get(y).getUsuario());
                        jsonEquiposDeTraslado.put("tipo", equipoTraslados.get(y).getTipo());
                        jsonEquiposTraslado.put(jsonEquiposDeTraslado);
                    }
                    jsonArticulosDeVelacioGeneral.put("Equipos_traslado", jsonEquiposTraslado);
                }
                Log.d(TAG, "createJsonForSync: No hay datos de equipo de traslado");
            } catch (JSONException ex) {
                ex.printStackTrace();
                falloAlgunRequestDeSincronizacion = true;
                Toast.makeText(sContext, "Ocurrio un error al sincronizar EQUIPO_TRASLADO:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
            }


            requestArticulosDeVelacion(jsonArticulosDeVelacioGeneral);
        } catch (Throwable e) {
            Log.e(TAG, "createJsonEvents: Error");
            falloAlgunRequestDeSincronizacion = true;
            Toast.makeText(sContext, "Ocurrio un error al sincronizar createJsonForSync:  " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

        return falloAlgunRequestDeSincronizacion;
    }

    @SuppressLint("LongLogTag")
    public void refreshLocation()
    {

        try{
            List<Movimientos> listaMovs = Movimientos.findWithQuery(Movimientos.class, "SELECT * FROM MOVIMIENTOS WHERE estatus = '0'");
            if (listaMovs.size() > 0) {
                JSONObject movimientos = new JSONObject();
                JSONArray arrayMovimientos = new JSONArray();
                for (int i = 0; i <= listaMovs.size() - 1; i++) {
                    try {
                        JSONObject jsonMovimientoss = new JSONObject();
                        jsonMovimientoss = new JSONObject();
                        jsonMovimientoss.put("id_list", listaMovs.get(i).getId());
                        jsonMovimientoss.put("id_chofer", listaMovs.get(i).getIdchofer());
                        jsonMovimientoss.put("id_ayudante", listaMovs.get(i).getIdayudante());
                        jsonMovimientoss.put("android_id", listaMovs.get(i).getAndroidid());
                        jsonMovimientoss.put("app_version", listaMovs.get(i).getAppversion());
                        jsonMovimientoss.put("android_model", listaMovs.get(i).getAndroidmodel());
                        jsonMovimientoss.put("imei", listaMovs.get(i).getImei());
                        jsonMovimientoss.put("fecha", listaMovs.get(i).getFecha());
                        jsonMovimientoss.put("movimiento", listaMovs.get(i).getMovimiento());
                        jsonMovimientoss.put("usuario", listaMovs.get(i).getUsuario());
                        jsonMovimientoss.put("isProveedor", Preferences.getIsProveedor(sContext, Preferences.PREFERENCE_IS_PROVEEDOR) ? "1" : "0");
                        arrayMovimientos.put(jsonMovimientoss);
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar MOVIMIENTOS:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
                try {
                    movimientos.put("movimientos",arrayMovimientos);
                    cargaDeMovimientosToServer(movimientos);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("TIMER_REFRESH", "refreshLocation: " +  e.getMessage());
                }
            } else
                Log.i(TAG, "NO hay registros de Movimientos");


            //********************************************************Ubicaciones******************************************************************
        }catch (Throwable e){
            Log.e("TIMER_REFRESH", "SaveMovements: " +  e.getMessage());
            Toast.makeText(sContext, "Error en SaveMovements", Toast.LENGTH_SHORT).show();
        }


        try{
            @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(sContext.getContentResolver(), Settings.Secure.ANDROID_ID);
            //*********************************** JSON UBICACIONES ***************************************
            String queryUbicaciones = "SELECT * FROM UBICACIONES";
            List<Ubicaciones> listaUbicaciones = Ubicaciones.findWithQuery(Ubicaciones.class, queryUbicaciones);
            if (listaUbicaciones.size() > 0) {
                JSONObject jsonGeneralUbicaciones = new JSONObject();
                JSONArray jsonArrayUbicaciones = new JSONArray();
                for (int i = 0; i <= listaUbicaciones.size() - 1; i++) {
                    try {
                        JSONObject jsonUbicaciones = new JSONObject();
                        jsonUbicaciones.put("evento_id", listaUbicaciones.get(i).getId());
                        jsonUbicaciones.put("id_device", android_id);
                        jsonUbicaciones.put("lat", listaUbicaciones.get(i).getLatitud());
                        jsonUbicaciones.put("lng", listaUbicaciones.get(i).getLongitud());
                        jsonUbicaciones.put("fecha", listaUbicaciones.get(i).getFecha());
                        jsonUbicaciones.put("hora", listaUbicaciones.get(i).getHora());
                        jsonUbicaciones.put("isProveedor", Preferences.getIsProveedor(sContext, Preferences.PREFERENCE_IS_PROVEEDOR) ? "1" : "0");
                        jsonUbicaciones.put("usuario", DatabaseAssistant.getUserNameFromSesiones());
                        jsonArrayUbicaciones.put(jsonUbicaciones);
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                        Log.i(TAG, "NO hay registros de Ubicaciones");
                    }
                }
                jsonGeneralUbicaciones.put("ubicaciones_list", jsonArrayUbicaciones);

                if(checkInternetConnection())
                    cargaDeUbicacionesToServer(jsonGeneralUbicaciones);

            } else
                Log.i(TAG, "NO hay registros de Ubicaciones");
        }catch (Throwable e){
            Log.e("TIMER_REFRESH", "positions: " +  e.getMessage());
            Toast.makeText(sContext, "Error en StorePosition", Toast.LENGTH_SHORT).show();
        }


    }

    public static void requestToSyncInfoAdicionalPorBitacora(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_SAVE_INFORMATION_ADICIONAL_POR_BITACORA, jsonParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getBoolean("Success")) {
                                Adicional.executeQuery("UPDATE ADICIONAL set sync = '1' WHERE bitacora = '" + jsonParams.getString("bitacora") + "'");
                                Documentos.executeQuery("UPDATE DOCUMENTOS set sync = '1' WHERE bitacora = '" + jsonParams.getString("bitacora") + "'");
                                Equipoinstalacion.executeQuery("UPDATE EQUIPOINSTALACION set sync = '1' WHERE bitacora = '" + jsonParams.getString("bitacora") + "'");
                                Equipocortejo.executeQuery("UPDATE EQUIPOCORTEJO set sync = '1' WHERE bitacora = '" + jsonParams.getString("bitacora") + "'");
                                Equipocortejo.executeQuery("UPDATE EQUIPO_RECOLECCION set sync = '1' WHERE bitacora = '" + jsonParams.getString("bitacora") + "'");
                                Equipocortejo.executeQuery("UPDATE EQUIPO_TRASLADO set sync = '1' WHERE bitacora = '" + jsonParams.getString("bitacora") + "'");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(sContext, "Ocurrio un error al sincronizar STORE_BITACORA_DETAILS:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar STORE_BITACORA_DETAILS:  " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
    }


    public static void requestArticulosDeVelacion(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_SAVE_ARTICLES_INVENTORY, jsonParams,
                new Response.Listener<JSONObject>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jsonSuccess = response.getJSONObject("success");
                            try {
                                JSONArray arrayInstalacion = jsonSuccess.getJSONArray("Equipos_instalacion");
                                JSONArray arrayCortejo = jsonSuccess.getJSONArray("Equipos_cortejo");
                                JSONArray arrayRecoleccion = jsonSuccess.getJSONArray("Equipos_recoleccion");
                                JSONArray arrayTraslado = jsonSuccess.getJSONArray("Equipos_traslado");

                                try {
                                    for (int i = 0; i <= arrayInstalacion.length() - 1; i++) {
                                        Equipoinstalacion.executeQuery("UPDATE EQUIPOINSTALACION set sync = '1' WHERE bitacora = '"
                                                + arrayInstalacion.getJSONObject(i).getString("bitacora")
                                                + "' and serie = '" + arrayInstalacion.getJSONObject(i).getString("serie")
                                                + "' and fechacaptura ='" + arrayInstalacion.getJSONObject(i).getString("fecha") + "'");

                                        Log.v("ASRTIUCLOS", "onResponse: Actualizado correctamente el articulo de Instalacion");
                                    }
                                } catch (Throwable e) {
                                    Log.e("ASRTIUCLOS", "onResponse: Error al actualizar el sync en instalacion: " + e.getMessage());
                                    Toast.makeText(sContext, "Ocurrio un error en storeBitacoraAppliances EQUIPOINSTALACION: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                }


                                try {
                                    for (int i = 0; i <= arrayCortejo.length() - 1; i++) {
                                        Equipocortejo.executeQuery("UPDATE EQUIPOCORTEJO set sync = '1' WHERE bitacora = '"
                                                + arrayCortejo.getJSONObject(i).getString("bitacora")
                                                + "' and serie = '" + arrayCortejo.getJSONObject(i).getString("serie")
                                                + "' and fechacaptura ='" + arrayCortejo.getJSONObject(i).getString("fecha") + "'");

                                        Log.v("ASRTIUCLOS", "onResponse: Actualizado correctamente el articulo de Equipocortejo");
                                    }
                                } catch (Throwable e) {
                                    Log.e("ASRTIUCLOS", "onResponse: Error al actualizar el sync en Equipocortejo: " + e.getMessage());
                                    Toast.makeText(sContext, "Ocurrio un error en storeBitacoraAppliances EQUIPOCORTEJO: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                }


                                try {
                                    for (int i = 0; i <= arrayRecoleccion.length() - 1; i++) {
                                        EquipoRecoleccion.executeQuery("UPDATE EQUIPO_RECOLECCION set sync = '1' WHERE bitacora = '"
                                                + arrayRecoleccion.getJSONObject(i).getString("bitacora")
                                                + "' and serie = '" + arrayRecoleccion.getJSONObject(i).getString("serie")
                                                + "' and fechacaptura ='" + arrayRecoleccion.getJSONObject(i).getString("fecha") + "'");

                                        Log.v("ASRTIUCLOS", "onResponse: Actualizado correctamente el articulo de EquipoRecoleccion");
                                    }
                                } catch (Throwable e) {
                                    Log.e("ASRTIUCLOS", "onResponse: Error al actualizar el sync en EquipoRecoleccion: " + e.getMessage());
                                    Toast.makeText(sContext, "Ocurrio un error en storeBitacoraAppliances EQUIPO_RECOLECCION: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                }


                                try {
                                    for (int i = 0; i <= arrayTraslado.length() - 1; i++) {
                                        EquipoTraslado.executeQuery("UPDATE EQUIPO_TRASLADO set sync = '1' WHERE bitacora = '"
                                                + arrayTraslado.getJSONObject(i).getString("bitacora")
                                                + "' and serie = '" + arrayTraslado.getJSONObject(i).getString("serie")
                                                + "' and fechacaptura ='" + arrayTraslado.getJSONObject(i).getString("fecha") + "'");
                                        Log.v("ASRTIUCLOS", "onResponse: Actualizado correctamente el articulo de EquipoTraslado");
                                    }
                                } catch (Throwable e) {
                                    Log.e("ASRTIUCLOS", "onResponse: Error al actualizar el sync en EquipoTraslado: " + e.getMessage());
                                    Toast.makeText(sContext, "Ocurrio un error en storeBitacoraAppliances EQUIPO_TRASLADO: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (Throwable e) {
                                Log.e(TAG, "onResponse: Erorr en requestArticulosDeVelacion");
                                Toast.makeText(sContext, "Ocurrio un error en storeBitacoraAppliances success: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("ASRTIUCLOS", "onResponse: Ocurrio un error: storeBitacoraAppliances " + e.getMessage());
                            //Toast.makeText(sContext, "Ocurrio un error en storeBitacoraAppliances onResponse: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("ASRTIUCLOS", "onErrorResponse: Error en sync articulos de velacion: " + error.getMessage());
                        Toast.makeText(sContext, "Ocurrio un error en storeBitacoraAppliances onErrorResponse: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
    }


    public static void requestToSyncAtaurnas(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_SAVE_ATAURNAS, jsonParams,
                new Response.Listener<JSONObject>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getBoolean("Success"))
                            {
                                JSONObject jsonResult = new JSONObject(response.getString("Result"));

                                if(jsonResult.length()>0) {

                                    if (jsonResult.has("Ataurnas_registradas")) {
                                        JSONArray arrayAtaurnasRegistradas = new JSONArray();
                                        arrayAtaurnasRegistradas = jsonResult.getJSONArray("Ataurnas_registradas");

                                        for (int i = 0; i <= arrayAtaurnasRegistradas.length() - 1; i++) {
                                            Inventario.executeQuery("UPDATE INVENTARIO set sync = '1' WHERE codigo = '" + arrayAtaurnasRegistradas.getJSONObject(i).getString("codigo_ataurna") + "'");
                                            Log.d(TAG, "onResponse: Inventario actualizado correctamente: " + arrayAtaurnasRegistradas.getJSONObject(i).getString("codigo_ataurna"));
                                        }
                                    }

                                    if (jsonResult.has("Ataurnas_eliminadas")) {
                                        JSONArray arrayAtaurnasEliminadas = new JSONArray();
                                        arrayAtaurnasEliminadas = jsonResult.getJSONArray("Ataurnas_eliminadas");
                                        for (int i = 0; i <= arrayAtaurnasEliminadas.length() - 1; i++) {
                                            Inventario.executeQuery("UPDATE CANCELADOS set sync = '1' WHERE codigo = '" + arrayAtaurnasEliminadas.getJSONObject(i).getString("codigo_ataurna") + "'");
                                            Log.d(TAG, "onResponse: Cancelados actualizados correctamente: " + arrayAtaurnasEliminadas.getJSONObject(i).getString("codigo_ataurna"));
                                        }
                                    }
                                }else
                                    Log.d(TAG, "onResponse: No se hay datos de ataudes y urnas");
                            } else
                                Log.d("LOCATION", response.getString("error"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(sContext, "Ocurrio un error al sincronizar STORE_URNA_AND_ATAUD:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar STORE_URNA_AND_ATAUD:  " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
    }


    public static void requestLoginOnline(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_LOGIN, jsonParams,
                new Response.Listener<JSONObject>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getBoolean("Success")) {

                                if(response.has("login")){
                                    Sesiones.executeQuery("UPDATE SESIONES SET statos ='1' WHERE latitud='" + response.getJSONObject("login").getString("lat") + "' AND longitud ='"+ response.getJSONObject("login").getString("lng") + "' AND fecha ='"+ response.getJSONObject("login").getString("fecha") +"' AND hora ='"+ response.getJSONObject("login").getString("hora") +"'");
                                    Log.d("SESIONES", "onResponse: Actualizado 1 registro de sesiones.");
                                }

                            }

                            if (response.has("laboratorios")) {
                                try {
                                    Laboratorios.deleteAll(Laboratorios.class);
                                    JSONArray jsonArrayLaboratorios = new JSONArray();
                                    jsonArrayLaboratorios = response.getJSONArray("laboratorios");
                                    for(int i =0; i<=jsonArrayLaboratorios.length()-1;i++){
                                        DatabaseAssistant.insertarLaboratorios(
                                                "" + jsonArrayLaboratorios.getJSONObject(i).getString("name"),
                                                "" + jsonArrayLaboratorios.getJSONObject(i).getString("status"),
                                                "" + jsonArrayLaboratorios.getJSONObject(i).getString("id")
                                        );
                                    }
                                } catch (Throwable e) {
                                    Log.e("SESIONES", "createJsonParametersForLoginOnline: " + e.getMessage());
                                    Toast.makeText(sContext, "Ocurrio un error al sincronizar LOGIN_APP:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                                }

                            } else {
                                Log.e("SESIONES", "run: no es success");
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(sContext, "Ocurrio un error al sincronizar LOGIN_APP:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar LOGIN_APP:  " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
    }


    public static void requestToSyncEdicionesDeDestinosParaBitacoras(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_SYNC_EDICION_DE_DESTINOS, jsonParams,
                new Response.Listener<JSONObject>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            JSONArray edicionesArray  = response.getJSONArray("success");
                            if(edicionesArray.length()>0)
                            {
                                for(int i =0; i<= edicionesArray.length()-1; i++)
                                {
                                    String query = "UPDATE EDICIONES set sync = '1' WHERE bitacora = '" + edicionesArray.getJSONObject(i).getString("bitacora") + "' and fechacaptura = '" + edicionesArray.getJSONObject(i).getString("fecha_captura") + "'";
                                    Ediciones.executeQuery(query);
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(TAG, "onResponse: Ocurrio un error al sincronizar las edicones: " + e.getMessage());
                            Toast.makeText(sContext, "Ocurrio un error al sincronizar EDIT_BITACORA_EVENT:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e(TAG, "onResponse: Ocurrio un error al sincronizar las edicones: " + error.getMessage());
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar EDIT_BITACORA_EVENT:  " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
    }


    public static void requestdoCheckINAndCheckOut(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_CHECKINCHECKOUT_NEW, jsonParams,
                new Response.Listener<JSONObject>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getBoolean("Success")) {
                                String query ="UPDATE ASISTENCIA set statos = '1' WHERE id = '" + jsonParams.getString("id_sync") + "' and fecha ='" + jsonParams.getString("fecha") +"' and hora = '" + jsonParams.getString("hora") +"'";
                                Asistencia.executeQuery(query);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(sContext, "Ocurrio un error al sincronizar DO_CHECKIN_AND_CHECKOUT:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar DO_CHECKIN_AND_CHECKOUT:  " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
    }






    public static void requestSaveCommentariosToServer(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_SAVE_COMENTARIOS, jsonParams,
                new Response.Listener<JSONObject>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(!response.getBoolean("error"))
                            {
                                JSONArray arrayResult = new JSONArray();
                                arrayResult = response.getJSONArray("result");
                                for(int i =0; i<=arrayResult.length()-1;i++){
                                    String query ="UPDATE COMENTARIOS set sync = '1' WHERE bitacora = '" + arrayResult.getJSONObject(i).getString("bitacora") + "' and fecha ='" + arrayResult.getJSONObject(i).getString("fecha_captura") +"'";
                                    Comentarios.executeQuery(query);
                                }
                                Log.d(TAG, "onResponse: Comentarios actualizados");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(sContext, "Ocurrio un error al sincronizar STORE_COMMENTS:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar STORE_COMMENTS:  " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
    }


    public static void cargaDeDatosAServidor(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_URL, jsonParams, new Response.Listener<JSONObject>() {
            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(JSONObject response)
            {
                try{
                    if(response.getBoolean("Success"))
                    {
                        if(response.has("success")) {
                            JSONArray jsonSuccess = response.getJSONArray("success");
                            if(jsonSuccess.length()>0) {
                                for (int i = 0; i <= jsonSuccess.length() - 1; i++) {
                                    String query = "UPDATE EVENTOS set estatus = '1' WHERE id = '" + jsonSuccess.getJSONObject(i).getString("evento_id") + "' and fecha = '" + jsonSuccess.getJSONObject(i).getString("fecha") + "'";
                                    Eventos.executeQuery(query);
                                }
                            }
                        }

                        if(response.has("query")) {
                            try {
                                String queryJSON = response.getString("query");
                                //String queryJSON = "INSERT INTO EQUIPOCORTEJO (bitacora, fechacaptura, fechacapturadesdeserver, is_bunker, latitud, longitud, nombre, serie, sync, usuario) SELECT bitacora, datetime('now','localtime'), '', is_bunker, latitud, longitud, (SELECT nombre FROM EQUIPOINSTALACION WHERE nombre='Candelabro' limit 0,1),(SELECT serie FROM EQUIPOINSTALACION WHERE nombre='Candelabro' limit 0,1), 0, usuario FROM EQUIPOCORTEJO WHERE nombre = 'Cristo Base' and 1 = (SELECT case when count(*) >= 1 then 2 else 1 end FROM EQUIPOCORTEJO WHERE nombre = 'Candelabro') union all SELECT bitacora, datetime('now','localtime'), '', is_bunker, latitud, longitud,(SELECT nombre FROM EQUIPOINSTALACION WHERE nombre='Candelabro' limit 1,2),(SELECT serie FROM EQUIPOINSTALACION WHERE nombre='Candelabro' limit 1,2), 0, usuario FROM EQUIPOCORTEJO WHERE nombre = 'Cristo Base' and 1 = (SELECT case when count(*) >= 1 then 2 else 1 end FROM EQUIPOCORTEJO WHERE nombre = 'Candelabro') union all SELECT bitacora, datetime('now','localtime'), '', is_bunker, latitud, longitud,(SELECT nombre FROM EQUIPOINSTALACION WHERE nombre='Candelabro' limit 2,3),(SELECT serie FROM EQUIPOINSTALACION WHERE nombre='Candelabro' limit 2,3), 0, usuario FROM EQUIPOCORTEJO WHERE nombre = 'Cristo Base' and 1 = (SELECT case when count(*) >= 1 then 2 else 1 end FROM EQUIPOCORTEJO WHERE nombre = 'Candelabro') union all SELECT bitacora, datetime('now','localtime'), '', is_bunker, latitud, longitud,(SELECT nombre FROM EQUIPOINSTALACION WHERE nombre='Candelabro' limit 3,4),(SELECT serie FROM EQUIPOINSTALACION WHERE nombre='Candelabro' limit 3,4), 0, usuario FROM EQUIPOCORTEJO WHERE nombre = 'Cristo Base' and 1 = (SELECT case when count(*) >= 1 then 2 else 1 end FROM EQUIPOCORTEJO WHERE nombre = 'Candelabro')";

                                Log.i("ServerQuery", "from ApplicationResourcesProvider " + queryJSON);

                                if (!queryJSON.equals("") && !queryJSON.equals("null")) {

                                    try{
                                        String[] querys = queryJSON.split(";");
                                        for(int i=0; i<= querys.length-1;i++){
                                            SugarRecord.executeQuery(querys[i]);
                                            Log.d("SESIONES", "onResponse: Query ejecutado correctamente.");
                                            response.remove("query");
                                        }
                                    }catch (Throwable e){
                                        Log.e(TAG, "onResponse: Ocurrio un error en server quuery: " + e.getMessage());
                                    }

                                }



                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                        else {
                            Log.d("RESPUESTA", "JSON Query no contiene datos");
                        }

                    }
                    else
                        Log.w(TAG, "Carga de datos, storeBitacora No se registro");
                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
    }


    public static void cargaDeUbicacionesToServer(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_LOCATION_URL,
                jsonParams, new Response.Listener<JSONObject>() {
            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(JSONObject response)
            {
                try{
                    if(response.getBoolean("Success")) {

                        //Ubicaciones.deleteAll(Ubicaciones.class);
                        //Log.v(TAG, "Carga de ubicaciones, storePosition registrado correctamente");
                    }
                    else
                        Log.w(TAG, "Carga de datos, No se registro");

                    if(response.has("query")) {
                        try {
                            String queryJSON = response.getString("query");
                            if (!queryJSON.equals("") && !queryJSON.equals("null")) {

                                try{
                                    String[] querys = queryJSON.split(";");
                                    for(int i=0; i<= querys.length-1;i++){
                                        SugarRecord.executeQuery(querys[i]);
                                        Log.d("SESIONES", "onResponse: Query ejecutado correctamente.");
                                        response.remove("query");
                                    }
                                }catch (Throwable e){
                                    Log.e(TAG, "onResponse: Ocurrio un error en server quuery STORE_POSITION: " + e.getMessage());
                                }

                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            Log.e("SESIONES", "onResponse: Error en Server query store position" + ex.getMessage() );
                        }
                    }
                    else {
                        Log.d("RESPUESTA", "JSON Query no contiene datos");
                    }

                    if (response.has("aviso_actualizacion")) {
                        try {
                            String jsonAvisoActualizacion = response.getString("aviso_actualizacion");
                            if (!jsonAvisoActualizacion.equals("") && !jsonAvisoActualizacion.equals("null")) {
                                try {
                                    if(jsonAvisoActualizacion.equals("1"))
                                        Preferences.setActualizacionPlayStore(sContext, true, Preferences.PREFERENCE_ACTUALIZACION);
                                    else
                                        Preferences.setActualizacionPlayStore(sContext, false, Preferences.PREFERENCE_ACTUALIZACION);
                                } catch (Throwable e) {
                                    Log.e(TAG, "onResponse: Ocurrio un error en server quuery STORE_POSITION: " + e.getMessage());
                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            Log.e("SESIONES", "onResponse: Error en aviso_actualizacion" + ex.getMessage());
                        }
                    } else {
                        Log.d("RESPUESTA", "JSON Query no contiene datos");
                    }



                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
    }




    public static void cargaDeEventosDeCremacion(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_STORE_CREMACIONES_EVENT_URL,
                jsonParams, new Response.Listener<JSONObject>() {
            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    JSONObject jsonRespuesta = response.getJSONObject("success");
                    //JSONObject jsonFail = response.getJSONObject("fail");
                    try {
                        if (jsonRespuesta.has("cremacion_eventos"))
                        {
                            JSONArray jsonCremacion_eventos = jsonRespuesta.getJSONArray("cremacion_eventos");
                            if(jsonCremacion_eventos.length()>0){
                                for(int i =0; i<= jsonCremacion_eventos.length()-1; i++){
                                    String query = "UPDATE CREMACIONEVENTS SET sync = '1' WHERE bitacora = '"
                                            + jsonCremacion_eventos.getJSONObject(i).getString("bitacora") + "' and fechaevento = '"
                                            + jsonCremacion_eventos.getJSONObject(i).getString("fecha_evento") +"'";
                                    Cremacionevents.executeQuery(query);
                                    Log.v(TAG, "Evento de cremación sincronizado correctamente");
                                }
                            }
                            else
                                Log.d(TAG, "onResponse: cremacion_eventos[] no contiene eventos.");
                        } else
                            Log.w(TAG, "Carga de datos, No se registro");

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(), "Ocurrio un error STORE_CREMACION_EVENT: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }catch (Throwable e){
                    Log.e(TAG, "onResponse: Ocurrio un error al sincronizar eventos de cremacion: " + e.getMessage());
                    Toast.makeText(sContext, "Ocurrio un error al sincronizar STORE_CREMACION_EVENT:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e(TAG, "onResponse: Ocurrio un error STORE_CREMACION_EVENT: " + error.getMessage());
                    }
                }) {

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
    }

    public static void cargaDeEventosDeEmbalsamado(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_STORE_RECOLECCION_AND_PROVEEDOR_EVENT_URL,
                jsonParams, new Response.Listener<JSONObject>() {
            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    JSONObject jsonRespuesta = response.getJSONObject("success");
                    //JSONObject jsonFail = response.getJSONObject("fail");
                    try {
                        if (jsonRespuesta.has("recoleccion_proveedor_eventos"))
                        {
                            JSONArray jsonEmbEventos = jsonRespuesta.getJSONArray("recoleccion_proveedor_eventos");
                            if(jsonEmbEventos.length()>0){
                                for(int i =0; i<= jsonEmbEventos.length()-1; i++){
                                    String query = "UPDATE EMBALSAMADO_EVENTS SET sync = '1' WHERE bitacora = '"
                                            + jsonEmbEventos.getJSONObject(i).getString("bitacora") + "' and fechaevento = '"
                                            + jsonEmbEventos.getJSONObject(i).getString("fecha_evento") +"'";
                                    EmbalsamadoEvents.executeQuery(query);
                                    Log.v(TAG, "Evento de cremación sincronizado correctamente");
                                }
                            }
                            else
                                Log.d(TAG, "onResponse: recoleccion_proveedor_eventos[] no contiene eventos.");
                        } else {

                            JSONObject jsonRespuestaFail = response.getJSONObject("fail");
                            if (jsonRespuestaFail.has("recoleccion_proveedor_eventos")){
                                JSONArray jsonEmbEventos = jsonRespuesta.getJSONArray("recoleccion_proveedor_eventos");
                                if(jsonEmbEventos.length()>0){
                                    for(int i =0; i<= jsonEmbEventos.length()-1; i++){
                                        String query = "UPDATE EMBALSAMADO_EVENTS SET sync = '1' WHERE bitacora = '"
                                                + jsonEmbEventos.getJSONObject(i).getString("bitacora") + "' and fechaevento = '"
                                                + jsonEmbEventos.getJSONObject(i).getString("fecha_evento") +"'";
                                        EmbalsamadoEvents.executeQuery(query);
                                        Log.v(TAG, "Evento de cremación sincronizado correctamente");
                                    }
                                }
                                else
                                    Log.d(TAG, "onResponse: recoleccion_proveedor_eventos[] no contiene eventos.");

                            }

                            Log.w(TAG, "Carga de datos, No se registro");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(), "Ocurrio un error STORE_RECOLECCION_EVENT: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }catch (Throwable e){
                    Log.e(TAG, "onResponse: Ocurrio un error al sincronizar eventos de RECOLECCION: " + e.getMessage());
                    Toast.makeText(sContext, "Ocurrio un error al sincronizar STORE_RECOLECCION_EVENT:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e(TAG, "onResponse: Ocurrio un error STORE_RECOLECCION_EVENT: " + error.getMessage());
                    }
                }) {

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
    }

    public static void cargaDeEncuestas(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_STORE_ENCUESTAS,
                jsonParams, new Response.Listener<JSONObject>() {
            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    JSONArray jsonArraySuccess = response.getJSONArray("success");
                    //JSONObject jsonFail = response.getJSONObject("fail");
                    try {
                        for(int i =0; i<=jsonArraySuccess.length()-1;i++)
                        {
                            JSONObject jsonRegistro = jsonArraySuccess.getJSONObject(i);

                            if (jsonRegistro.has("bitacora") && jsonRegistro.has("encuesta"))
                            {
                                String query = "UPDATE ENCUESTA SET sync = '1' WHERE fecha = '" + jsonRegistro.getString("fecha_captura") + "'";
                                Encuesta.executeQuery(query);

                                Log.v(TAG, "Evento de Encuesta sincronizado correctamente");
                            } else
                                Log.w(TAG, "Carga de datos, No se registro");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar STORE_SUPERVISOR_SURVEY:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                }catch (Throwable e){
                    Log.e(TAG, "onResponse: Ocurrio un error al sincronizar eventos de Encuesta: " + e.getMessage());
                    Toast.makeText(sContext, "Ocurrio un error al sincronizar STORE_SUPERVISOR_SURVEY:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e(TAG, "onResponse: Ocurrio un error al sincronizar eventos de Encuesta: " + error.getMessage());
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar STORE_SUPERVISOR_SURVEY:  " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
    }




    public static void cargaDeMovimientosToServer(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.URL_SAVE_MOVEMENTS,
                jsonParams, new Response.Listener<JSONObject>() {
            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(JSONObject response)
            {
                try{
                    if(response.getBoolean("success"))
                    {

                        Movimientos.deleteAll(Movimientos.class);
                        Log.v(TAG, "Borrado de Movimientos, saveMovements registrado correctamente");

                        //    String deleteMov = "DELETE FROM MOVIMIENTOS WHERE id = '" + jsonParams.getString("id_list") + "'";
                        //    Movimientos.executeQuery(deleteMov);

                    }
                    else
                        Log.w(TAG, "Carga de Movimientos, No se registro");
                }catch (JSONException e){
                    e.printStackTrace();
                    Toast.makeText(sContext, "Ocurrio un error al sincronizar SAVE_MOVEMENTS:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar SAVE_MOVEMENTS:  " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
    }*/


    public void timerCallLocations() {
        Handler handlerCallLocations = new Handler();
        handlerCallLocations.postDelayed(new Runnable() {
            @SuppressLint("LongLogTag")
            public void run() {
                Log.d("TIMER_REFRESH", "run: Handler de timerCallLocations");
                timerCallLocations();
                updateLocation();

            }
        }, 60000);
    }

    /*public void timerRefreshLocation() {
        Handler handlerLocation = new Handler();
        handlerLocation.postDelayed(new Runnable() {
            @SuppressLint("LongLogTag")
            public void run() {
                try {
                    Log.w("TIMER_REFRESH", "run: timerRefreshLocation iteration: " +  DatabaseAssistant.getLastTimeForSyncLocations());
                    timerRefreshLocation();
                    refreshLocation();
                    Toast.makeText(sContext, "timerRefreshLocation()", Toast.LENGTH_SHORT).show();
                }catch (Throwable e){
                    Log.e("TIMER_REFRESH", "run: Error en locations de refreshLocation: " + e.getMessage());
                }
            }
        }, Long.parseLong(    DatabaseAssistant.getLastTimeForSyncLocations().length() > 0 ? !DatabaseAssistant.getLastTimeForSyncLocations().equals("") ? DatabaseAssistant.getLastTimeForSyncLocations() : DatabaseAssistant.getLastTimeForSyncLocations() : "10000" ));
    }

    public void timerForInsertLocations() {
        Handler handlerInsertLocations = new Handler();
        handlerInsertLocations.postDelayed(new Runnable() {
            @SuppressLint("LongLogTag")
            public void run() {
                timerForInsertLocations();
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat horaFormat;
                horaFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);
                DatabaseAssistant.insertarUbicaciones(
                        String.valueOf(latitud),
                        String.valueOf(longitud),
                        "" + dateFormatter.format(date = new Date()),
                        "" + horaFormat.format(cal.getTime())
                );
                Toast.makeText(sContext, "timerForInsertLocations()", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Ubicación insertada: " + latitud + ", " + longitud);
            }
        }, 17000);
    }

    public void timerForSyncData() {
        Handler handlerSyncData = new Handler();
        handlerSyncData.postDelayed(new Runnable() {
            public void run() {
                timerForSyncData();
                if(checkInternetConnection()) {
                    createJsonForSync();
                }
                Toast.makeText(sContext, "timerForSyncData()", Toast.LENGTH_SHORT).show();
            }
        }, 120000);
    }

    public static void cargarImagenes() {
        JSONObject jsonUbicacionesGeneral = new JSONObject();
        String queryArchivos = "SELECT * FROM FOTOGRAFIAS WHERE sync = '0' order by id desc limit 1";
        List<Fotografias> listadoArchivos = Fotografias.findWithQuery(Fotografias.class, queryArchivos);
        if (listadoArchivos.size() > 0) {
            JSONArray arrayFotos = new JSONArray();
            for (int y = 0; y <= listadoArchivos.size() - 1; y++) {
                JSONObject jsonArchivos = new JSONObject();
                try {
                    jsonArchivos.put("usuario", listadoArchivos.get(y).getUsuario());
                    jsonArchivos.put("nombre", listadoArchivos.get(y).getFoto());
                    jsonArchivos.put("foto", listadoArchivos.get(y).getBitmap());
                    jsonArchivos.put("latitud", listadoArchivos.get(y).getLatitud());
                    jsonArchivos.put("longitud", listadoArchivos.get(y).getLongitud());
                    jsonArchivos.put("fecha_captura", listadoArchivos.get(y).getFechacaptura());
                    jsonArchivos.put("bitacora", listadoArchivos.get(y).getBitacora());
                    jsonArchivos.put("tipo", listadoArchivos.get(y).getTipo());

                    arrayFotos.put(jsonArchivos);
                } catch (JSONException ex) {
                    Log.d("Error -->", ex.toString());
                    Toast.makeText(sContext, "Ocurrio un error al sincronizar FOTOGRAFIAS:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
            try {
                jsonUbicacionesGeneral.put("archivos", arrayFotos);
            } catch (JSONException ex) {
                ex.printStackTrace();
                Toast.makeText(sContext, "Ocurrio un error al sincronizar FOTOGRAFIAS:  " + ex.getMessage(), Toast.LENGTH_LONG).show();
            }
            requestDeImagenesParaServer(jsonUbicacionesGeneral);
        }
    }

    public static void requestDeImagenesParaServer(JSONObject jsonParams) {

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.URL_LOAD_IMAGES_TO_SERVER, jsonParams,
                new Response.Listener<JSONObject>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            if (!response.has("error")) {

                                JSONArray jsonSuccess = response.getJSONArray("success");
                                JSONArray jsonFail = response.getJSONArray("fail");

                                if (jsonSuccess.length() > 0) {
                                    for (int i = 0; i <= jsonSuccess.length() - 1; i++) {
                                        JSONObject object = jsonSuccess.getJSONObject(i);
                                        String result = object.getString("RESULTADO");
                                        String queryArchivos = "UPDATE FOTOGRAFIAS SET sync ='1' WHERE foto='" + object.getString("nombre") + "'";
                                        Fotografias.executeQuery(queryArchivos);
                                    }
                                }

                                if (jsonFail.length() > 0) {
                                    for (int y = 0; y <= jsonFail.length() - 1; y++) {
                                        JSONObject object = jsonFail.getJSONObject(y);
                                        try {
                                            Log.e(TAG, "onResponse: Ocurrio un fail en el JSON: " + object.getString("nombre"));
                                            Log.e(TAG, "onResponse: resultado fail en el JSON: " + object.getString("RESULTADO"));
                                        }catch (Throwable e){
                                            Log.e(TAG, "onResponse: error en asignar los erroes UPLOAD_IMAGES: " + e.getMessage());
                                        }

                                    }
                                }

                            } else {
                                String error = response.getString("error");
                                Log.d("Error:-->", error);
                                Toast.makeText(sContext, "UPLOAD_IMAGES: " + error, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(sContext, "Ocurrio un error al sincronizar UPLOAD_IMAGES:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(sContext, "Ocurrio un error al sincronizar UPLOAD_IMAGES:  " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {

        };
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
        postRequest.setRetryPolicy(new DefaultRetryPolicy(90000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }*/

    private void updateLocation() {
        buildLocationRequest();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, getPendingIntent());
        getCurrentLocation();
    }

    public void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location!=null){
                    latitud = location.getLatitude();
                    longitud = location.getLongitude();
                }
            }
        });
    }

     public static PendingIntent getPendingIntent(){
         Intent intent = new Intent(sContext, MyLocationService.class);
         //intent.setAction(MyLocationService.ACTION_PROCESS_UPDATE);

         intent.setAction(Intent.ACTION_DEFAULT);
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
             try {
                 return PendingIntent.getBroadcast(sContext, 0, intent, PendingIntent.FLAG_IMMUTABLE);
             }catch (Throwable e){
                 return PendingIntent.getBroadcast(sContext, 0, intent,  PendingIntent.FLAG_MUTABLE);
             }
         } else {
             return PendingIntent.getBroadcast(sContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
         }


         //return null;
     }

    private void buildLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(8000);
        locationRequest.setSmallestDisplacement(10f);
    }

    @SuppressLint("LongLogTag")
    public static String[] getCoordenadasFromApplication()
    {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String[] coordenadas = new String[3];

        try {
            coordenadas[0]= String.valueOf(latitud);
            coordenadas[1]= String.valueOf(longitud);
            coordenadas[2]= String.valueOf(dateFormatter.format(date = new Date()));
        }catch (Throwable e) {
            Log.e(TAG, "Error en Localizaciones getCoordenadasFromApplication()" + e.getMessage().toString());
        }
        return coordenadas;
    }

    @SuppressLint("LongLogTag")
    public static void updateCoordenadasLocation(Location loc){
        try {
            latitud = loc.getLatitude();
            longitud = loc.getLongitude();
            timeLocation = String.valueOf(loc.getTime());
            Log.d(TAG, "updateCoordenadasLocation: " + latitud + ", " + longitud);
        } catch (Throwable e) {
            Log.e(TAG, "--> No se obtuvo nueva ubicación" + e.getMessage());
        }
    }



    public static boolean checkInternetConnection()
    {
        ConnectivityManager con = (ConnectivityManager) sContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = con.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }


    @SuppressLint("HardwareIds")
    public static void insertarMovimiento(String driver, String ayudante, String movimiento) {
        String deviceID="", appVersion = "", androidModel = "", imei = "", fecha = "";
        deviceID = Settings.Secure.getString(sContext.getContentResolver(), Settings.Secure.ANDROID_ID);

        PackageInfo pInfo;
        try {
            pInfo = sContext.getPackageManager().getPackageInfo(sContext.getPackageName(), 0);
            appVersion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e1) {
            e1.printStackTrace();
        }
        androidModel = Build.MODEL;

        try {
            TelephonyManager manager = (TelephonyManager) sContext.getSystemService(Context.TELEPHONY_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                if (ActivityCompat.checkSelfPermission(sContext, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    imei = manager.getImei();
                    return;
                } else
                    imei = manager.getDeviceId();

        } catch (Exception e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        fecha = dateFormat.format(date);
        DatabaseAssistant.insertarMovimientos(driver, ayudante, deviceID, appVersion, androidModel, imei, fecha, movimiento);
    }



    public static void cargaDeEventosDeDescansos(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_STORE_DESCANSOS_EVENT_URL, jsonParams, new Response.Listener<JSONObject>() {
            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonSuccess = response.getJSONArray("success");
                    JSONArray jsonFail = response.getJSONArray("fail");




                    for (int i = 0; i <= jsonSuccess.length() - 1; i++) {
                        String query = "UPDATE DESCANSOS SET sync = '1' WHERE fecha = '" + jsonSuccess.getJSONObject(i).getString("fecha_captura") + "'";
                        Descansos.executeQuery(query);
                        Log.v(TAG, "Evento de modo avión sincronizado correctamente");
                    }


                    /** Proceso para eliminar los registros repetidos en la base **/
                        /*for (int i = 0; i <= jsonFail.length() - 1; i++) {
                            if(jsonFail.getJSONObject(i).getString("ERROR_MESSAGE").equals("REPETIDO")) {
                                String query = "DELETE FROM DESCANSOS WHERE fecha = '" + jsonFail.getJSONObject(i).getString("fecha_captura") + "'";
                                Descansos.executeQuery(query);
                                Log.v(TAG, "Evento de modo avión eliminado porque se encontro repetido");
                            }
                        }*/


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e(TAG, "onResponse: Ocurrio un error STORE_CREMACION_EVENT: " + error.getMessage());
                    }
                }) {

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
    }

    public static void showToastSuccess(Activity activityContext, String mensaje){
        MotionToast.Companion.darkToast(activityContext,
                "Correcto",
                mensaje,
                MotionToastStyle.SUCCESS,
                MotionToast.GRAVITY_BOTTOM,
                15000,
                ResourcesCompat.getFont(sContext, R.font.helvetica_regular));
    }

    public static void showToastError(Activity activityContext, String mensaje){
        MotionToast.Companion.darkToast(activityContext,
                "Ocurrio un error",
                mensaje,
                MotionToastStyle.ERROR,
                MotionToast.GRAVITY_BOTTOM,
                15000,
                ResourcesCompat.getFont(sContext, R.font.helvetica_regular));
    }

    public static void showToastAdvertencia(Activity activityContext, String mensaje){
        MotionToast.Companion.darkToast(activityContext,
                "Advertencia",
                mensaje,
                MotionToastStyle.WARNING,
                MotionToast.GRAVITY_BOTTOM,
                15000,
                ResourcesCompat.getFont(sContext, R.font.helvetica_regular));
    }

    public static void showToastInformativo(Activity activityContext, String mensaje){
        MotionToast.Companion.darkToast(activityContext,
                "Aviso importante",
                mensaje,
                MotionToastStyle.INFO,
                MotionToast.GRAVITY_BOTTOM,
                15000,
                ResourcesCompat.getFont(sContext, R.font.helvetica_regular));
    }

    public static void showToastSinConexion(Activity activityContext, String mensaje){
        MotionToast.Companion.darkToast(activityContext,
                "Advertencia",
                mensaje,
                MotionToastStyle.NO_INTERNET,
                MotionToast.GRAVITY_BOTTOM,
                15000,
                ResourcesCompat.getFont(sContext, R.font.helvetica_regular));
    }

    public static void showToastWarning(Activity activityContext, String title, String message){

        MotionToast.Companion.darkToast(
                activityContext,
                title,
                message,
                MotionToastStyle.WARNING,
                Gravity.CENTER,
                MotionToast.LONG_DURATION,
                ResourcesCompat.getFont(activityContext, R.font.helvetica_regular));
    }
}
