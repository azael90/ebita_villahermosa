package com.latinofuneral.bitacoras2020Villahermosa.Utils;


@SuppressWarnings("FieldCanBeLocal")
public final class BuildConfig {

    private static Branches targetBranch = Branches.VILLAHERMOSA;

    @SuppressWarnings("SpellCheckingInspection")
    public static enum Branches{VILLAHERMOSA, CIUDAD}

    private BuildConfig(){}

    public static Branches getTargetBranch(){
        return targetBranch;
    }

}
