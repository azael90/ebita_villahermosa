package com.latinofuneral.bitacoras2020Villahermosa.Utils;

public class ConstantsBitacoras extends BaseActivity{

    public static final String baseURL = "https://bitacoras.latinofuneral.com/ebita-vsa/public/ws";

    public static final int START_PRINTING = 1000;
    public static final int FINISHED_PRINTING = 1001;
    public static final int START_REQUEST = 2000;
    public static final int FINISHED_REQUEST = 2001;
    public static boolean finishPrinting = false;

    //** CATALOGOS DE DESCARGA NORMAL **//
    public static final String WS_DRIVERS_URL= baseURL +"/drivers";
    public static final String WS_PLACES_URL= baseURL + "/places";
    public static final String WS_NOTIFICATIONS_URL= baseURL + "/getNotifications";
    public static final String WS_VEHICLES_URL= baseURL + "/vehicles";
    public static final String WS_PARENTESCOS_URL= baseURL + "/parentescos";
    public static final String WS_BITACORA_DETAILS= baseURL + "/getCatalogoBitacoraDetalles";
    public static final String WS_BINNACLE_URL= baseURL + "/getCatalogoBitacoras";
    public static final String WS_DOWNLOAD_BITACORAS_COMPLETAS= baseURL + "/getCatalogoBitacorasPreconfiguradas";
    public static final String WS_DOWNLOAD_BITACORA_INDIVIDUAL= baseURL + "/getSingleBitacora";
    public static final String WS_SAVE_FACTURA_DOCUMENTS= baseURL + "/saveFacturaDocuments";
    public static final String WS_DOWNLOAD_ARTICULOS_CATALOGO= baseURL + "/getInventoryType";

    public static final String WS_STORE_DESCANSOS_EVENT_URL= baseURL + "/saveDescansosEventos";
    public static final String WS_CONFIGURATION= baseURL + "/getConfig";
    public static final String WS_CHECK_STATUS_BINNACLE= baseURL + "/getAuthorizationToEndBitacora";
    public static final String WS_CHECK_STATUS_ARTICLES= baseURL + "/getAuthorizationToCancelInventoryItem";
    public static final String WS_CHECK_STATUS_ATAURNA= baseURL + "/checkIfAtaurnaIsRegistered";
    public static final String WS_GET_LAST_CREMACION_EVENT= baseURL + "/getLastCremacionEvent";
    public static final String WS_LOGIN= baseURL + "/loginApp";
    public static final String WS_CHECKINCHECKOUT_NEW= baseURL + "/doCheckinAndCheckout";
    public static final String WS_SYNC_EDICION_DE_DESTINOS= baseURL + "/editBitacoraEvent";
    public static final String WS_GET_LAST_RECOLECCION_AND_PROVEEDOR_EVENT= baseURL + "/getLastRecoleccionAndProveedorEvent";

    /***** NEW PROCESSS UPDATE ***/
    public static final String URL_CHECK_VERSION= "https://bitacoras.latinofuneral.com/public/update/version.txt";
    public static final String URL_DOWNLOAD_APK=  "https://bitacoras.latinofuneral.com/public/update/";





    //** GUARDAR MOVIEMIENTOS REGISTRADOS **//
    public static final String WS_URL= baseURL + "/storeBitacora";
    public static final String WS_ASSETS_URL= baseURL + "/inventorys";
    public static final String WS_LOCATION_URL= baseURL + "/storePosition";
    public static final String WS_STORE_CREMACIONES_EVENT_URL= baseURL + "/storeCremacionEvent";
    public static final String WS_STORE_ENCUESTAS= baseURL + "/saveSupervisorSurvey";
    public static final String URL_SAVE_MOVEMENTS= baseURL + "/saveMovements";
    public static final String WS_CHECKIN_AND_CHECKOUT= baseURL + "/storeCheckiAndCheckout";
    public static final String WS_SAVE_INFORMATION_ADICIONAL_POR_BITACORA = baseURL + "/storeBitacoraDetails";
    public static final String WS_SAVE_EQUIPOS_VELACION = baseURL + "/equiposVelacionnnnn";
    public static final String WS_SAVE_ATAURNAS = baseURL + "/storeUrnaAndAtaud";
    public static final String WS_SAVE_ARTICLES_INVENTORY = baseURL + "/storeBitacoraAppliances";
    public static final String WS_SAVE_COMENTARIOS = baseURL + "/storeComments";
    public static final String WS_GET_COMENTARIOS = baseURL + "/getComments";
    public static final String URL_LOAD_IMAGES_TO_SERVER =  baseURL + "/uploadImagesSupervisorEncuesta";
    public static final String WS_STORE_RECOLECCION_AND_PROVEEDOR_EVENT_URL= baseURL + "/storeRecoleccionAndProveedorEvent";


}
