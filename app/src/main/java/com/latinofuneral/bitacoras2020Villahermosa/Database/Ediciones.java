package com.latinofuneral.bitacoras2020Villahermosa.Database;

import com.orm.SugarRecord;

public class Ediciones extends SugarRecord {
    String bitacora = "", fechaevento="", fechacaptura="", descripcion="", sync="";

    public Ediciones() {
    }

    public Ediciones(String bitacora, String fechaevento, String fechacaptura, String descripcion, String sync) {
        this.bitacora = bitacora;
        this.fechaevento = fechaevento;
        this.fechacaptura = fechacaptura;
        this.descripcion = descripcion;
        this.sync = sync;
    }

    public String getBitacora() {
        return bitacora;
    }

    public void setBitacora(String bitacora) {
        this.bitacora = bitacora;
    }

    public String getFechaevento() {
        return fechaevento;
    }

    public void setFechaevento(String fechaevento) {
        this.fechaevento = fechaevento;
    }

    public String getFechacaptura() {
        return fechacaptura;
    }

    public void setFechacaptura(String fechacaptura) {
        this.fechacaptura = fechacaptura;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }
}
