package com.latinofuneral.bitacoras2020Villahermosa.Database;

import com.orm.SugarRecord;

public class Cancelados extends SugarRecord
{
    String codigo ="", descripcion = "" , serie="",  fechaAtaurna="", proveedor = "", bitacora="", sync ="", fechaCancelacion="", fechaCaptura="";


    public Cancelados() {
    }

    public Cancelados(String codigo, String descripcion, String serie, String fechaAtaurna, String proveedor,
                      String bitacora, String sync, String fechaCancelacion, String fechaCaptura) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.serie = serie;
        this.fechaAtaurna = fechaAtaurna;
        this.proveedor = proveedor;
        this.bitacora = bitacora;
        this.sync = sync;
        this.fechaCancelacion = fechaCancelacion;
        this.fechaCaptura = fechaCaptura;
    }

    public String getFechaCaptura() {
        return fechaCaptura;
    }

    public void setFechaCaptura(String fechaCaptura) {
        this.fechaCaptura = fechaCaptura;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getBitacora() {
        return bitacora;
    }

    public void setBitacora(String bitacora) {
        this.bitacora = bitacora;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }


    public String getFechaAtaurna() {
        return fechaAtaurna;
    }

    public void setFechaAtaurna(String fechaAtaurna) {
        this.fechaAtaurna = fechaAtaurna;
    }

    public String getFechaCancelacion() {
        return fechaCancelacion;
    }

    public void setFechaCancelacion(String fechaCancelacion) {
        this.fechaCancelacion = fechaCancelacion;
    }
}
