package com.latinofuneral.bitacoras2020Villahermosa.Database;

import com.orm.SugarRecord;

public class ConceptosDescanso extends SugarRecord {
    String concepto="";

    public ConceptosDescanso() {
    }

    public ConceptosDescanso(String concepto) {
        this.concepto = concepto;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }
}
