package com.latinofuneral.bitacoras2020Villahermosa.Database;

import com.orm.SugarRecord;

public class Documentos extends SugarRecord {

    String bitacora ="", documento="", fecha ="", sync="", quien="";

    public Documentos() {
    }

    public Documentos(String bitacora, String documento, String fecha, String sync, String quien) {
        this.bitacora = bitacora;
        this.documento = documento;
        this.fecha = fecha;
        this.sync = sync;
        this.quien = quien;
    }

    public String getQuien() {
        return quien;
    }

    public void setQuien(String quien) {
        this.quien = quien;
    }

    public String getBitacora() {
        return bitacora;
    }

    public void setBitacora(String bitacora) {
        this.bitacora = bitacora;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }
}
