package com.latinofuneral.bitacoras2020Villahermosa.Database;

import com.orm.SugarRecord;

public class Fotosfirma extends SugarRecord {
    String inefrente="", ineatras="", firma="", fecha="", sync="", opciondefacturacion="", bitacora="";

    public Fotosfirma() {
    }

    public Fotosfirma(String inefrente, String ineatras, String firma, String fecha, String sync, String opciondefacturacion, String bitacora) {
        this.inefrente = inefrente;
        this.ineatras = ineatras;
        this.firma = firma;
        this.fecha = fecha;
        this.sync = sync;
        this.opciondefacturacion = opciondefacturacion;
        this.bitacora = bitacora;
    }

    public String getBitacora() {
        return bitacora;
    }

    public void setBitacora(String bitacora) {
        this.bitacora = bitacora;
    }

    public String getOpciondefacturacion() {
        return opciondefacturacion;
    }

    public void setOpciondefacturacion(String opciondefacturacion) {
        this.opciondefacturacion = opciondefacturacion;
    }

    public String getInefrente() {
        return inefrente;
    }

    public void setInefrente(String inefrente) {
        this.inefrente = inefrente;
    }

    public String getIneatras() {
        return ineatras;
    }

    public void setIneatras(String ineatras) {
        this.ineatras = ineatras;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }
}
