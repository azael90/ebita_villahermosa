package com.latinofuneral.bitacoras2020Villahermosa.Database;

import com.orm.SugarRecord;

public class Finado extends SugarRecord {
    String finado="", bitacora="";

    public Finado() {
    }

    public Finado(String finado, String bitacora) {
        this.finado = finado;
        this.bitacora = bitacora;
    }

    public String getFinado() {
        return finado;
    }

    public void setFinado(String finado) {
        this.finado = finado;
    }

    public String getBitacora() {
        return bitacora;
    }

    public void setBitacora(String bitacora) {
        this.bitacora = bitacora;
    }
}
