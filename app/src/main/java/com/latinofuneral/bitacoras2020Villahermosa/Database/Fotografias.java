package com.latinofuneral.bitacoras2020Villahermosa.Database;

import com.orm.SugarRecord;

public class Fotografias extends SugarRecord {
    String bitacora = "", foto="", bitmap ="", fechacaptura="", latitud="", longitud="", sync="", usuario="", tipo="";

    public Fotografias() {
    }

    public Fotografias(String bitacora, String foto, String bitmap, String fechacaptura, String latitud, String longitud, String sync, String usuario, String tipo) {
        this.bitacora = bitacora;
        this.foto = foto;
        this.bitmap = bitmap;
        this.fechacaptura = fechacaptura;
        this.latitud = latitud;
        this.longitud = longitud;
        this.sync = sync;
        this.usuario = usuario;
        this.tipo = tipo;
    }


    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public String getBitacora() {
        return bitacora;
    }

    public void setBitacora(String bitacora) {
        this.bitacora = bitacora;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getBitmap() {
        return bitmap;
    }

    public void setBitmap(String bitmap) {
        this.bitmap = bitmap;
    }

    public String getFechacaptura() {
        return fechacaptura;
    }

    public void setFechacaptura(String fechacaptura) {
        this.fechacaptura = fechacaptura;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String longitud) {
        this.tipo = tipo;
    }
}
