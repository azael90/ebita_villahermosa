package com.latinofuneral.bitacoras2020Villahermosa.Database;

import com.orm.SugarRecord;

public class Parentescos extends SugarRecord {
    String descripcion="", idserver="";

    public Parentescos() {
    }

    public Parentescos(String descripcion, String idserver) {
        this.descripcion = descripcion;
        this.idserver = idserver;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getIdserver() {
        return idserver;
    }

    public void setIdserver(String idserver) {
        this.idserver = idserver;
    }
}
