package com.latinofuneral.bitacoras2020Villahermosa.Database;

import com.orm.SugarRecord;

public class Inventario extends SugarRecord {
    String codigo="", descripcion="", serie="", fechaataurna="", proveedor ="", sync="", bitacora="", borrado="", fechacaptura="", latitud="", longitud ="", tipomovimiento="", serieataurna="";

    public Inventario() {
    }

    public Inventario(String codigo, String descripcion, String serie, String fechaataurna,
                      String proveedor, String sync, String bitacora, String borrado, String fechacaptura, String latitud, String longitud, String tipomovimiento, String serieataurna) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.serie = serie;
        this.fechaataurna = fechaataurna;
        this.proveedor = proveedor;
        this.sync = sync;
        this.bitacora = bitacora;
        this.borrado = borrado;
        this.fechacaptura = fechacaptura;
        this.latitud = latitud;
        this.longitud = longitud;

        this.tipomovimiento = tipomovimiento;
        this.serieataurna = serieataurna;

    }

    public String getTipomovimiento() {
        return tipomovimiento;
    }

    public void setTipomovimiento(String tipomovimiento) {
        this.tipomovimiento = tipomovimiento;
    }

    public String getSerieataurna() {
        return serieataurna;
    }

    public void setSerieataurna(String serieataurna) {
        this.serieataurna = serieataurna;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getFechaAtaurna() {
        return fechaataurna;
    }

    public void setFechaAtaurna(String fechaataurna) {
        this.fechaataurna = fechaataurna;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public String getBitacora() {
        return bitacora;
    }

    public void setBitacora(String bitacora) {
        this.bitacora = bitacora;
    }

    public String getBorrado() {
        return borrado;
    }

    public void setBorrado(String borrado) {
        this.borrado = borrado;
    }

    public String getFechaCaptura() {
        return fechacaptura;
    }

    public void setFechaCaptura(String fechacaptura) {
        this.fechacaptura = fechacaptura;
    }
}
