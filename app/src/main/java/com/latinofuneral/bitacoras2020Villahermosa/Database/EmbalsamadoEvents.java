package com.latinofuneral.bitacoras2020Villahermosa.Database;

import com.orm.SugarRecord;

public class EmbalsamadoEvents extends SugarRecord {
    /***
     *   "bitacora": "VSA21SEP001",
     *       "tipo_evento": "1",
     *       "fecha_evento": "2021-09-27 13:43:20",
     *       "usuario": "C9999",
     *       "quien_recibe_cenizas": ""
     */
    String bitacora="", tipoevento="", nombreevento="", fechaevento="", usuario="", recibecenizas="", sync = "";

    public EmbalsamadoEvents() {
    }

    public EmbalsamadoEvents(String bitacora, String tipoevento, String nombreevento, String fechaevento, String usuario, String recibecenizas, String sync) {
        this.bitacora = bitacora;
        this.tipoevento = tipoevento;
        this.nombreevento = nombreevento;
        this.fechaevento = fechaevento;
        this.usuario = usuario;
        this.recibecenizas = recibecenizas;
        this.sync = sync;
    }

    public String getBitacora() {
        return bitacora;
    }

    public void setBitacora(String bitacora) {
        this.bitacora = bitacora;
    }

    public String getTipoevento() {
        return tipoevento;
    }

    public void setTipoevento(String tipoevento) {
        this.tipoevento = tipoevento;
    }

    public String getNombreevento() {
        return nombreevento;
    }

    public void setNombreevento(String nombreevento) {
        this.nombreevento = nombreevento;
    }

    public String getFechaevento() {
        return fechaevento;
    }

    public void setFechaevento(String fechaevento) {
        this.fechaevento = fechaevento;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getRecibecenizas() {
        return recibecenizas;
    }

    public void setRecibecenizas(String recibecenizas) {
        this.recibecenizas = recibecenizas;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }
}
