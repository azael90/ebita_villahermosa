package com.latinofuneral.bitacoras2020Villahermosa.Database;

import com.orm.SugarRecord;

public class Cremacionevents extends SugarRecord {
    /***
     *   "bitacora": "VSA21MAR016",
     *       "tipo_evento": "1",
     *       "fecha_evento": "2021-06-24 17:41:30",
     *       "usuario": "C9999",
     *       "quien_recibe_cenizas": ""
     */
    String bitacora="", tipoevento="", fechaevento="", usuario="", recibecenizas="", sync = "";

    public Cremacionevents() {
    }

    public Cremacionevents(String bitacora, String tipoevento, String fechaevento, String usuario, String recibecenizas, String sync) {
        this.bitacora = bitacora;
        this.tipoevento = tipoevento;
        this.fechaevento = fechaevento;
        this.usuario = usuario;
        this.recibecenizas = recibecenizas;
        this.sync = sync;
    }

    public String getBitacora() {
        return bitacora;
    }

    public void setBitacora(String bitacora) {
        this.bitacora = bitacora;
    }

    public String getTipoevento() {
        return tipoevento;
    }

    public void setTipoevento(String tipoevento) {
        this.tipoevento = tipoevento;
    }

    public String getFechaevento() {
        return fechaevento;
    }

    public void setFechaevento(String fechaevento) {
        this.fechaevento = fechaevento;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getRecibecenizas() {
        return recibecenizas;
    }

    public void setRecibecenizas(String recibecenizas) {
        this.recibecenizas = recibecenizas;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }
}
