package com.latinofuneral.bitacoras2020Villahermosa.Database;

import com.orm.SugarRecord;

public class Descansos extends SugarRecord {
    String usuario="", concepto="" , fecha="", latitud="", longitud="", sync="", tipo="", isBunker="", geofenceActual="";

    public Descansos() {
    }

    public Descansos(String usuario, String concepto, String fecha, String latitud, String longitud,
                     String sync, String tipo, String isBunker, String geofenceActual) {
        this.usuario = usuario;
        this.concepto = concepto;
        this.fecha = fecha;
        this.latitud = latitud;
        this.longitud = longitud;
        this.sync = sync;
        this.tipo = tipo;
        this.isBunker = isBunker;
        this.geofenceActual = geofenceActual;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getIsBunker() {
        return isBunker;
    }

    public void setIsBunker(String isBunker) {
        this.isBunker = isBunker;
    }

    public String getGeofenceActual() {
        return geofenceActual;
    }

    public void setGeofenceActual(String geofenceActual) {
        this.geofenceActual = geofenceActual;
    }
}
