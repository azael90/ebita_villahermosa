package com.latinofuneral.bitacoras2020Villahermosa.Printer;

import android.bluetooth.BluetoothAdapter;

import com.latinofuneral.bitacoras2020Villahermosa.Database.DatabaseAssistant;

//import com.pabs.panteon.database.DatabaseAssistant;


public class BluetoothPrinter {

    private static BluetoothPrinter mInstance = new BluetoothPrinter();
    private TSCPrinter tscActivity;
    public static boolean printerConnected;

    private BluetoothPrinter()
    {
        printerConnected = false;

        tscActivity = new TSCPrinter();
    }

    public static BluetoothPrinter getInstance()
    {
        return mInstance;
    }

    public void connect() throws Exception
    {
        if (isBluetoothAvailable() && !isPrinterConnected()) {
            //tscActivity.openport("00:19:0E:A4:16:E0");
            tscActivity.openport(DatabaseAssistant.getMacAddress());
        }
        else
            printerConnected = true;
    }

    public void disconnect()
    {
        if (isBluetoothAvailable()) {
            //tscActivity.closeport(300);
            tscActivity.closeport();
        }

        BluetoothPrinter.printerConnected = false;
    }

    public void setup(int width, int height, int speed, int density, int sensor, int sensor_distance, int sensor_offset) throws Exception
    {
        tscActivity.setup(width, height, speed, density, sensor, sensor_distance, sensor_offset);
    }

    public void sendcommand(String message) throws Exception
    {
        tscActivity.sendcommand(message);
    }

    public void printQRCode(String bitacora) throws Exception
    {
        tscActivity.barcode(120, 150, "128", 90, 0, 0, 2, 2, bitacora);
    }

    public void sendcommand(byte[] message) throws Exception
    {
        tscActivity.sendcommand(message);
    }

    public String sendcommand_getString(String command) throws Exception
    {
        return tscActivity.sendcommand_getstring(command);
    }

    public void clearbuffer()
    {
        tscActivity.clearbuffer();
    }

    public void printerfont(int x, int y, String size, int rotation, int x_multiplication, int y_multiplication, String message) throws Exception
    {
        tscActivity.printerfont(x, y, size, rotation, x_multiplication, y_multiplication, message);
    }

    public void printlabel(int quantity, int copy) throws Exception
    {
        tscActivity.printlabel(quantity, copy);
    }

    public static boolean isBluetoothAvailable()
    {
        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        return (bluetoothAdapter != null && bluetoothAdapter.isEnabled());
    }

    public boolean isPrinterConnected() throws Exception
    {
        return tscActivity.isSocketConnected();
    }
}
