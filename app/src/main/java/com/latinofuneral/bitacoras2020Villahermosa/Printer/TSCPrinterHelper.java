package com.latinofuneral.bitacoras2020Villahermosa.Printer;

import android.os.Looper;

import com.latinofuneral.bitacoras2020Villahermosa.Database.DatabaseAssistant;

import java.io.IOException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


public class TSCPrinterHelper {

    private static TSCPrinter tscActivity;
    private static long dateAux = new Date().getTime();
    private static Timer timerToCancelPrinting;
    private static OnCheckPrinterStatusErrorListener onCheckPrinterStatusErrorListener;

    public static TSCPrinter getOpenPort() throws Exception {
        if (tscActivity == null) {

            /*Util.Log.ih("NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL\n" +
                    "NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL\n" +
                    "NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL");*/

            if (Looper.myLooper() == null){
                Looper.prepare();
            }

            tscActivity = new TSCPrinter();
        }
        else {
           /* Util.Log.ih("NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL\n" +
                    "NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL,\n" +
                    "NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL");*/

            return tscActivity;
        }

        timerToCancelPrinting = new Timer();
        timerToCancelPrinting.schedule(new TimerTask() {

            int attempt;
            //TSCPrinter tscDll;

            @Override
            public void run() {
                if (checkTime()) {
                    //Util.Log.ih("Closing port");
                    try {
                       // Util.Log.ih("CLOSING PORT OPENED -----------");
                        TSCPrinterHelper.cancelConnection();
                    } catch (Exception e) {
                        e.printStackTrace();
                        timerToCancelPrinting.cancel();
                    }
                }
                //Util.Log.ih("Hora, attempt: " + attempt);
            }

            public TimerTask setData(TSCPrinter tscDll) {
                //this.tscDll = tscDll;
                return this;
            }
        }, (1000 * 60));

        tscActivity.openport(DatabaseAssistant.getMacAddress());
        //tscActivity.openport("00:19:0E:A4:16:E0");

        timerToCancelPrinting.cancel();
       // Util.Log.ih("PORT OPENED -----------------------------");

        return tscActivity;
    }

    public static void cancelConnection(){
        //tscActivity.closeport();

        if (tscActivity != null) {
            tscActivity.closeport();
            tscActivity = null;

            //Util.Log.ih("PORT CLOSED -----------------------------");
        }
    }

    public static void printSELFTEST() {
        tscActivity.printSELFTEST();
    }

    public static String getPrinterStatus() throws IOException {
        return tscActivity.status();
    }

    public static String getPrinterBatch() {
        return tscActivity.batch();
    }

    private static void checkPrinterStatusForError(){
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    //Util.Log.ih("check printer status...");
                    //Util.Log.ih("tscActivity.status() = " + tscActivity.status());
                    switch (tscActivity.status()) {
                        case "Head Open":
                        case "Ribbon Jam":
                        case "Ribbon Empty":
                        case "No Paper":
                        case "Paper Jam":
                        case "Paper Empty":
                        case "Pause":
                            onCheckPrinterStatusErrorListener.OnCheckPrinterStatusError(true, 1);
                            timer.cancel();
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 500, 500);
    }

    public static void setOnCheckPrinterStatusErrorListener(OnCheckPrinterStatusErrorListener listener){
        onCheckPrinterStatusErrorListener = listener;
        checkPrinterStatusForError();
    }

    public interface OnCheckPrinterStatusErrorListener {
        public void OnCheckPrinterStatusError(boolean error, int errorType);
    }

    /**
     * checks if more than 59 seconds have passed
     * @return
     *          true -> difference is more than limit
     *          false -> difference is under limit
     */
    public static boolean checkTime() {
        long newTime = new Date().getTime();
        long differences = Math.abs(dateAux - newTime);
        return (differences > (1000 * 59));
    }

}
