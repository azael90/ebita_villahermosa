package com.latinofuneral.bitacoras2020Villahermosa.Printer;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Environment;
import android.util.Log;

import com.latinofuneral.bitacoras2020Villahermosa.Database.DatabaseAssistant;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

//import com.pabs.panteon.util.Util;

public class TSCPrinter {
    private static final String TAG = "THINBTCLIENT";
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothSocket btSocket = null;
    private OutputStream OutStream = null;
    private InputStream InStream = null;
    private byte[] buffer = new byte[1024];
    private byte[] readBuf = new byte[1024];
    private String printerstatus = "";
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static String address = DatabaseAssistant.getMacAddress();
    private static String receive_data = "";

    public TSCPrinter() {
        Log.e("THINBTCLIENT", "+++ ON CREATE +++");
        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(this.mBluetoothAdapter == null) {
            Log.e(TAG, "Bluetooth is not available.");
        } else if(!this.mBluetoothAdapter.isEnabled()) {
            Log.e(TAG, "Please enable your BT and re-run this program.");
        } else {
            Log.e(TAG, "+++ DONE IN ON CREATE, GOT LOCAL BT ADAPTER +++");
        }
    }

    /*
    public void onStart() {
        //super.onStart();
        Log.e("THINBTCLIENT", "++ ON START ++");
    }
    */

    /*
    public void onResume() {
        super.onResume();
        Log.e("THINBTCLIENT", "+ ON RESUME +");
        Log.e("THINBTCLIENT", "+ ABOUT TO ATTEMPT CLIENT CONNECT +");
        this.test1 = (TextView)this.findViewById(2131165184);
        this.b1 = (Button)this.findViewById(2131165185);
        this.b1.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                TSCActivity.this.openport("00:19:0E:A0:04:E1");
                TSCActivity.this.sendcommand("PRINT 10\n");
            }
        });
        this.b2 = (Button)this.findViewById(2131165186);
        this.b2.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String QQ = TSCActivity.this.batch();
                TSCActivity.this.test1.setText(QQ);
            }
        });
    }
    */

    /*public void flushOutStream() {
        if(this.OutStream != null) {
            try {
                Util.Log.ih("gettin flushOutStream...");
                this.OutStream = this.btSocket.getOutputStream();
                Util.Log.ih("gettin flushOutStream... DONE");
                //this.OutStream.flush();
            } catch (IOException var3) {
                Log.e(TAG, "ON PAUSE: Couldn\'t flush output stream.", var3);
            }
        }

        /*
        try {
            this.btSocket.close();
        } catch (IOException var2) {
            Log.e("THINBTCLIENT", "ON PAUSE: Unable to close socket.", var2);
        }
        */

    //}

    public void openport(String address) {
        BluetoothDevice device = null;
        BluetoothAdapter mBluetoothAdapter = null;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        device = mBluetoothAdapter.getRemoteDevice(address);

        /*
        try {
            Util.Log.ih("doing bluetooth stuff...");
            BluetoothSocket bs = device.createInsecureRfcommSocketToServiceRecord(MY_UUID);
            Field f = bs.getClass().getDeclaredField("mFdHandle");
            f.setAccessible(true);
            f.set(bs, 0x8000);
            bs.close();
            Thread.sleep(2000); // Just in case the socket was really connected
            Util.Log.ih("finish doing bluetooth stuff");
        } catch (Exception e) {
            Log.e(TAG, "Reset Failed", e);
        }
        */

        try {
            this.btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException var8) {
            ;
        }

        mBluetoothAdapter.cancelDiscovery();

        try {
            this.btSocket.connect();
            this.OutStream = this.btSocket.getOutputStream();
            this.InStream = this.btSocket.getInputStream();
        } catch (IOException var7) {
            try {
                this.btSocket.close();
            } catch (IOException var6) {
                ;
            }
        }
    }

    public boolean isSocketConnected()
    {
        try {
            OutStream.write(new byte[1024]);

            Log.d("OutStream", "No Exception");

            return true;
        }
        catch (Exception ex)
        {
            Log.d("OutStream", "Exception");

            ex.printStackTrace();

            BluetoothPrinter.printerConnected = false;
            OutStream = null;
        }

        return false;
    }

    public void sendcommand(String message) throws IOException {
        byte[] msgBuffer = message.getBytes();

        try {
            this.OutStream.write(msgBuffer);
        } catch (IOException var4) {
            Log.e(TAG, "ON RESUME: Exception during write.", var4);
            throw new IOException(var4.getMessage());
        }

    }

    public void sendcommand(byte[] message) {
        try {
            this.OutStream.write(message);
        } catch (IOException var3) {
            Log.e(TAG, "ON RESUME: Exception during write.", var3);
        }
    }

    public String sendcommand_getstring(String message) {
        readBuf = new byte[1024];
        String end_judge = "OUT \"ENDLINE\"\r\n";
        byte[] msgBuffer = message.getBytes();
        byte[] msgBuffer1 = "\r\n".getBytes();
        byte[] msgBuffer2 = end_judge.getBytes();

        try {
            this.OutStream.write(msgBuffer);
            this.OutStream.write(msgBuffer1);
            this.OutStream.write(msgBuffer2);
        } catch (IOException var7) {
            return "-1";
        }

        try {
            while(!this.ReadStream_judge()) {
                ;
            }
        } catch (Exception var8) {
            return "-1";
        }

        return receive_data;
    }

    private boolean ReadStream_judge() {
        receive_data = "";

        do {
            try {
                Thread.sleep(300L);
            } catch (InterruptedException var2) {
                var2.printStackTrace();
            }

            try {
                while(this.InStream.available() > 0) {
                    int length = this.InStream.read(readBuf);
                    receive_data = receive_data + new String(readBuf);
                }
            } catch (IOException var3) {
                return false;
            }
        } while(!receive_data.contains("ENDLINE\r\n"));

        receive_data = receive_data.replace("ENDLINE\r\n", "");
        return true;
    }

    public String status() throws IOException {
        byte[] message = new byte[]{(byte)27, (byte)33, (byte)83};

        try {

            this.OutStream.write(message);
        } catch (IOException var4) {
            throw new IOException(var4.getMessage());
        } catch (NullPointerException var5){
            throw new NullPointerException(var5.getMessage());
        }

        try {
            Thread.sleep(1000L);
        } catch (InterruptedException var3) {
            var3.printStackTrace();
        }


        int tim;

        try {
            while(this.InStream.available() > 0) {
                this.readBuf = new byte[1024];
                tim = this.InStream.read(this.readBuf);
            }
        } catch (IOException var5) {
            throw new IOException(var5.getMessage());
        }


        if(this.readBuf[0] == 2 && this.readBuf[5] == 3) {
            for(tim = 0; tim <= 7; ++tim) {
                if(this.readBuf[tim] == 2 && this.readBuf[tim + 1] == 64 && this.readBuf[tim + 2] == 64 && this.readBuf[tim + 3] == 64 && this.readBuf[tim + 4] == 64 && this.readBuf[tim + 5] == 3) {
                    this.printerstatus = "Ready";
                    this.readBuf = new byte[1024];
                    break;
                }

                if(this.readBuf[tim] == 2 && this.readBuf[tim + 1] == 69 && this.readBuf[tim + 2] == 64 && this.readBuf[tim + 3] == 64 && this.readBuf[tim + 4] == 96 && this.readBuf[tim + 5] == 3) {
                    this.printerstatus = "Head Open";
                    this.readBuf = new byte[1024];
                    break;
                }

                if(this.readBuf[tim] == 2 && this.readBuf[tim + 1] == 64 && this.readBuf[tim + 2] == 64 && this.readBuf[tim + 3] == 64 && this.readBuf[tim + 4] == 96 && this.readBuf[tim + 5] == 3) {
                    this.printerstatus = "Head Open";
                    this.readBuf = new byte[1024];
                    break;
                }

                if(this.readBuf[tim] == 2 && this.readBuf[tim + 1] == 69 && this.readBuf[tim + 2] == 64 && this.readBuf[tim + 3] == 64 && this.readBuf[tim + 4] == 72 && this.readBuf[tim + 5] == 3) {
                    this.printerstatus = "Ribbon Jam";
                    this.readBuf = new byte[1024];
                    break;
                }

                if(this.readBuf[tim] == 2 && this.readBuf[tim + 1] == 69 && this.readBuf[tim + 2] == 64 && this.readBuf[tim + 3] == 64 && this.readBuf[tim + 4] == 68 && this.readBuf[tim + 5] == 3) {
                    this.printerstatus = "Ribbon Empty";
                    this.readBuf = new byte[1024];
                    break;
                }

                if(this.readBuf[tim] == 2 && this.readBuf[tim + 1] == 69 && this.readBuf[tim + 2] == 64 && this.readBuf[tim + 3] == 64 && this.readBuf[tim + 4] == 65 && this.readBuf[tim + 5] == 3) {
                    this.printerstatus = "No Paper";
                    this.readBuf = new byte[1024];
                    break;
                }

                if(this.readBuf[tim] == 2 && this.readBuf[tim + 1] == 69 && this.readBuf[tim + 2] == 64 && this.readBuf[tim + 3] == 64 && this.readBuf[tim + 4] == 66 && this.readBuf[tim + 5] == 3) {
                    this.printerstatus = "Paper Jam";
                    this.readBuf = new byte[1024];
                    break;
                }

                if(this.readBuf[tim] == 2 && this.readBuf[tim + 1] == 69 && this.readBuf[tim + 2] == 64 && this.readBuf[tim + 3] == 64 && this.readBuf[tim + 4] == 65 && this.readBuf[tim + 5] == 3) {
                    this.printerstatus = "Paper Empty";
                    this.readBuf = new byte[1024];
                    break;
                }

                if(this.readBuf[tim] == 2 && this.readBuf[tim + 1] == 67 && this.readBuf[tim + 2] == 64 && this.readBuf[tim + 3] == 64 && this.readBuf[tim + 4] == 64 && this.readBuf[tim + 5] == 3) {
                    this.printerstatus = "Cutting";
                    this.readBuf = new byte[1024];
                    break;
                }

                if(this.readBuf[tim] == 2 && this.readBuf[tim + 1] == 75 && this.readBuf[tim + 2] == 64 && this.readBuf[tim + 3] == 64 && this.readBuf[tim + 4] == 64 && this.readBuf[tim + 5] == 3) {
                    this.printerstatus = "Waiting to Press Print Key";
                    this.readBuf = new byte[1024];
                    break;
                }

                if(this.readBuf[tim] == 2 && this.readBuf[tim + 1] == 76 && this.readBuf[tim + 2] == 64 && this.readBuf[tim + 3] == 64 && this.readBuf[tim + 4] == 64 && this.readBuf[tim + 5] == 3) {
                    this.printerstatus = "Waiting to Take Label";
                    this.readBuf = new byte[1024];
                    break;
                }

                if(this.readBuf[tim] == 2 && this.readBuf[tim + 1] == 80 && this.readBuf[tim + 2] == 64 && this.readBuf[tim + 3] == 64 && this.readBuf[tim + 4] == 64 && this.readBuf[tim + 5] == 3) {
                    this.printerstatus = "Printing Batch";
                    this.readBuf = new byte[1024];
                    break;
                }

                if(this.readBuf[tim] == 2 && this.readBuf[tim + 1] == 96 && this.readBuf[tim + 2] == 64 && this.readBuf[tim + 3] == 64 && this.readBuf[tim + 4] == 64 && this.readBuf[tim + 5] == 3) {
                    this.printerstatus = "Pause";
                    this.readBuf = new byte[1024];
                    break;
                }

                if(this.readBuf[tim] == 2 && this.readBuf[tim + 1] == 69 && this.readBuf[tim + 2] == 64 && this.readBuf[tim + 3] == 64 && this.readBuf[tim + 4] == 64 && this.readBuf[tim + 5] == 3) {
                    this.printerstatus = "Pause";
                    this.readBuf = new byte[1024];
                    break;
                }
            }
        }

        return this.printerstatus;
    }

    public String batch() {
        boolean printvalue = false;
        String printbatch = "";
        String stringbatch = "";
        String message = "~HS";
        byte[] batcharray = new byte[8];
        byte[] msgBuffer = message.getBytes();

        try {
            this.OutStream.write(msgBuffer);
        } catch (IOException var9) {
            Log.e(TAG, "ON RESUME: Exception during write.", var9);
        }

        try {
            Thread.sleep(100L);
        } catch (InterruptedException var8) {
            var8.printStackTrace();
        }

        try {
            while(this.InStream.available() > 0) {
                this.readBuf = new byte[1024];
                int e = this.InStream.read(this.readBuf);
            }
        } catch (IOException var10) {
            var10.printStackTrace();
        }

        if(this.readBuf[0] == 2) {
            System.arraycopy(this.readBuf, 55, batcharray, 0, 8);
            stringbatch = new String(batcharray);
            int printvalue1 = Integer.parseInt(stringbatch);
            printbatch = Integer.toString(printvalue1);
        }

        return printbatch;
    }

    public void closeport() {
        try {
            if (btSocket != null)
                this.btSocket.close();
        } catch (IOException var2) {
            var2.printStackTrace();
        }

    }

    public void setup(int width, int height, int speed, int density, int sensor, int sensor_distance, int sensor_offset) throws IOException, NullPointerException {
        String message = "";
        String size = "SIZE " + width + " mm" + ", " + height + " mm";
        String speed_value = "SPEED " + speed;
        String density_value = "DENSITY " + density;
        String sensor_value = "";
        if(sensor == 0) {
            sensor_value = "GAP " + sensor_distance + " mm" + ", " + sensor_offset + " mm";
        } else if(sensor == 1) {
            sensor_value = "BLINE " + sensor_distance + " mm" + ", " + sensor_offset + " mm";
        }

        message = size + "\n" + speed_value + "\n" + density_value + "\n" + sensor_value + "\n";
        byte[] msgBuffer = message.getBytes();

        try {
            this.OutStream.write(msgBuffer);
        } catch (IOException var15) {
            var15.printStackTrace();
            throw new IOException(var15.getMessage());
        } catch (NullPointerException var16){
            throw new NullPointerException(var16.getMessage());
        }

    }

    public void clearbuffer() {
        String message = "CLS\n";
        byte[] msgBuffer = message.getBytes();

        try {
            this.OutStream.write(msgBuffer);
        } catch (IOException var4) {
            var4.printStackTrace();
        }

    }

    public void barcode(int x, int y, String type, int height, int human_readable, int rotation, int narrow, int wide, String string) {
        String message = "";
        String barcode = "BARCODE ";
        String position = x + "," + y;
        String mode = "\"" + type + "\"";
        String height_value = "" + height;
        String human_value = "" + human_readable;
        String rota = "" + rotation;
        String narrow_value = "" + narrow;
        String wide_value = "" + wide;
        String string_value = "\"" + string + "\"";
        message = barcode + position + " ," + mode + " ," + height_value + " ," + human_value + " ," + rota + " ," + narrow_value + " ," + wide_value + " ," + string_value + "\n";
        byte[] msgBuffer = message.getBytes();

        try {
            this.OutStream.write(msgBuffer);
        } catch (IOException var22) {
            var22.printStackTrace();
        }

    }

    public void printSELFTEST() {
        String message = "SELFTEST\n";
        byte[] msgBuffer = message.getBytes();

        try {
            this.OutStream.write(msgBuffer);
        } catch (IOException var18) {
            var18.printStackTrace();
        }
    }

    /*public void printKILL() {
        String message = "KILL *";
        byte[] msgBuffer = message.getBytes();

        try {
            Util.Log.ih("printKILL method...");
            this.OutStream.write(msgBuffer);
            Util.Log.ih("printKILL method... DONE");
        } catch (IOException var18) {
            var18.printStackTrace();
        }
    }*/

    public void printerfont(int x, int y, String size, int rotation, int x_multiplication, int y_multiplication, String string) {
        String message = "";
        String text = "TEXT ";
        String position = x + "," + y;
        String size_value = "\"" + size + "\"";
        String rota = "" + rotation;
        String x_value = "" + x_multiplication;
        String y_value = "" + y_multiplication;
        String string_value = "\"" + string + "\"";
        message = text + position + " ," + size_value + " ," + rota + " ," + x_value + " ," + y_value + " ," + string_value + "\n";
        byte[] msgBuffer = message.getBytes();

        try {
            this.OutStream.write(msgBuffer);
        } catch (IOException var18) {
            var18.printStackTrace();
        }

    }

    public void printlabel(int quantity, int copy) throws IOException {
        String message = "";
        message = "PRINT " + quantity + ", " + copy + "\n";
        byte[] msgBuffer = message.getBytes();

        try {
            this.OutStream.write(msgBuffer);
        } catch (IOException var6) {
            throw new IOException(var6.getMessage());
        }
    }

    public void formfeed() {
        String message = "";
        message = "FORMFEED\n";
        byte[] msgBuffer = message.getBytes();

        try {
            this.OutStream.write(msgBuffer);
        } catch (IOException var4) {
            var4.printStackTrace();
        }

    }

    public void nobackfeed() {
        String message = "";
        message = "SET TEAR OFF\n";
        byte[] msgBuffer = message.getBytes();

        try {
            this.OutStream.write(msgBuffer);
        } catch (IOException var4) {
            var4.printStackTrace();
        }

    }

    public void sendfile(String filename) {
        try {
            FileInputStream fis = new FileInputStream("/sdcard/Download/" + filename);
            byte[] data = new byte[fis.available()];
            int[] FF = new int[data.length];

            while(fis.read(data) != -1) {
                ;
            }

            this.OutStream.write(data);
            fis.close();
        } catch (Exception var5) {
            ;
        }

    }

    public void downloadpcx(String filename) {
        try {
            FileInputStream fis = new FileInputStream("/sdcard/Download/" + filename);
            byte[] data = new byte[fis.available()];
            int[] FF = new int[data.length];
                String download = "DOWNLOAD F,\"" + filename + "\"," + data.length + ",";
            byte[] download_head = download.getBytes();

            while(fis.read(data) != -1) {
                ;
            }

            this.OutStream.write(download_head);
            this.OutStream.write(data);
            fis.close();
        } catch (Exception var7) {
            ;
        }

    }

    public void downloadbmp(String filename) {
        try {
            String path = Environment.getExternalStorageDirectory()+"/Download/Probenso.bmp";
            FileInputStream fis = new FileInputStream(path);

            //Util.Log.ih("fis.available() = " + fis.available());
            byte[] data = new byte[fis.available()];
            int[] FF = new int[data.length];
            String download = "DOWNLOAD F,\"" + path + "\"," + data.length + ",";
            byte[] download_head = download.getBytes();

            while(fis.read(data) != -1) {
                ;
            }

            this.OutStream.write(download_head);
            this.OutStream.write(data);
            fis.close();
        } catch (Exception var7) {
            ;
        }

    }

    public void downloadttf(String filename) {
        try {
            FileInputStream fis = new FileInputStream("/sdcard/Download/" + filename);
            byte[] data = new byte[fis.available()];
            int[] FF = new int[data.length];
            String download = "DOWNLOAD F,\"" + filename + "\"," + data.length + ",";
            byte[] download_head = download.getBytes();

            while(fis.read(data) != -1) {
                ;
            }

            this.OutStream.write(download_head);
            this.OutStream.write(data);
            fis.close();
        } catch (Exception var7) {
            ;
        }

    }
}