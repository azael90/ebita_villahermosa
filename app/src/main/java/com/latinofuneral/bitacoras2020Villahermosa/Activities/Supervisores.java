package com.latinofuneral.bitacoras2020Villahermosa.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.latinofuneral.bitacoras2020Villahermosa.Adapters.AdapterFotografiasTomadas;
import com.latinofuneral.bitacoras2020Villahermosa.Adapters.SliderAdapter;
import com.latinofuneral.bitacoras2020Villahermosa.BuildConfig;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Bitacoras;
import com.latinofuneral.bitacoras2020Villahermosa.Database.DatabaseAssistant;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Encuesta;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Parentescos;
import com.latinofuneral.bitacoras2020Villahermosa.Models.ModelFotografiasTomadas;
import com.latinofuneral.bitacoras2020Villahermosa.R;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.ApplicationResourcesProvider;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.Constants;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.ConstantsBitacoras;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.ForegroundService;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.Preferences;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.VolleySingleton;
import com.polyak.iconswitch.IconSwitch;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import soup.neumorphism.NeumorphButton;
import soup.neumorphism.NeumorphImageButton;

public class Supervisores extends AppCompatActivity {


    String anioSeleccionado="", mesSeleccionado ="", bitacoraEncontrada="";
    boolean domicilioPresionado = false, funeriaPresionada=false, borrarDatosCompletos= false;
    LinearLayout layoutResponseSuccess;
    EditText etDigitosBitacora;
    NeumorphButton btBuscarBitacora;
    TextView tvTotalPreguntas;
    TextView tvOmitir1, tvOmitir2, tvOmitir3, tvOmitir4, tvOmitir5, tvOmitir6, tvOmitir7, tvFinado, tvPlace;
    Button  btGuardarEncuesta, btCapturarImagenes;
    RadioButton rb11, rb12, rb13, rb14, rb21, rb22, rb31, rb32, rb41, rb42, rb51, rb52, rb61, rb62, rb63, rb64, rb71, rb72, rb73, rb74;
    RadioGroup radioGroup1, radioGroup2, radioGroup3, radioGroup4, radioGroup5, radioGroup6, radioGroup7, radioGroup8, radioGroup9, radioGroup10;


    String question1 = "", question2 = "", question3 = "", question4 = "", question5 = "", question6 = "", question7 = "", question8 = "", question9 = "", question10 = "";
    AutoCompleteTextView etSugerencias, etNombreFamiliar, etParentesco, etComment1, etComment2, etComment3, etComment4, etComment5, etComment6, etComment7, etComment8, etComment9, etComment10;
    NestedScrollView layoutGeneral;
    IconSwitch btCerrarAsistencia;
    private String parentescoSeleccionado ="";
    /****/
    private static String MEDIA_DIRECTORY = "EBITA_IMAGES";
    String imageName;
    Bitmap bitmap;
    private String mPath;
    private final int PHOTO_CODE = 200;
    int PICK_IMAGE_REQUEST = 1;
    private final int SELECT_PICTURE = 300;
    public ArrayList<String> listaImagenes = new ArrayList<>();
    public ArrayList<String> listaBitmaps =new ArrayList<>();
    /****/
    RecyclerView rvImagenes;
    StaggeredGridLayoutManager gridLayoutManager;
    AdapterFotografiasTomadas adapterFotografiasTomadas;
    List<ModelFotografiasTomadas> modelBitacorasActivas = new ArrayList<>();

    private void sincronizarDatos() {
        if (ApplicationResourcesProvider.checkInternetConnection()) {
            showMyCustomDialog();
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //downloadParentescos();
                                ForegroundService.createJsonForSync();
                            }
                        });

                        synchronized (this) {
                            wait(3000);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    dismissMyCustomDialog();
                                    Toast.makeText(getApplicationContext(), "Sincronizado correctamente", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Ocurrio un error, intenta de nuevo", Toast.LENGTH_SHORT).show();
                    }
                }

                ;
            };
            thread.start();
        } else
            Toast.makeText(getApplicationContext(), "No tienes internet", Toast.LENGTH_SHORT).show();
    }

    private void closeTeclado() {
        try {
            View view = this.getCurrentFocus();
            view.clearFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Throwable e) {
            Log.e("", "closeTeclado: Error en cerrar teclado" + e.getMessage());
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supervisores);
        Spinner spAno = (Spinner) findViewById(R.id.spAno);
        Spinner spMes = (Spinner) findViewById(R.id.spMes);
        layoutResponseSuccess = (LinearLayout) findViewById(R.id.layoutResponseSuccess);
        etDigitosBitacora = (EditText) findViewById(R.id.etDigitosBitacora);





        rvImagenes = findViewById(R.id.rvImagenes);
        rvImagenes.setHasFixedSize(true);
        gridLayoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        rvImagenes.setLayoutManager(gridLayoutManager);





        radioGroup1 = findViewById(R.id.radioGroup1);
        radioGroup2 = findViewById(R.id.radioGroup2);
        radioGroup3 = findViewById(R.id.radioGroup3);
        radioGroup4 = findViewById(R.id.radioGroup4);
        radioGroup5 = findViewById(R.id.radioGroup5);
        radioGroup6 = findViewById(R.id.radioGroup6);
        radioGroup7 = findViewById(R.id.radioGroup7);
        radioGroup8 = findViewById(R.id.radioGroup8);
        radioGroup9 = findViewById(R.id.radioGroup9);
        radioGroup10 = findViewById(R.id.radioGroup10);



        btCerrarAsistencia = (IconSwitch) findViewById(R.id.btCerrarAsistencia);
        if (Preferences.getPreferenceCheckinCheckoutAssistant(Supervisores.this, Preferences.PREFERENCE_CHECKIN_CHECKOUT_ASSISTANT))
            btCerrarAsistencia.setChecked(IconSwitch.Checked.RIGHT);

        btCapturarImagenes = (Button) findViewById(R.id.btCapturarImagenes);

        etNombreFamiliar = (AutoCompleteTextView) findViewById(R.id.etNombreFamiliar);
        etParentesco = (AutoCompleteTextView) findViewById(R.id.etParentesco);

        btBuscarBitacora = (NeumorphButton) findViewById(R.id.btBuscarBitacora);
        tvTotalPreguntas = (TextView) findViewById(R.id.tvTotalPreguntas);
        btGuardarEncuesta = (Button) findViewById(R.id.btGuardarEncuesta);
        layoutGeneral = (NestedScrollView) findViewById(R.id.layoutGeneral);

        tvFinado = findViewById(R.id.tvFinado);
        tvPlace = findViewById(R.id.tvPlace);

        /**  RadioButton rb11, rb12, rb13, rb14, rb21, rb22, rb31, rb32, rb41, rb42, rb51, rb52, rb61, rb62, rb63, rb64, rb71, rb72, rb73, rb74; **/
        rb11 = (RadioButton) findViewById(R.id.rb11);
        rb12 = (RadioButton) findViewById(R.id.rb12);
        rb13 = (RadioButton) findViewById(R.id.rb13);
        rb14 = (RadioButton) findViewById(R.id.rb14);

        rb21 = (RadioButton) findViewById(R.id.rb21);
        rb22 = (RadioButton) findViewById(R.id.rb22);

        rb31 = (RadioButton) findViewById(R.id.rb31);
        rb32 = (RadioButton) findViewById(R.id.rb32);

        rb41 = (RadioButton) findViewById(R.id.rb41);
        rb42 = (RadioButton) findViewById(R.id.rb42);
        rb51 = (RadioButton) findViewById(R.id.rb51);
        rb52 = (RadioButton) findViewById(R.id.rb52);

        rb61 = (RadioButton) findViewById(R.id.rb61);
        rb62 = (RadioButton) findViewById(R.id.rb62);
        rb63 = (RadioButton) findViewById(R.id.rb63);
        rb64 = (RadioButton) findViewById(R.id.rb64);

        rb71 = (RadioButton) findViewById(R.id.rb71);
        rb72 = (RadioButton) findViewById(R.id.rb72);
        rb73 = (RadioButton) findViewById(R.id.rb73);
        rb74 = (RadioButton) findViewById(R.id.rb74);

        etSugerencias =(AutoCompleteTextView) findViewById(R.id.etSugerencias);
        NeumorphImageButton btDescargarTodo = (NeumorphImageButton) findViewById(R.id.btDescargarTodo);
        btDescargarTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ApplicationResourcesProvider.checkInternetConnection()) {
                    try {
                        sincronizarDatos();
                    }catch (Throwable e){
                        Log.e("TAG", "onClick: Error en sincronizar infirmacion desd MainActivity" + e.getMessage());
                        Toast.makeText(Supervisores.this, "Ocurio un error con la sincronización, vuelve a intentarlo mas tarde.", Toast.LENGTH_SHORT).show();
                    }
                } else
                    Toast.makeText(getApplicationContext(), "No tienes internet", Toast.LENGTH_SHORT).show();
            }
        });

        //Parentescos.deleteAll(Parentescos.class);
        //DatabaseAssistant.insertParentesco("Papá");
        //DatabaseAssistant.insertParentesco("Mamá");
        //DatabaseAssistant.insertParentesco("Hermano");
        //DatabaseAssistant.insertParentesco("Hermana");
        //DatabaseAssistant.insertParentesco("Hijo");
        //DatabaseAssistant.insertParentesco("Hija");

        btCapturarImagenes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tvBtacoraEncontrada =(TextView) findViewById(R.id.tvBtacoraEncontrada);
                if(!bitacoraEncontrada.equals("") || !tvBtacoraEncontrada.getText().equals(""))
                    showOptions(bitacoraEncontrada);
                else
                    Toast.makeText(Supervisores.this, "Necesitas tener una bitácora activa", Toast.LENGTH_SHORT).show();
            }
        });


        etParentesco.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etParentesco.getText().toString().toUpperCase().length() >= 1) {
                    ArrayAdapter<String> adapterColonias = new ArrayAdapter<String>(getApplicationContext(), R.layout.style_spinner,
                            DatabaseAssistant.getParentescos(etParentesco.getText().toString().toUpperCase()));
                    etParentesco.setAdapter(adapterColonias);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        etParentesco.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                parentescoSeleccionado = etParentesco.getEditableText().toString();
                etParentesco.setTextColor(Color.parseColor("#969da9"));
                closeTeclado();
            }
        });



        NeumorphButton tvRegistroDeAsistencia =(NeumorphButton) findViewById(R.id.tvRegistroDeAsistencia);
        tvRegistroDeAsistencia.setTypeface(ApplicationResourcesProvider.regular);

        tvRegistroDeAsistencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean actualmenteEsEntrada;

                if(Preferences.getPreferencesAsistenciaEntrada(getApplicationContext(), Preferences.PREFERENCE_ASISTENCIA_ENTRADA)) {
                    actualmenteEsEntrada = true;
                }else {
                    actualmenteEsEntrada = false;
                }
                showBottomLayoutForHorariosDeAsistencia(v, actualmenteEsEntrada);
            }
        });


        btCerrarAsistencia.setCheckedChangeListener(new IconSwitch.CheckedChangeListener() {
            @Override
            public void onCheckChanged(IconSwitch.Checked current) {
                Log.d("ASISTENCIA", "onCheckChanged: " + current.toggle());
                if(current.name().equals("RIGHT")){
                    //Encendido
                    Log.d("ASISTENCIA", "onCheckChanged: Entro a RIGHT");
                }
                else if(current.name().equals("LEFT")){
                    //APAGADO
                    Log.d("ASISTENCIA", "onCheckChanged: Entro a LEFT");
                    showErrorDialog("¿Cerrar sesión?", "");
                }
            }
        });

        String[] anos = {"21", "22", "23", "24", "25", "26", "27", "28", "29", "30"};
        ArrayAdapter<String> adapterAnos = new ArrayAdapter<String>(getApplicationContext(), R.layout.style_spinner, anos);
        spAno.setAdapter(adapterAnos);

        String[] meses = {"ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"};
        ArrayAdapter<String> adapterMeses = new ArrayAdapter<String>(getApplicationContext(), R.layout.style_spinner, meses);
        spMes.setAdapter(adapterMeses);



        Date date = new Date();
        LocalDate localDate = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            try {
                localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                int month = localDate.getMonthValue();
                spMes.setSelection(month - 1);
            }catch (Throwable e){
                Log.e("TAG", "onCreate: Error en asignacion de meses: " + e.getMessage());
            }
        }



        spAno.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                anioSeleccionado = parent.getItemAtPosition(position).toString();
                layoutResponseSuccess.setVisibility(View.GONE);
                btCapturarImagenes.setVisibility(View.GONE);
                limpiarTodo(false);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                anioSeleccionado ="";
                layoutResponseSuccess.setVisibility(View.GONE);
                btCapturarImagenes.setVisibility(View.GONE);
                limpiarTodo(false);
            }
        });

        spMes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mesSeleccionado = parent.getItemAtPosition(position).toString();
                layoutResponseSuccess.setVisibility(View.GONE);
                btCapturarImagenes.setVisibility(View.GONE);
                limpiarTodo(false);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mesSeleccionado ="";
                layoutResponseSuccess.setVisibility(View.GONE);
                btCapturarImagenes.setVisibility(View.GONE);
                limpiarTodo(false);
            }
        });


        btBuscarBitacora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    modelBitacorasActivas.clear();
                    adapterFotografiasTomadas.notifyDataSetChanged();
                }catch (Throwable e){
                    Log.e("TAG", "onClick: Error:  " + e.getMessage() );
                }
                layoutResponseSuccess.setVisibility(View.GONE);
                btCapturarImagenes.setVisibility(View.GONE);

                limpiarTodo(false);
                desbloquearCampos();
                try {
                    closeTeclado();
                }catch (Throwable e){
                    Log.e("TAG", "onClick: ");
                }
                String bitacoraGeneral = "VSA"+anioSeleccionado+mesSeleccionado+etDigitosBitacora.getText().toString();
                if (!anioSeleccionado.equals("") && !mesSeleccionado.equals("") && bitacoraGeneral.length() > 8 && !etDigitosBitacora.getText().toString().equals(""))
                {
                    if (!DatabaseAssistant.laBitacoraEstaActiva(bitacoraGeneral.toUpperCase())) {
                        searchBitacoraInWeb(bitacoraGeneral.toUpperCase());
                    }else
                        Toast.makeText(getApplicationContext(), "Ya tienes esta bitácora activa.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Por favor verificar la bitácora a buscar.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        CardView btDomicilio =(CardView) findViewById(R.id.btDomicilio);
        CardView btFuneraria =(CardView) findViewById(R.id.btFuneraria);
        btDomicilio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    btFuneraria.clearAnimation();
                    btDomicilio.clearAnimation();
                }catch (Throwable e){
                    Log.e("TAG", "onClick: Error" + e.getMessage());
                }

                if(borrarDatosCompletos)
                    limpiarTodo(false);

                domicilioPresionado = true;
                funeriaPresionada = false;


               /* try {
                    v.clearAnimation();
                }catch (Throwable e){
                    Log.e("TAG", "onClick: Error" + e.getMessage());
                }*/

                /** Animar el boton btDomicilio y desanimar el boton de funeraria**/
                //Animation animationDomicilio = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
                //Animation animationFuneraria = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out);
                LinearLayout contornoDomicilio = (LinearLayout) findViewById(R.id.contornoDomicilio);
                LinearLayout contornoFuneraria = (LinearLayout) findViewById(R.id.contornoFuneraria);
                TextView tvDomicilio = (TextView) findViewById(R.id.tvDomicilio);
                TextView tvFuneraria = (TextView) findViewById(R.id.tvFuneraria);

                contornoDomicilio.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_blue));
                //btDomicilio.setCardElevation(10f);
                //tvDomicilio.setTextColor(Color.parseColor("#1b1b1b"));
                //btDomicilio.startAnimation(animationDomicilio);

                contornoFuneraria.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_transparent));
                //btFuneraria.setCardElevation(5f);
                //tvFuneraria.setTextColor(Color.parseColor("#969da9"));
                //btFuneraria.startAnimation(animationFuneraria);

                desbloquearCampos();

                if(borrarDatosCompletos)
                    llenarCamposDeEncuestaPorTipoEncuesta(bitacoraEncontrada, "Domicilio");


            }
        });


        btFuneraria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(borrarDatosCompletos)
                    limpiarTodo(false);
                funeriaPresionada = true;
                domicilioPresionado = false;
                try {
                    btFuneraria.clearAnimation();
                    btDomicilio.clearAnimation();
                }catch (Throwable e){
                    Log.e("TAG", "onClick: Error" + e.getMessage());
                }

                /** Animar el boton btDomicilio y desanimar el boton de funeraria**/
                //Animation animationFuneraria = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
                //Animation animationDomicilio = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out);
                LinearLayout contornoDomicilio = (LinearLayout) findViewById(R.id.contornoDomicilio);
                LinearLayout contornoFuneraria = (LinearLayout) findViewById(R.id.contornoFuneraria);
                TextView tvDomicilio = (TextView) findViewById(R.id.tvDomicilio);
                TextView tvFuneraria = (TextView) findViewById(R.id.tvFuneraria);

                contornoFuneraria.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_blue));
                //btFuneraria.setCardElevation(10f);
                //tvFuneraria.setTextColor(Color.parseColor("#1b1b1b"));
                //btFuneraria.startAnimation(animationFuneraria);

                contornoDomicilio.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_transparent));
                //btDomicilio.setCardElevation(5f);
                //tvDomicilio.setTextColor(Color.parseColor("#969da9"));
                //btDomicilio.startAnimation(animationDomicilio);

                desbloquearCampos();

                if(borrarDatosCompletos)
                    llenarCamposDeEncuestaPorTipoEncuesta(bitacoraEncontrada, "Funeraria");


            }
        });


        etComment1 = findViewById(R.id.etComment1);
        etComment2 = findViewById(R.id.etComment2);
        etComment3 = findViewById(R.id.etComment3);
        etComment4 = findViewById(R.id.etComment4);
        etComment5 = findViewById(R.id.etComment5);
        etComment6 = findViewById(R.id.etComment6);
        etComment7 = findViewById(R.id.etComment7);
        etComment8 = findViewById(R.id.etComment8);
        etComment9 = findViewById(R.id.etComment9);
        etComment10 = findViewById(R.id.etComment10);

        btGuardarEncuesta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /**Validar cada Radio para saber que todos esten activos, pero por grupos de pregunta **/

                getOptionsSelected();

                if(etNombreFamiliar.getText().toString().trim().length() == 0){
                    etNombreFamiliar.requestFocus();
                    etNombreFamiliar.setError(getString(R.string.error_family_name));
                }
                else if(etParentesco.getText().toString().trim().length() == 0){
                    etParentesco.requestFocus();
                    etParentesco.setError(getString(R.string.error_relationship));
                }

                else if(!domicilioPresionado && !funeriaPresionada){
                    showErrorBtn();
                }


                else if(question1.equals("")){
                    Toast.makeText(Supervisores.this, "Completa la pregunta uno(1) para continuar", Toast.LENGTH_LONG).show();
                }else if(question2.equals("")){
                    Toast.makeText(Supervisores.this, "Completa la pregunta dos(2) para continuar", Toast.LENGTH_LONG).show();
                }else if(question3.equals("")){
                    Toast.makeText(Supervisores.this, "Completa la pregunta tres(3) para continuar", Toast.LENGTH_LONG).show();
                }else if(question4.equals("")){
                    Toast.makeText(Supervisores.this, "Completa la pregunta cuatro(4) para continuar", Toast.LENGTH_LONG).show();
                }else if(question5.equals("")){
                    Toast.makeText(Supervisores.this, "Completa la pregunta cinco(5) para continuar", Toast.LENGTH_LONG).show();
                }else if(question6.equals("")){
                    Toast.makeText(Supervisores.this, "Completa la pregunta seis(6) para continuar", Toast.LENGTH_LONG).show();
                }else if(question7.equals("")){
                    Toast.makeText(Supervisores.this, "Completa la pregunta siete(7) para continuar", Toast.LENGTH_LONG).show();
                }else if(question8.equals("")){
                    Toast.makeText(Supervisores.this, "Completa la pregunta ocho(8) para continuar", Toast.LENGTH_LONG).show();
                }else if(question9.equals("")){
                    Toast.makeText(Supervisores.this, "Completa la pregunta nueve(9) para continuar", Toast.LENGTH_LONG).show();
                }
                else if(question10.equals("")){
                    Toast.makeText(Supervisores.this, "Completa la pregunta diez(10) para continuar", Toast.LENGTH_LONG).show();
                }else{

                    JSONArray arrayPreguntas = new JSONArray();
                    JSONObject jsonPregunta = new JSONObject();
                    /** Ciclo para guardar las preguntas del 0 al 10 **/
                    for(int i=0; i<=9; i++) {
                        if (i + 1 == 1) {

                            try {
                                JSONObject preguntas = new JSONObject();
                                preguntas.put("pregunta", "1");
                                preguntas.put("respuesta", "" + question1);
                                preguntas.put("explicacion_respuesta", etComment1.getText().toString());
                                arrayPreguntas.put(preguntas);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else if (i + 1 == 2) {
                            try {
                                JSONObject preguntas = new JSONObject();
                                preguntas.put("pregunta", "2");
                                preguntas.put("respuesta", "" + question2);
                                preguntas.put("explicacion_respuesta", "" + etComment2.getText().toString());
                                arrayPreguntas.put(preguntas);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else if (i + 1 == 3) {

                            try {
                                JSONObject preguntas = new JSONObject();
                                preguntas.put("pregunta", "3");
                                preguntas.put("respuesta", "" + question3);
                                preguntas.put("explicacion_respuesta", "" + etComment3.getText().toString());
                                arrayPreguntas.put(preguntas);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (i + 1 == 4) {

                            try {
                                JSONObject preguntas = new JSONObject();
                                preguntas.put("pregunta", "4");
                                preguntas.put("respuesta", "" + question4);
                                preguntas.put("explicacion_respuesta", "" + etComment4.getText().toString());
                                arrayPreguntas.put(preguntas);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (i + 1 == 5) {
                            try {
                                JSONObject preguntas = new JSONObject();
                                preguntas.put("pregunta", "5");
                                preguntas.put("respuesta", "" + question5);
                                preguntas.put("explicacion_respuesta", "" + etComment5.getText().toString());
                                arrayPreguntas.put(preguntas);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        } else if (i + 1 == 6) {

                            try {
                                JSONObject preguntas = new JSONObject();
                                preguntas.put("pregunta", "6");
                                preguntas.put("respuesta", "" + question6);
                                preguntas.put("explicacion_respuesta", "" + etComment6.getText().toString());
                                arrayPreguntas.put(preguntas);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else if (i + 1 == 7) {
                            try {
                                JSONObject preguntas = new JSONObject();
                                preguntas.put("pregunta", "7");
                                preguntas.put("respuesta", "" + question7);
                                preguntas.put("explicacion_respuesta", "" + etComment7.getText().toString());
                                arrayPreguntas.put(preguntas);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else if (i + 1 == 8) {
                            try {
                                JSONObject preguntas = new JSONObject();
                                preguntas.put("pregunta", "8");
                                preguntas.put("respuesta", "" + question8);
                                preguntas.put("explicacion_respuesta", "" + etComment8.getText().toString());
                                arrayPreguntas.put(preguntas);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else if (i + 1 == 9) {
                            try {
                                JSONObject preguntas = new JSONObject();
                                preguntas.put("pregunta", "9");
                                preguntas.put("respuesta", "" + question9);
                                preguntas.put("explicacion_respuesta", "" + etComment9.getText().toString());
                                arrayPreguntas.put(preguntas);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else if (i + 1 == 10) {
                            try {
                                JSONObject preguntas = new JSONObject();
                                preguntas.put("pregunta", "10");
                                preguntas.put("respuesta", "" + question10);
                                preguntas.put("explicacion_respuesta", "" + etComment10.getText().toString());
                                arrayPreguntas.put(preguntas);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }


                    try {
                        JSONObject preguntas = new JSONObject();
                        preguntas.put("pregunta", "Sugerencias o comentarios");
                        preguntas.put("respuesta",     "" + etSugerencias.getText().toString());
                        preguntas.put("explicacion_respuesta",  "");
                        arrayPreguntas.put(preguntas);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                    try {
                        jsonPregunta.put("encuesta", arrayPreguntas);
                    }catch (Throwable e){
                        Log.e("TAG", "onClick: " + e.getMessage());
                    }


                    @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String[] coordenadasss = ApplicationResourcesProvider.getCoordenadasFromApplication();
                    DatabaseAssistant.insertarPregunta(
                            ""+ bitacoraEncontrada,
                            "" + etNombreFamiliar.getText().toString(),
                            "" + etParentesco.getText().toString(),
                            "" + (domicilioPresionado ? "Domicilio" : funeriaPresionada ? "Funeraria" : "No seleccionado"),
                            "" +DatabaseAssistant.getUserNameFromSesiones(),
                            "" + dateFormat.format(new Date()),
                            "" + jsonPregunta.toString(), "0",
                            "" + coordenadasss[0],
                            "" + coordenadasss[1]
                    );

                    Log.e("JSON_ENCUESTAS", "JSON: " + new Gson().toJson(Encuesta.listAll(Encuesta.class)));


                    LayoutInflater inflater = getLayoutInflater();
                    View view = inflater.inflate(R.layout.custom_toast_layout, (ViewGroup)findViewById(R.id.relativeLayout1));
                    LottieAnimationView lottieAnimationView = view.findViewById(R.id.imageView1);
                    lottieAnimationView.setAnimation("success_toast.json");
                    lottieAnimationView.loop(false);
                    lottieAnimationView.playAnimation();
                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(view);
                    toast.show();
                    //btDomicilio.setCardElevation(5f);
                    //btFuneraria.setCardElevation(5f);

                    limpiarTodo(false);
                    layoutResponseSuccess.setVisibility(View.GONE);
                    btCapturarImagenes.setVisibility(View.GONE);
                }
            }
        });


        TextView textoSinBitacora =(TextView) findViewById(R.id.textoSinBitacora);
        textoSinBitacora.setText(DatabaseAssistant.getLastestTimeSesion());

        Preferences.setActividadYaIniciada(getApplicationContext(), true, Preferences.PREFERENCE_ACTIVIDAD_INICIADA);
        startService();
    }


    public void showErrorBtn(){
        CardView btDomicilio =(CardView) findViewById(R.id.btDomicilio);
        CardView btFuneraria =(CardView) findViewById(R.id.btFuneraria);
        Toast.makeText(Supervisores.this, "Selecciona Domicilio o Funeraria.", Toast.LENGTH_LONG).show();

        final Display display = getWindowManager().getDefaultDisplay();
        try {
            layoutGeneral.smoothScrollTo(0, 450 );

        }catch (Throwable e){
            layoutGeneral.smoothScrollTo(0, display.getHeight() );

        }
        btDomicilio.requestFocus();
        btFuneraria.requestFocus();

        LinearLayout contornoDomicilio = (LinearLayout) findViewById(R.id.contornoDomicilio);
        LinearLayout contornoFuneraria = (LinearLayout) findViewById(R.id.contornoFuneraria);

        Animation mAnimation = new AlphaAnimation(1, 0);
        mAnimation.setDuration(350);
        mAnimation.setInterpolator(new LinearInterpolator());
        mAnimation.setRepeatCount(Animation.INFINITE);
        mAnimation.setRepeatMode(Animation.REVERSE);

        btFuneraria.startAnimation(mAnimation);
        btDomicilio.startAnimation(mAnimation);

        contornoDomicilio.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_blue));
        contornoFuneraria.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_blue));

        borrarDatosCompletos = false;
    }

    public void getOptionsSelected(){

        int selectedId =  radioGroup1.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton) findViewById(selectedId);
        if (radioButton!=null)
            question1 = radioButton.getText().toString();

        selectedId =  radioGroup2.getCheckedRadioButtonId();
        radioButton = (RadioButton) findViewById(selectedId);
        if (radioButton!=null)
            question2 = radioButton.getText().toString();

        selectedId =  radioGroup3.getCheckedRadioButtonId();
        radioButton = (RadioButton) findViewById(selectedId);
        if (radioButton!=null)
            question3 = radioButton.getText().toString();

        selectedId =  radioGroup4.getCheckedRadioButtonId();
        radioButton = (RadioButton) findViewById(selectedId);
        if (radioButton!=null)
            question4 = radioButton.getText().toString();

        selectedId =  radioGroup5.getCheckedRadioButtonId();
        radioButton = (RadioButton) findViewById(selectedId);
        if (radioButton!=null)
            question5 = radioButton.getText().toString();

        selectedId =  radioGroup6.getCheckedRadioButtonId();
        radioButton = (RadioButton) findViewById(selectedId);
        if (radioButton!=null)
            question6 = radioButton.getText().toString();

        selectedId =  radioGroup7.getCheckedRadioButtonId();
        radioButton = (RadioButton) findViewById(selectedId);
        if (radioButton!=null)
            question7 = radioButton.getText().toString();

        selectedId =  radioGroup8.getCheckedRadioButtonId();
        radioButton = (RadioButton) findViewById(selectedId);
        if (radioButton!=null)
            question8 = radioButton.getText().toString();

        selectedId =  radioGroup9.getCheckedRadioButtonId();
        radioButton = (RadioButton) findViewById(selectedId);
        if (radioButton!=null)
            question9 = radioButton.getText().toString();

        selectedId =  radioGroup10.getCheckedRadioButtonId();
        radioButton = (RadioButton) findViewById(selectedId);
        if (radioButton!=null)
            question10 = radioButton.getText().toString();

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void startService() {
        try {
            Intent serviceIntent = new Intent(this, ForegroundService.class);
            serviceIntent.putExtra("inputExtra", Constants.ACTION.STARTFOREGROUND_ACTION);
            ContextCompat.startForegroundService(this, serviceIntent);
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        stopService();
        Preferences.setWithinTheZone(getApplicationContext(), false, Preferences.PREFERENCE_WITHIN_THE_ZONE_TO_LOGIN);
        Preferences.setEndLoginTheZone(getApplicationContext(), false, Preferences.PREFERENCE_END_SESION_THE_ZONE_TO_LOGIN);

    }

    public void stopService() {
        try {
            Intent serviceIntent = new Intent(this, ForegroundService.class);
            serviceIntent.putExtra("inputExtra", Constants.ACTION.STOPFOREGROUND_ACTION);
            stopService(serviceIntent);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    private void searchBitacoraInWeb(String bitacora)
    {

        LinearLayout contornoFuneraria = (LinearLayout) findViewById(R.id.contornoFuneraria);
        LinearLayout contornoDomicilio = (LinearLayout) findViewById(R.id.contornoDomicilio);

        contornoFuneraria.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_transparent));
        contornoDomicilio.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_transparent));

        if (ApplicationResourcesProvider.checkInternetConnection()) {
            showMyCustomDialog();
            try {
                String token = FirebaseInstanceId.getInstance().getToken();
                Log.d("FIREBASE", "Refresh Token: " + token);
                if (token != null)
                    DatabaseAssistant.insertarToken(token);
                else
                    DatabaseAssistant.insertarToken("Unknown");
            } catch (Throwable e) {
                Log.e("kjk", "downloadBitacoras: " + e.getMessage());
            }

            JSONObject params = new JSONObject();
            try {
                params.put("usuario", DatabaseAssistant.getUserNameFromSesiones());
                params.put("token_device", DatabaseAssistant.getTokenDeUsuario());
                params.put("isProveedor", "0");
                params.put("isBunker", Preferences.getPreferenceIsbunker(getApplicationContext(), Preferences.PREFERENCE_ISBUNKER) ? "1" : "0");
                params.put("bitacora", bitacora);
                params.put("consulta_supervisor", "1");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_DOWNLOAD_BITACORA_INDIVIDUAL, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Bitacoras.deleteAll(Bitacoras.class);
                    JSONObject jsonArraySupervisorEncuesta = new JSONObject();

                    try {
                        if(!response.getBoolean("error"))
                        {
                            if (response.has("supervisorEncuesta"))
                            {
                                try {
                                    jsonArraySupervisorEncuesta = response.getJSONObject("supervisorEncuesta");
                                    JSONArray jsonArrayEncuesta = jsonArraySupervisorEncuesta.getJSONArray("encuestas");


                                    layoutResponseSuccess.setVisibility(View.VISIBLE);
                                    btCapturarImagenes.setVisibility(View.VISIBLE);
                                    TextView tvBitacoraEncontrada = (TextView) findViewById(R.id.tvBtacoraEncontrada);


                                    try {
                                        tvBitacoraEncontrada.setText(jsonArrayEncuesta.getJSONObject(0).getString("bitacora").toUpperCase());
                                        bitacoraEncontrada = jsonArrayEncuesta.getJSONObject(0).getString("bitacora").toUpperCase();
                                    }catch (Throwable e){
                                        Log.e("TAG", "onResponse: Error al asiganr la bitacora desde el WS, no tiene encuestas. " + e.getMessage());
                                        bitacoraEncontrada = params.getString("bitacora");
                                        tvBitacoraEncontrada.setText(params.getString("bitacora"));
                                    }


                                    if(jsonArrayEncuesta.length()>0)
                                    {
                                        String[] arrayCoordenadas = ApplicationResourcesProvider.getCoordenadasFromApplication();
                                        for (int i = 0; i <= jsonArrayEncuesta.length()-1; i++) {


                                            if (DatabaseAssistant.existEncuesta(jsonArrayEncuesta.getJSONObject(i))) {
                                                DatabaseAssistant.deleteEncuesta(jsonArrayEncuesta.getJSONObject(i));
                                            }

                                            //Insertar encuestas
                                            DatabaseAssistant.insertarPregunta(
                                                    "" + jsonArrayEncuesta.getJSONObject(i).getString("bitacora"),
                                                    "" + jsonArrayEncuesta.getJSONObject(i).getString("nombre_familiar"),
                                                    "" + jsonArrayEncuesta.getJSONObject(i).getString("parentesco"),
                                                    "" + jsonArrayEncuesta.getJSONObject(i).getString("tipo_encuesta"),
                                                    "" + jsonArrayEncuesta.getJSONObject(i).getString("usuario"),
                                                    "" + jsonArrayEncuesta.getJSONObject(i).getString("fecha_captura"),
                                                    "" + jsonArrayEncuesta.getJSONObject(i).getString("encuesta"),
                                                    "4",
                                                    "" + arrayCoordenadas[0],
                                                    "" + arrayCoordenadas[1]
                                            );

                                        }

                                    }else{
                                        Toast.makeText(getApplicationContext(), "No encontramos encuestas realizadas de la web", Toast.LENGTH_LONG).show();
                                    }
                                    llenarCamposDeEncuesta(params.getString("bitacora"));
                                    borrarDatosCompletos = true;
                                    dismissMyCustomDialog();

                                    //Guardado de parentescos
                                    JSONArray arrayParentescos = response.getJSONArray("parentesco");
                                    if(arrayParentescos.length()>0) {
                                        Parentescos.deleteAll(Parentescos.class);
                                        for (int i = 0; i <= arrayParentescos.length() - 1; i++) {
                                            DatabaseAssistant.insertParentesco("" + arrayParentescos.getJSONObject(i).getString("name"));
                                        }
                                    }

                                    if(layoutResponseSuccess.getVisibility()==View.GONE) {
                                      /*  TextView tvVerMas1 = (TextView) findViewById(R.id.tvVerMas1);
                                        tvVerMas1.performClick();

                                       */
                                    }


                                    if (response.has("bitacorasDetails")){

                                        if (response.getJSONArray("bitacorasDetails").getJSONObject(0).has("finado")){
                                            tvFinado.setText(response.getJSONArray("bitacorasDetails").getJSONObject(0).getString("finado"));
                                        }

                                        if (response.getJSONArray("bitacorasDetails").getJSONObject(0).has("lugar_de_velacion")){
                                            tvPlace.setText(response.getJSONArray("bitacorasDetails").getJSONObject(0).getString("lugar_de_velacion"));
                                        }

                                    }

                                } catch (Throwable e) {
                                    Log.e("sasdas", "onResponse: Error al consultar y guardar la bitacora individual: " + e.getMessage());
                                    dismissMyCustomDialog();
                                    Toast.makeText(getApplicationContext(), "No encontramos información de la bitácora que ingresaste, verifica nuevamente.", Toast.LENGTH_SHORT).show();
                                }

                            }else {
                                Toast.makeText(getApplicationContext(), "No encontramos información de la bitácora que ingresaste, verifica nuevamente.", Toast.LENGTH_SHORT).show();
                                dismissMyCustomDialog();
                            }

                        }else{
                            Toast.makeText(getApplicationContext(), "No encontramos información de la bitácora que ingresaste, verifica nuevamente.", Toast.LENGTH_SHORT).show();
                            dismissMyCustomDialog();
                        }

                    } catch (Exception e) {
                        Log.e("TAG", "onResponse: " + e.getMessage() );
                        dismissMyCustomDialog();
                        Toast.makeText(getApplicationContext(), "No encontramos información de la bitácora que ingresaste, verifica nuevamente.", Toast.LENGTH_SHORT).show();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("TAG", "onErrorResponse: Error: " + error.getMessage());
                            dismissMyCustomDialog();
                            Toast.makeText(getApplicationContext(), "No encontramos información de la bitácora que ingresaste, verifica nuevamente.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
            };

            postRequest.setRetryPolicy(new DefaultRetryPolicy(90000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);

        } else
            Toast.makeText(getApplicationContext(), "Verifica tu conexión a internet", Toast.LENGTH_LONG).show();

    }

    public void llenarCamposDeEncuesta(String bitacora){

        Animation mAnimation = new AlphaAnimation(1, 0);
        mAnimation.setDuration(600);
        mAnimation.setInterpolator(new LinearInterpolator());
        mAnimation.setRepeatCount(Animation.INFINITE);
        mAnimation.setRepeatMode(Animation.REVERSE);




        List<Encuesta> lista = Encuesta.findWithQuery(Encuesta.class, "SELECT * FROM ENCUESTA where bitacora = '" + bitacora + "' ORDER BY fecha ASC");
        if(lista.size() > 0) {
            for (int i = 0; i <= lista.size()-1; i++) {

                etNombreFamiliar.setText(lista.get(i).getNombrefamiliar());
                etParentesco.setText(lista.get(i).getParentesco());

                if(lista.get(i).getLugar().equals("Domicilio")) {
                    LinearLayout contornoDomicilio = (LinearLayout) findViewById(R.id.contornoDomicilio);
                    LinearLayout contornoFuneraria = (LinearLayout) findViewById(R.id.contornoFuneraria);
                    CardView btDomicilio =(CardView) findViewById(R.id.btDomicilio);
                    contornoDomicilio.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_green));
                    contornoFuneraria.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_transparent));
                    //btDomicilio.setCardElevation(10f);
                    //btDomicilio.startAnimation(mAnimation);
                    //btDomicilio.setEnabled(false);
                    domicilioPresionado = true;
                    funeriaPresionada = false;

                }

                if(lista.get(i).getLugar().equals("Funeraria")) {
                    LinearLayout contornoDomicilio = (LinearLayout) findViewById(R.id.contornoDomicilio);
                    LinearLayout contornoFuneraria = (LinearLayout) findViewById(R.id.contornoFuneraria);
                    CardView btFuneraria =(CardView) findViewById(R.id.btFuneraria);
                    contornoFuneraria.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_green));
                    contornoDomicilio.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_transparent));
                    //btFuneraria.setCardElevation(10f);
                    //btFuneraria.startAnimation(mAnimation);
                    //btFuneraria.setEnabled(false);
                    domicilioPresionado = false;
                    funeriaPresionada = true;
                }

                try {
                    JSONObject jsonEncuesta = new JSONObject(lista.get(i).getEncuesta());
                    JSONArray encuesta = jsonEncuesta.getJSONArray("encuesta");
                    for(int x =0; x<= encuesta.length()-1; x++){
                        try {
                            switch (x + 1) {
                                case 1:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("1")){

                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup1.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup1.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup1.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup1.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup1.getChildAt(4)).setChecked(true);
                                                break;
                                        }

                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment1.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }

                                    }
                                    break;
                                case 2:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("2")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup2.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup2.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup2.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup2.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup2.getChildAt(4)).setChecked(true);
                                                break;
                                        }
                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment2.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;
                                case 3:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("3")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup3.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup3.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup3.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup3.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup3.getChildAt(4)).setChecked(true);
                                                break;
                                        }
                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment3.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;
                                case 4:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("4")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup4.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup4.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup4.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup4.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup4.getChildAt(4)).setChecked(true);
                                                break;
                                        }
                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment4.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;
                                case 5:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("5")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup5.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup5.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup5.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup5.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup5.getChildAt(4)).setChecked(true);
                                                break;
                                        }
                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment5.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;
                                case 6:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("6")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup6.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup6.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup6.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup6.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup6.getChildAt(4)).setChecked(true);
                                                break;
                                        }
                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment6.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;
                                case 7:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("7")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup7.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup7.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup7.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup7.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup7.getChildAt(4)).setChecked(true);
                                                break;
                                        }
                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment7.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;

                                case 8:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("8")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup8.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup8.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup8.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup8.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup8.getChildAt(4)).setChecked(true);
                                                break;
                                        }
                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment8.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;

                                case 9:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("9")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup9.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup9.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup9.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup9.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup9.getChildAt(4)).setChecked(true);
                                                break;
                                        }
                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment9.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;

                                case 10:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("10")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup10.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup10.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup10.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup10.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup10.getChildAt(4)).setChecked(true);
                                                break;
                                        }
                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment10.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;
                            }

                            try {
                                //Validamos si tenemos pregunta  de sugerencias
                                if(encuesta.getJSONObject(x).has("pregunta")) {
                                    if (encuesta.getJSONObject(x).getString("pregunta").equals("Sugerencias o comentarios")) {
                                        etSugerencias.setText(encuesta.getJSONObject(x).getString("respuesta"));
                                    }
                                }
                            }catch (Throwable e){
                                Log.e("TAG", "llenarCamposDeEncuesta: Error al consultar sugerencias y comentarios" + e.getMessage());
                            }


                        }catch (Throwable e){
                            Log.e("TAG", "llenarCamposDeEncuesta: Error en asignar las posiciones de encuesta");
                        }
                    }

               /* try {
                    //Validamos si tenemos pregunta  de sugerencias
                    if(encuesta.getJSONObject(7).has("pregunta")) {
                        if (encuesta.getJSONObject(7).getString("pregunta").equals("Sugerencias o comentarios")) {
                            etSugerencias.setText(encuesta.getJSONObject(7).getString("respuesta"));
                        }
                    }
                }catch (Throwable e){
                    Log.e("TAG", "llenarCamposDeEncuesta: Error al consultar sugerencias y comentarios" + e.getMessage());
                }*/

                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        }


        bloquearCampos();






    }


    public void llenarCamposDeEncuestaPorTipoEncuesta(String bitacora, String lugarEncuesta){
        //String[] arrayEncuestaLocalData = DatabaseAssistant.getEncuestaPorBitacoraAndTipo(bitacora, lugarEncuesta);


        List<Encuesta> listaPrincipal = Encuesta.findWithQuery(Encuesta.class, "SELECT * FROM ENCUESTA where bitacora = '" + bitacora + "' and lugar ='"+ lugarEncuesta +"' ORDER BY fecha ASC ");
        if(listaPrincipal.size() > 0)
        {
            limpiarTodo(false);


            Animation mAnimation = new AlphaAnimation(1, 0);
            mAnimation.setDuration(600);
            mAnimation.setInterpolator(new LinearInterpolator());
            mAnimation.setRepeatCount(Animation.INFINITE);
            mAnimation.setRepeatMode(Animation.REVERSE);


            for (int i = 0; i <= listaPrincipal.size()-1; i++) {

                etNombreFamiliar.setText(listaPrincipal.get(i).getNombrefamiliar());
                etParentesco.setText(listaPrincipal.get(i).getParentesco());

                if(listaPrincipal.get(i).getLugar().equals("Domicilio")) {
                    LinearLayout contornoDomicilio = (LinearLayout) findViewById(R.id.contornoDomicilio);
                    LinearLayout contornoFuneraria = (LinearLayout) findViewById(R.id.contornoFuneraria);
                    CardView btDomicilio =(CardView) findViewById(R.id.btDomicilio);
                    contornoDomicilio.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_green));
                    contornoFuneraria.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_transparent));
                    //btDomicilio.setCardElevation(10f);
                    //btDomicilio.startAnimation(mAnimation);
                    //btDomicilio.setEnabled(false);
                    domicilioPresionado = true;
                    funeriaPresionada = false;

                }

                if(listaPrincipal.get(i).getLugar().equals("Funeraria")) {
                    LinearLayout contornoDomicilio = (LinearLayout) findViewById(R.id.contornoDomicilio);
                    LinearLayout contornoFuneraria = (LinearLayout) findViewById(R.id.contornoFuneraria);
                    CardView btFuneraria =(CardView) findViewById(R.id.btFuneraria);
                    contornoFuneraria.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_green));
                    contornoDomicilio.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_transparent));
                    //btFuneraria.setCardElevation(10f);
                    //btFuneraria.startAnimation(mAnimation);
                    //btFuneraria.setEnabled(false);
                    domicilioPresionado = false;
                    funeriaPresionada = true;
                }

                try {
                    JSONObject jsonEncuesta = new JSONObject(listaPrincipal.get(i).getEncuesta());
                    JSONArray encuesta = jsonEncuesta.getJSONArray("encuesta");
                    for(int x =0; x<= encuesta.length()-1; x++){
                        try {
                            switch (x + 1) {
                                case 1:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("1")){

                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup1.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup1.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup1.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup1.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup1.getChildAt(4)).setChecked(true);
                                                break;
                                        }

                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment1.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;
                                case 2:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("2")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup2.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup2.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup2.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup2.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup2.getChildAt(4)).setChecked(true);
                                                break;
                                        }
                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment2.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;
                                case 3:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("3")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup3.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup3.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup3.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup3.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup3.getChildAt(4)).setChecked(true);
                                                break;
                                        }
                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment3.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;
                                case 4:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("4")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup4.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup4.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup4.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup4.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup4.getChildAt(4)).setChecked(true);
                                                break;
                                        }
                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment4.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;
                                case 5:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("5")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup5.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup5.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup5.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup5.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup5.getChildAt(4)).setChecked(true);
                                                break;
                                        }
                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment5.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;
                                case 6:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("6")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup6.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup6.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup6.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup6.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup6.getChildAt(4)).setChecked(true);
                                                break;
                                        }
                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment6.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;
                                case 7:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("7")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup7.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup7.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup7.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup7.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup7.getChildAt(4)).setChecked(true);
                                                break;
                                        }
                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment7.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;

                                case 8:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("8")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup8.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup8.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup8.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup8.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup8.getChildAt(4)).setChecked(true);
                                                break;
                                        }

                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment8.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;

                                case 9:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("9")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup9.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup9.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup9.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup9.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup9.getChildAt(4)).setChecked(true);
                                                break;
                                        }
                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment9.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;

                                case 10:
                                    if(encuesta.getJSONObject(x).getString("pregunta").equals("10")){
                                        switch (encuesta.getJSONObject(x).getString("respuesta")){
                                            case "1":
                                                ((RadioButton)radioGroup10.getChildAt(0)).setChecked(true);
                                                break;
                                            case "2":
                                                ((RadioButton)radioGroup10.getChildAt(1)).setChecked(true);
                                                break;

                                            case "3":
                                                ((RadioButton)radioGroup10.getChildAt(2)).setChecked(true);
                                                break;
                                            case "4":
                                                ((RadioButton)radioGroup10.getChildAt(3)).setChecked(true);
                                                break;
                                            case "5":
                                                ((RadioButton)radioGroup10.getChildAt(4)).setChecked(true);
                                                break;
                                        }
                                        if(encuesta.getJSONObject(x).has("explicacion_respuesta")){
                                            etComment10.setText(encuesta.getJSONObject(x).getString("explicacion_respuesta"));
                                        }
                                    }
                                    break;
                            }

                            try {
                                //Validamos si tenemos pregunta  de sugerencias
                                if(encuesta.getJSONObject(x).has("pregunta")) {
                                    if (encuesta.getJSONObject(x).getString("pregunta").equals("Sugerencias o comentarios")) {
                                        etSugerencias.setText(encuesta.getJSONObject(x).getString("respuesta"));
                                    }
                                }
                            }catch (Throwable e){
                                Log.e("TAG", "llenarCamposDeEncuesta: Error al consultar sugerencias y comentarios" + e.getMessage());
                            }


                        }catch (Throwable e){
                            Log.e("TAG", "llenarCamposDeEncuesta: Error en asignar las posiciones de encuesta");
                        }
                    }

               /* try {
                    //Validamos si tenemos pregunta  de sugerencias
                    if(encuesta.getJSONObject(7).has("pregunta")) {
                        if (encuesta.getJSONObject(7).getString("pregunta").equals("Sugerencias o comentarios")) {
                            etSugerencias.setText(encuesta.getJSONObject(7).getString("respuesta"));
                        }
                    }
                }catch (Throwable e){
                    Log.e("TAG", "llenarCamposDeEncuesta: Error al consultar sugerencias y comentarios" + e.getMessage());
                }*/

                } catch (Throwable e) {
                    e.printStackTrace();
                }

                bloquearCampos();

            }

        }else
            limpiarTodo(true);
    }


    private void bloquearCampos() {

        //  Toast.makeText(getApplicationContext(), "BloquearCampos", Toast.LENGTH_LONG).show();
        /*RadioGroup radioGroup1 = (RadioGroup) findViewById(R.id.radioGroup1);
        RadioGroup radioGroup2 = (RadioGroup) findViewById(R.id.radioGroup2);
        RadioGroup radioGroup3 = (RadioGroup) findViewById(R.id.radioGroup3);
        RadioGroup radioGroup4 = (RadioGroup) findViewById(R.id.radioGroup4);
        RadioGroup radioGroup5 = (RadioGroup) findViewById(R.id.radioGroup5);
        RadioGroup radioGroup6 = (RadioGroup) findViewById(R.id.radioGroup6);
        RadioGroup radioGroup7 = (RadioGroup) findViewById(R.id.radioGroup7);
        radioGroup1.setEnabled(false);
        radioGroup2.setEnabled(false);
        radioGroup3.setEnabled(false);
        radioGroup4.setEnabled(false);
        radioGroup5.setEnabled(false);
        radioGroup6.setEnabled(false);
        radioGroup7.setEnabled(false);

        etNombreFamiliar.setEnabled(false);
        etParentesco.setEnabled(false);
        etSugerencias.setEnabled(false);
        btGuardarEncuesta.setEnabled(false);

        rb11.setEnabled(false);
        rb12.setEnabled(false);
        rb13.setEnabled(false);
        rb14.setEnabled(false);
        rb21.setEnabled(false);
        rb22.setEnabled(false);
        rb31.setEnabled(false);
        rb32.setEnabled(false);
        rb41.setEnabled(false);
        rb42.setEnabled(false);
        rb51.setEnabled(false);
        rb52.setEnabled(false);
        rb61.setEnabled(false);
        rb62.setEnabled(false);
        rb63.setEnabled(false);
        rb64.setEnabled(false);
        rb71.setEnabled(false);
        rb72.setEnabled(false);
        rb73.setEnabled(false);
        rb74.setEnabled(false);

        respuestaNo2.setEnabled(false);
        respuestaNo3.setEnabled(false);
        respuestaNo4.setEnabled(false);
        respuestaNo5.setEnabled(false);

        btGuardarEncuesta.setEnabled(false);
        btSiguiente1.setEnabled(false);
        btSiguiente2.setEnabled(false);
        btSiguiente3.setEnabled(false);
        btSiguiente4.setEnabled(false);
        btSiguiente5.setEnabled(false);
        btSiguiente6.setEnabled(false);
        btSiguiente7.setEnabled(false);*/

    }


    private void desbloquearCampos() {

        RadioGroup radioGroup1 = (RadioGroup) findViewById(R.id.radioGroup1);
        RadioGroup radioGroup2 = (RadioGroup) findViewById(R.id.radioGroup2);
        RadioGroup radioGroup3 = (RadioGroup) findViewById(R.id.radioGroup3);
        RadioGroup radioGroup4 = (RadioGroup) findViewById(R.id.radioGroup4);
        RadioGroup radioGroup5 = (RadioGroup) findViewById(R.id.radioGroup5);
        RadioGroup radioGroup6 = (RadioGroup) findViewById(R.id.radioGroup6);
        RadioGroup radioGroup7 = (RadioGroup) findViewById(R.id.radioGroup7);
        radioGroup1.setEnabled(true);
        radioGroup2.setEnabled(true);
        radioGroup3.setEnabled(true);
        radioGroup4.setEnabled(true);
        radioGroup5.setEnabled(true);
        radioGroup6.setEnabled(true);
        radioGroup7.setEnabled(true);
        radioGroup8.setEnabled(true);
        radioGroup9.setEnabled(true);
        radioGroup10.setEnabled(true);

        etNombreFamiliar.setEnabled(true);
        etParentesco.setEnabled(true);
        etSugerencias.setEnabled(true);
        btGuardarEncuesta.setEnabled(true);

        rb11.setEnabled(true);
        rb12.setEnabled(true);
        rb13.setEnabled(true);
        rb14.setEnabled(true);
        rb21.setEnabled(true);
        rb22.setEnabled(true);
        rb31.setEnabled(true);
        rb32.setEnabled(true);
        rb41.setEnabled(true);
        rb42.setEnabled(true);
        rb51.setEnabled(true);
        rb52.setEnabled(true);
        rb61.setEnabled(true);
        rb62.setEnabled(true);
        rb63.setEnabled(true);
        rb64.setEnabled(true);
        rb71.setEnabled(true);
        rb72.setEnabled(true);
        rb73.setEnabled(true);
        rb74.setEnabled(true);

        btGuardarEncuesta.setEnabled(true);

    }


    private void showMyCustomDialog() {
        final FrameLayout flLoading = (FrameLayout) findViewById(R.id.layoutCargando);
        flLoading.setVisibility(View.VISIBLE);
    }

    private void dismissMyCustomDialog() {
        final FrameLayout flLoading = (FrameLayout) findViewById(R.id.layoutCargando);
        flLoading.setVisibility(View.GONE);
    }


    @Override
    protected void onResume() {
        super.onResume();

        /*try {
            guardarImagenCargadaEnCarpetaEbita(bitmap, "" + imageName);
        } catch (IOException e) {
            e.printStackTrace();
        }*/

    }


    public void showBottomLayoutForHorariosDeAsistencia(View view, boolean actualmenteEsEntrada) {
        try {
            String tipoDEntrada="";
            RadioButton bt08am, bt20pm, bt7am, bt8am, bt9am, bt19, bt20, bt7amm, bt199, bt19a9;
            NeumorphButton btGuardarAsistencia;
            TextView btCerrarAsistencia;
            TextView tvMensaje, tvOrigen, tvDestino, etTituloAsistencia;
            BottomSheetDialog mBottomSheetDialog = null;
            mBottomSheetDialog = new BottomSheetDialog(Supervisores.this);
            mBottomSheetDialog.setContentView(R.layout.bottom_layout_horarios);
            mBottomSheetDialog.setCancelable(false);
            mBottomSheetDialog.setCanceledOnTouchOutside(false);

            bt08am = (RadioButton) mBottomSheetDialog.findViewById(R.id.bt08am);
            bt20pm = (RadioButton) mBottomSheetDialog.findViewById(R.id.bt20pm);
            bt7am = (RadioButton) mBottomSheetDialog.findViewById(R.id.bt7am);
            bt8am = (RadioButton) mBottomSheetDialog.findViewById(R.id.bt8am);
            bt9am = (RadioButton) mBottomSheetDialog.findViewById(R.id.bt9am);
            bt19 = (RadioButton) mBottomSheetDialog.findViewById(R.id.bt19);
            bt7amm = (RadioButton) mBottomSheetDialog.findViewById(R.id.bt7amm);
            bt20 = (RadioButton) mBottomSheetDialog.findViewById(R.id.bt20);
            bt199 = (RadioButton) mBottomSheetDialog.findViewById(R.id.bt199);
            bt19a9 = (RadioButton) mBottomSheetDialog.findViewById(R.id.bt19a9);
            btGuardarAsistencia =(NeumorphButton) mBottomSheetDialog.findViewById(R.id.btGuardarAsistencia);
            tvMensaje =(TextView) mBottomSheetDialog.findViewById(R.id.tvMensaje);
            tvOrigen =(TextView) mBottomSheetDialog.findViewById(R.id.tvOrigen);
            tvDestino =(TextView) mBottomSheetDialog.findViewById(R.id.tvDestino);
            etTituloAsistencia =(TextView) mBottomSheetDialog.findViewById(R.id.etTituloAsistencia);
            btCerrarAsistencia =(TextView) mBottomSheetDialog.findViewById(R.id.btCerrarAsistencia);


            BottomSheetDialog finalMBottomSheetDialog = mBottomSheetDialog;
            btCerrarAsistencia.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finalMBottomSheetDialog.dismiss();
                }
            });

            tvOrigen.setText("Entrada: " + DatabaseAssistant.getLastHoraEntrada());
            tvDestino.setText("Próxima salida: " + DatabaseAssistant.getLastHoraSalida());


            LinearLayout layoutRegistrosEntradaSalida = (LinearLayout) mBottomSheetDialog.findViewById(R.id.layoutRegistrosEntradaSalida);
            if(!DatabaseAssistant.getLastHoraEntrada().equals(""))
                layoutRegistrosEntradaSalida.setVisibility(View.VISIBLE);
            else
                layoutRegistrosEntradaSalida.setVisibility(View.GONE);


            if(actualmenteEsEntrada){
                //mostrar las horas de salida
                tipoDEntrada = "4";
                LinearLayout layoutHoraDeEntrada =(LinearLayout) mBottomSheetDialog.findViewById(R.id.layoutHoraDeEntrada);
                layoutHoraDeEntrada.setVisibility(View.GONE);
                btGuardarAsistencia.setText("Guardar salida");
                etTituloAsistencia.setText("Registro de salida");

            }else {
                //mostrar las horas de entrada
                tipoDEntrada = "3";
                layoutRegistrosEntradaSalida.setVisibility(View.GONE);
                btGuardarAsistencia.setText("Guardar entrada");
                etTituloAsistencia.setText("Registro de entrada");
            }


            String finalTipoDEntrada = tipoDEntrada;
            BottomSheetDialog finalMBottomSheetDialog1 = mBottomSheetDialog;
            btGuardarAsistencia.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(!bt08am.isChecked() && !bt20pm.isChecked() && !bt7am.isChecked() &&  !bt8am.isChecked() && !bt9am.isChecked() && !bt199.isChecked() && !bt19a9.isChecked() && !actualmenteEsEntrada){
                        Toast.makeText(Supervisores.this, "Selecciona un horario para continuar", Toast.LENGTH_SHORT).show();
                    }else{
                        String tipoDeHorario="", horaSeleccionada="", horaEntrada="", horaSalida="";
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String[] arregloCoordenadas = ApplicationResourcesProvider.getCoordenadasFromApplication();
                        Calendar cal = Calendar.getInstance();

                        try {
                            if (bt08am.isChecked()) {
                                horaSeleccionada = "08:00 - 20:00 hrs";
                                horaEntrada = "08:00 hrs";
                                horaSalida = "20:00 hrs";
                            }else if (bt20pm.isChecked()) {
                                horaSeleccionada = "20:00 - 08:00 hrs";
                                horaEntrada = "20:00 hrs";
                                horaSalida = "08:00 hrs";
                            }else if (bt7am.isChecked()) {
                                horaSeleccionada = "07:00 - 19:00 hrs";
                                horaEntrada = "07:00 hrs";
                                horaSalida = "19:00 hrs";
                            }else if (bt8am.isChecked()) {
                                horaSeleccionada = "08:00 - 19:00 hrs";
                                horaEntrada = "08:00 hrs";
                                horaSalida = "19:00 hrs";
                            }else if (bt9am.isChecked()) {
                                horaSeleccionada = "09:00 - 19:00 hrs";
                                horaEntrada = "09:00 hrs";
                                horaSalida = "19:00 hrs";
                            }else if (bt199.isChecked()) {
                                horaSeleccionada = "19:00 - 07:00 hrs";
                                horaEntrada = "19:00 hrs";
                                horaSalida = "07:00 hrs";
                            }
                            else if (bt19a9.isChecked()) {
                                horaSeleccionada = "19:00 - 09:00 hrs";
                                horaEntrada = "19:00 hrs";
                                horaSalida = "09:00 hrs";
                            }else {
                                horaSeleccionada = DatabaseAssistant.getLastHoraEntrada().substring(0, 5) + " - " + DatabaseAssistant.getLastHoraSalida().substring(0, 5) + " hrs";
                                horaEntrada = DatabaseAssistant.getLastHoraEntrada();
                                horaSalida = DatabaseAssistant.getLastHoraSalida();
                            }
                        }catch (Throwable e){
                            Log.e("TAG", "onClick: Error" + e.getMessage());
                        }

                        if(finalTipoDEntrada.equals("3") && horaSeleccionada.equals("00:00:00") || horaSeleccionada == null){
                            Toast.makeText(Supervisores.this, "Debes seleccionar una hora para guardar tu asistencia.", Toast.LENGTH_SHORT).show();
                        }else
                        {
                            DatabaseAssistant.insertarAsistencia(
                                    "" + DatabaseAssistant.getUserNameFromSesiones(),
                                    "",
                                    "" + dateFormat.format(new Date()),
                                    "" + arregloCoordenadas[0],
                                    "" + arregloCoordenadas[1],
                                    "" + finalTipoDEntrada,
                                    "" + timeFormat.format(cal.getTime()),
                                    "0",
                                    "" + (Preferences.getPreferenceIsbunker(Supervisores.this, Preferences.PREFERENCE_ISBUNKER) ? "1" : "0"),
                                    "" + DatabaseAssistant.getIsProveedor(),
                                    Preferences.getGeofenceActual(getApplicationContext(), Preferences.PREFERENCE_GEOFENCE_ACTUAL),
                                    "" + horaSeleccionada,
                                    "" + horaEntrada,
                                    ""+ horaSalida
                            );

                            if(finalTipoDEntrada.equals("3")){
                                Preferences.setPreferencesAsistenciaEntrada(Supervisores.this, true, Preferences.PREFERENCE_ASISTENCIA_ENTRADA);

                            }else {
                                Preferences.setPreferencesAsistenciaEntrada(Supervisores.this, false, Preferences.PREFERENCE_ASISTENCIA_ENTRADA);
                            }

                            try {

                                TextView textoSinBitacora =(TextView) findViewById(R.id.textoSinBitacora);
                                textoSinBitacora.setText(DatabaseAssistant.getLastestTimeSesion());

                                TextView tvUltimo =(TextView) findViewById(R.id.tvUltimo);
                                if(Preferences.getPreferencesAsistenciaEntrada(Supervisores.this, Preferences.PREFERENCE_ASISTENCIA_ENTRADA)) {
                                    //mostrar las horas de salida
                                    tvUltimo.setText("Último registro de entrada:");
                                }else {
                                    tvUltimo.setText("Último registro de salida:");
                                }

                                LayoutInflater inflater = getLayoutInflater();
                                View view = inflater.inflate(R.layout.custom_toast_layout, (ViewGroup) findViewById(R.id.relativeLayout1));
                                LottieAnimationView lottieAnimationView = view.findViewById(R.id.imageView1);
                                lottieAnimationView.setAnimation("success_toast.json");
                                lottieAnimationView.loop(false);
                                lottieAnimationView.playAnimation();
                                Toast toast = new Toast(getApplicationContext());
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.setDuration(Toast.LENGTH_LONG);
                                toast.setView(view);
                                toast.show();
                                if (finalMBottomSheetDialog1.isShowing())
                                    finalMBottomSheetDialog1.dismiss();
                                Log.d("TAG", "onClick: Asistencia insertada correctamente");
                            }catch (Throwable e){
                                Log.e("TAG", "onClick: Error en layout de guadrdado correctamente " + e.getMessage() );
                            }
                        }
                    }
                }
            });

            mBottomSheetDialog.show();
        } catch (Throwable e) {
            Log.e("TAG", "Error en showDownloadsPDF(): " + e.toString());
        }

    }

    private void limpiarTodo(Boolean flag){
        RadioGroup radioGroup1 = (RadioGroup) findViewById(R.id.radioGroup1);
        RadioGroup radioGroup2 = (RadioGroup) findViewById(R.id.radioGroup2);
        RadioGroup radioGroup3 = (RadioGroup) findViewById(R.id.radioGroup3);
        RadioGroup radioGroup4 = (RadioGroup) findViewById(R.id.radioGroup4);
        RadioGroup radioGroup5 = (RadioGroup) findViewById(R.id.radioGroup5);
        RadioGroup radioGroup6 = (RadioGroup) findViewById(R.id.radioGroup6);
        RadioGroup radioGroup7 = (RadioGroup) findViewById(R.id.radioGroup7);
        RadioGroup radioGroup8 = (RadioGroup) findViewById(R.id.radioGroup8);
        RadioGroup radioGroup9 = (RadioGroup) findViewById(R.id.radioGroup9);
        RadioGroup radioGroup10 = (RadioGroup) findViewById(R.id.radioGroup10);

        radioGroup1.clearCheck();
        radioGroup2.clearCheck();
        radioGroup3.clearCheck();
        radioGroup4.clearCheck();
        radioGroup5.clearCheck();
        radioGroup6.clearCheck();
        radioGroup7.clearCheck();
        radioGroup8.clearCheck();
        radioGroup9.clearCheck();
        radioGroup10.clearCheck();

        question1 = "";
        question2 = "";
        question3 = "";
        question4 = "";
        question5 = "";
        question6 = "";
        question7 = "";
        question8 = "";
        question9 = "";
        question10 = "";

        etComment1.setText("");
        etComment2.setText("");
        etComment3.setText("");
        etComment4.setText("");
        etComment5.setText("");
        etComment6.setText("");
        etComment7.setText("");
        etComment8.setText("");
        etComment9.setText("");
        etComment10.setText("");

        etNombreFamiliar.setText("");
        etSugerencias.setText("");
        etParentesco.setText("");
        listaImagenes.clear();
        listaBitmaps.clear();



        CardView btDomicilio =(CardView) findViewById(R.id.btDomicilio);
        CardView btFuneraria =(CardView) findViewById(R.id.btFuneraria);

        btFuneraria.clearAnimation();
        btDomicilio.clearAnimation();

        TextView tvDomicilio = (TextView) findViewById(R.id.tvDomicilio);
        TextView tvFuneraria = (TextView) findViewById(R.id.tvFuneraria);

        Animation animationFuneraria = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out);

        LinearLayout contornoFuneraria = (LinearLayout) findViewById(R.id.contornoFuneraria);
        LinearLayout contornoDomicilio = (LinearLayout) findViewById(R.id.contornoDomicilio);


        if(!funeriaPresionada) {
            contornoFuneraria.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_transparent));
        }
        if(!domicilioPresionado) {
            contornoDomicilio.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style_transparent));
        }

        if(!flag){
            domicilioPresionado = false;
            funeriaPresionada = false;
        }

    }


    public void showErrorDialog(final String codeError, String bitacora) {
        final NeumorphButton btNo, btSi;
        TextView tvCodeError;

        TextInputLayout etDescripcionPeticion2;
        Dialog dialogoError = new Dialog(Supervisores.this);
        dialogoError.setContentView(R.layout.layout_error);
        dialogoError.setCancelable(true);
        etDescripcionPeticion2 = (TextInputLayout) dialogoError.findViewById(R.id.etDescripcionPeticion2);
        btNo = (NeumorphButton) dialogoError.findViewById(R.id.btNo);
        btSi = (NeumorphButton) dialogoError.findViewById(R.id.btSi);
        tvCodeError = (TextView) dialogoError.findViewById(R.id.tvCodeError);
        tvCodeError.setText(codeError);
        etDescripcionPeticion2.setVisibility(View.GONE);


        btNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(codeError.equals("¿Cerrar sesión?")){
                    btCerrarAsistencia.setChecked(IconSwitch.Checked.RIGHT);
                }
                dialogoError.dismiss();
            }
        });

        btSi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if (codeError.equals("¿Cerrar sesión?"))
                {
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
                    Calendar cal = Calendar.getInstance();
                    Preferences.setPreferenceCheckinCheckoutAssistant(Supervisores.this, false, Preferences.PREFERENCE_CHECKIN_CHECKOUT_ASSISTANT);
                    String[] coordenadasFromApplication = ApplicationResourcesProvider.getCoordenadasFromApplication();
                    String[] dataSessions = DatabaseAssistant.getLastedDataFromSessions();

                    if (coordenadasFromApplication.length > 0 && dataSessions.length > 0) {
                        DatabaseAssistant.insertarSesiones(
                                "" + DatabaseAssistant.getCodigoUsuario(),
                                "" + dataSessions[1],
                                "" + dateFormat.format(new Date()),
                                "" + coordenadasFromApplication[0],
                                "" + coordenadasFromApplication[1],
                                "2",
                                "" + timeFormat.format(cal.getTime()),
                                "0",
                                "0",
                                "0",
                                Preferences.getGeofenceActual(getApplicationContext(), Preferences.PREFERENCE_GEOFENCE_ACTUAL),
                                "" + DatabaseAssistant.getLastIsFuneraria(),
                                ""+ DatabaseAssistant.getUserNameFromSesiones(),
                                ""+ DatabaseAssistant.getLastesCheckArticulosVelacion(),
                                "" + DatabaseAssistant.getMacAddress(),
                                "" + DatabaseAssistant.getLastIsSupervisor(),
                                "" + DatabaseAssistant.getLastBloqueo(),
                                "" + DatabaseAssistant.getLastTimeForSyncLocations()
                        );
                        dialogoError.dismiss();
                        Toast.makeText(getApplicationContext(), "Hasta luego", Toast.LENGTH_SHORT).show();

                        finishAffinity();

                    } else
                        Toast.makeText(Supervisores.this, "Ocurrio un error al cerrar tu sesión, intenta nuevamente", Toast.LENGTH_SHORT).show();



                } else {
                    dialogoError.dismiss();
                }
            }
        });
        dialogoError.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogoError.show();

    }

    private void downloadParentescos() {
        showMyCustomDialog();

        JSONObject params = new JSONObject();
        try {
            params.put("usuario", DatabaseAssistant.getUserNameFromSesiones());
            params.put("token_device", DatabaseAssistant.getTokenDeUsuario());
            params.put("isProveedor", "0");
        } catch (JSONException e) {
            e.printStackTrace();
            dismissMyCustomDialog();
        }

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_PARENTESCOS_URL, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {

                JSONArray jsonArrayParentescos = new JSONArray();
                try {
                    jsonArrayParentescos = response.getJSONArray("parentescos");

                    if(jsonArrayParentescos.length()>0) {
                        Parentescos.deleteAll(Parentescos.class);
                        for (int i = 0; i <= jsonArrayParentescos.length() - 1; i++) {
                            DatabaseAssistant.insertParentesco(
                                    "" + jsonArrayParentescos.getJSONObject(i).getString("name")
                            );
                        }
                    }
                    else
                        Toast.makeText(getApplicationContext(), "Ocurrio un error al consultar el catálogo de parentescos, vuelve a intentarlo", Toast.LENGTH_LONG).show();

                    dismissMyCustomDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                    dismissMyCustomDialog();
                    Toast.makeText(Supervisores.this, "Ocurrio un error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        dismissMyCustomDialog();
                        Toast.makeText(Supervisores.this, "Ocurrio un error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(90000, 1, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);

    }


    private void showOptions(String bitacoraEncontrada) {

        if(!domicilioPresionado && !funeriaPresionada) {
            showErrorBtn();
        }
        else{
            final CharSequence[] option = {"Tomar foto", "Elegir de galeria", "Cancelar"};
            final AlertDialog.Builder builder = new AlertDialog.Builder(Supervisores.this);
            builder.setTitle("Eleige una opción");
            builder.setItems(option, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (option[which] == "Tomar foto") {
                        openCamera(bitacoraEncontrada);
                    } else if (option[which] == "Elegir de galeria") {
                        showFileChooser();
                    } else {
                        dialog.dismiss();
                    }
                }
            });

            builder.show();
        }
    }

    private void openCamera(String bitacoraEncontrada)
    {




        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ContentResolver resolver = getContentResolver();
            ContentValues contentValues = new ContentValues();
            contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name);
            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/png");
            contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, "DCIM/" + MEDIA_DIRECTORY);
            Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
            fos = resolver.openOutputStream(imageUri);
        } else {
            String imagesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + MEDIA_DIRECTORY + File.separator + imageName;
            File file = new File(imagesDir);

            if (!file.exists()) {
                file.mkdir();
            }
            File image = new File(imagesDir, name + ".jpg");
            fos = new FileOutputStream(image);
        }*/





        //File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + MEDIA_DIRECTORY + File.separator + imageName);
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), MEDIA_DIRECTORY);
        boolean isDirectoryCreated = file.exists();

        if(!isDirectoryCreated)
            isDirectoryCreated = file.mkdirs();


        if(isDirectoryCreated) {
            Long timestamp = System.currentTimeMillis() / 1000;
            imageName =  DatabaseAssistant.getUserNameFromSesiones() +"_"+ bitacoraEncontrada + "_" + timestamp.toString() + ".jpg";
            //imageName =  DatabaseAssistant.getUserNameFromSesiones() +"_"+ bitacoraEncontrada + "_" + timestamp.toString();
            //mPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + File.separator + MEDIA_DIRECTORY + File.separator + imageName;
            mPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + MEDIA_DIRECTORY + File.separator + imageName;
            File newFile = new File(mPath);

            try {
                newFile.createNewFile();
            }
            catch (IOException e) {
                Log.v("CAMARA", "Error al abrir camara: " + e.getMessage());
            }

            Log.d("CAMARA", "onActivityResult:");
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID+ ".provider", newFile));
            startActivityForResult(intent, PHOTO_CODE);
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Seleciona imagen"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK)
        {
            switch (requestCode){
                case PHOTO_CODE:
                    MediaScannerConnection.scanFile(this, new String[]{mPath}, null, new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("CAMARA", "Scanned " + path + ":");
                            Log.i("CAMARA", "-> Uri = " + uri);
                        }
                    });

                    Bitmap bitmap = BitmapFactory.decodeFile(mPath);

                    String valor = String.valueOf((int)(Math.random() * 999999) * (-1) / 2);
                    listaImagenes.add(DatabaseAssistant.getUserNameFromSesiones()+"_" + bitacoraEncontrada + "_" +valor);
                    listaBitmaps.add(getStringImagen(bitmap));

                    /*if(listaBitmaps.size()>0 && listaImagenes.size()>0)
                        reCapturoImagenes = true;*/

                    //String bitacora, String foto, String bitmap, String fechacaptura, String latitud, String longitud
                    if(listaBitmaps.size()>0)
                    {
                        String[] arregloCoordenadas = ApplicationResourcesProvider.getCoordenadasFromApplication();
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        for(int i=0; i<=listaBitmaps.size()-1;i++)
                        {
                            DatabaseAssistant.insertarFotografias(
                                    "" + bitacoraEncontrada,
                                    "" + listaImagenes.get(i).toString(),
                                    "" + listaBitmaps.get(i).toString(),
                                    "" + dateFormat.format(new Date()),
                                    "" + Double.parseDouble(arregloCoordenadas[0]),
                                    "" + Double.parseDouble(arregloCoordenadas[1]),
                                    domicilioPresionado ? "Domicilio" : funeriaPresionada ? "Funeraria" : "No seleccionado"
                            );
                        }


                        LayoutInflater inflater = getLayoutInflater();
                        View view = inflater.inflate(R.layout.custom_toast_layout, (ViewGroup) findViewById(R.id.relativeLayout1));
                        LottieAnimationView lottieAnimationView = view.findViewById(R.id.imageView1);
                        lottieAnimationView.setAnimation("success_toast.json");
                        lottieAnimationView.loop(false);
                        lottieAnimationView.playAnimation();
                        Toast toast = new Toast(getApplicationContext());
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.setDuration(Toast.LENGTH_LONG);
                        toast.setView(view);
                        toast.show();

                        try {
                            ExifInterface ei = new ExifInterface(mPath);
                            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

                            Bitmap rotatedBitmap = null;
                            switch (orientation) {

                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    rotatedBitmap = rotateImage(bitmap, 90);
                                    break;

                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    rotatedBitmap = rotateImage(bitmap, 180);
                                    break;

                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    rotatedBitmap = rotateImage(bitmap, 270);
                                    break;

                                case ExifInterface.ORIENTATION_NORMAL:
                                default:
                                    rotatedBitmap = bitmap;
                            }

                            modelBitacorasActivas.add(new ModelFotografiasTomadas(rotatedBitmap));
                            adapterFotografiasTomadas = new AdapterFotografiasTomadas(getApplicationContext(), modelBitacorasActivas);
                            rvImagenes.setAdapter(adapterFotografiasTomadas);

                        }catch (Throwable e){
                            Log.e("TAG", "onActivityResult: asdasdasd");
                        }

                        //modelBitacorasActivas.add(new ModelFotografiasTomadas(bitmap));
                        //adapterFotografiasTomadas = new AdapterFotografiasTomadas(getApplicationContext(), modelBitacorasActivas);
                        //rvImagenes.setAdapter(adapterFotografiasTomadas);
                    }


                    break;

                case SELECT_PICTURE:
                    Uri path = data.getData();
                    try
                    {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                        //imgDomicilio.setImageURI(path);
                    }catch (IOException e)
                    {
                        e.printStackTrace();
                        Log.d("CAMARA", "onActivityResult: Error: " + e.getLocalizedMessage());
                    }

                    break;
            }
        }

        //----------------Elegir imagen-------------------------------
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            try {
                Uri selectedImage = data.getData();
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                String wholeID = DocumentsContract.getDocumentId(selectedImage);
                String id = wholeID.split(":")[1];
                String[] column = { MediaStore.Images.Media.DATA };
                String sel = MediaStore.Images.Media._ID + "=?";
                Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column, sel, new String[] { id }, null);

               /* String filesPath = "";
                int columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst()) {
                    filesPath = cursor.getString(columnIndex);
                }
                cursor.close();*/


                //long timestamp = System.currentTimeMillis() / 1000;
                imageName =  DatabaseAssistant.getUserNameFromSesiones() +"_"+ bitacoraEncontrada + "_" + Long.toString(System.currentTimeMillis() / 1000);
                /*filesPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + File.separator + MEDIA_DIRECTORY + File.separator + imageName;
                File newFile = new File(filesPath);
                try {
                    newFile.createNewFile();
                }
                catch (IOException e) {
                    Log.v("CAMARA", "Error al abrir camara: " + e.getMessage());
                }*/


                guardarImagenCargadaEnCarpetaEbita(bitmap, "" + imageName);
                String valor = String.valueOf((int)(Math.random() * 999999) * (1+1) / 2);
                listaImagenes.add(DatabaseAssistant.getUserNameFromSesiones()+"_" + bitacoraEncontrada+ "_"+valor);
                listaBitmaps.add(getStringImagen(bitmap));

                if(listaBitmaps.size()>0)
                {
                    String[] arregloCoordenadas = ApplicationResourcesProvider.getCoordenadasFromApplication();
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    for(int i=0; i<=listaBitmaps.size()-1;i++)
                    {
                        DatabaseAssistant.insertarFotografias(
                                "" + bitacoraEncontrada,
                                "" + listaImagenes.get(i).toString(),
                                "" + listaBitmaps.get(i).toString(),
                                "" + dateFormat.format(new Date()),
                                "" + Double.parseDouble(arregloCoordenadas[0]),
                                "" + Double.parseDouble(arregloCoordenadas[1]),
                                domicilioPresionado ? "Domicilio" : funeriaPresionada ? "Funeraria" : "No seleccionado"
                        );
                    }


                    LayoutInflater inflater = getLayoutInflater();
                    View view = inflater.inflate(R.layout.custom_toast_layout, (ViewGroup) findViewById(R.id.relativeLayout1));
                    LottieAnimationView lottieAnimationView = view.findViewById(R.id.imageView1);
                    lottieAnimationView.setAnimation("success_toast.json");
                    lottieAnimationView.loop(false);
                    lottieAnimationView.playAnimation();
                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(view);
                    toast.show();


                    //modelBitacorasActivas.clear();
                    //for (int i = 0; i <= listaBitmaps.size() - 1; i++) {
                    //  ModelFotografiasTomadas product = new ModelFotografiasTomadas(bitmap);
                    modelBitacorasActivas.add(new ModelFotografiasTomadas(bitmap));
                    //}
                    adapterFotografiasTomadas = new AdapterFotografiasTomadas(getApplicationContext(), modelBitacorasActivas);
                    rvImagenes.setAdapter(adapterFotografiasTomadas);
                }


                /*if(listaBitmaps.size()>0 && listaImagenes.size()>0)
                    reCapturoImagenes = true;*/

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Ocurrio un error en la carga de la imagen, verifica que los permisos estén activados en tu celular.", Toast.LENGTH_LONG).show();
            }
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private void guardarImagenCargadaEnCarpetaEbita(Bitmap bitmap, @NonNull String name) throws IOException {
        boolean saved;
        OutputStream fos;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ContentResolver resolver = getContentResolver();
            ContentValues contentValues = new ContentValues();
            contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name);
            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/png");
            contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, "DCIM/" + MEDIA_DIRECTORY);
            Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
            fos = resolver.openOutputStream(imageUri);
        } else {
            String imagesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + MEDIA_DIRECTORY + File.separator + imageName;

            File file = new File(imagesDir);

            if (!file.exists()) {
                file.mkdirs();
            }

            File image = new File(imagesDir, name + ".jpg");
            try
            {
                image.createNewFile();
            }
            catch (IOException e)
            {
                Log.e("Error: ",  e.getMessage());
            }
            fos = new FileOutputStream(image);

        }

        try {
            saved = bitmap.compress(Bitmap.CompressFormat.JPEG, 10, fos);
        }catch (Throwable e){
            Log.e("TAG", "guardarImagenCargadaEnCarpetaEbita: Error al guaedar la imagen" + e.getMessage());
        }

        fos.flush();
        fos.close();
    }


    public String getStringImagen(Bitmap bmp) {
        String encodedImage="";
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 10, baos);
            byte[] imageBytes = baos.toByteArray();
            encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
            return encodedImage;
        }catch (Throwable e){
            Log.e("TAG", "getStringImagen: " +  e.getMessage());
            Toast.makeText(this, "Coloca el dispositivo de forma vertical para guardar imagenes.", Toast.LENGTH_LONG).show();
        }
        return encodedImage;
    }


    @Override
    protected void onStart() {
        super.onStart();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(getApplicationContext()).checkLocationSettings(builder.build());

        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                resolvable.startResolutionForResult(Supervisores.this,
                                        LocationRequest.PRIORITY_HIGH_ACCURACY);
                            } catch (IntentSender.SendIntentException e) {
                                Log.e("TAG", "onComplete: error no importante");
                            } catch (ClassCastException e) {
                                Log.e("TAG", "onComplete: error no importante");
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            break;
                    }
                }
            }
        });

    }

}




