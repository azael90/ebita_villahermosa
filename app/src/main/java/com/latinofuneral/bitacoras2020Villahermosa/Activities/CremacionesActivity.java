package com.latinofuneral.bitacoras2020Villahermosa.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.latinofuneral.bitacoras2020Villahermosa.Database.Bitacoras;
import com.latinofuneral.bitacoras2020Villahermosa.Database.DatabaseAssistant;
import com.latinofuneral.bitacoras2020Villahermosa.Printer.BluetoothPrinter;
import com.latinofuneral.bitacoras2020Villahermosa.R;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.ApplicationResourcesProvider;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.ConstantsBitacoras;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.ForegroundService;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.Preferences;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import soup.neumorphism.NeumorphButton;
import soup.neumorphism.NeumorphCardView;

public class CremacionesActivity extends AppCompatActivity {

    private static final String TAG = "CREMACIONES";

    /** Componentes **/
    Button btBuscarBitacora, btCapturarEvento;
    AutoCompleteTextView etBitacora;
    LinearLayout layoutBitacora, layoutBotonesQR, layoutEventosCapturados;
    TextView tvBitacora, tvSiguienteEvento, tvSiguienteEventoLabel;
    ImageView btBack;
    NeumorphCardView btImprimir, btCapturarCodigo;

    /*** Variables ***/
    boolean errorStackTraceBitacoras = false, scannerEventos = false, scannerEventosInicio=false;
    String codigoErrorStackTraceBitacoras = "", bitacoraGlobal="", nombreFinado="", siguienteEvento="";


    /** Impresiones **/
    private BluetoothPrinter printer;
    public static final int width = 70;
    public static final int height = 230;
    public static final int speed = 2;
    public static final int density = 15;
    public static final int sensorType = 0;
    public static final int gapBlackMarkVerticalDistance = 0;
    public static  final int gapBlackMarkShiftDistance = 0;
    public static String mac = DatabaseAssistant.getMacAddress();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cremaciones);

        btBuscarBitacora =(Button) findViewById(R.id.btBuscarBitacora);
        btCapturarEvento =(Button) findViewById(R.id.btCapturarEvento);
        etBitacora =(AutoCompleteTextView) findViewById(R.id.etBitacora);
        layoutBitacora =(LinearLayout) findViewById(R.id.layoutBitacora);
        layoutBotonesQR =(LinearLayout) findViewById(R.id.layoutBotonesQR);
        layoutEventosCapturados =(LinearLayout) findViewById(R.id.layoutEventosCapturados);
        tvBitacora =(TextView) findViewById(R.id.tvBitacora);
        tvSiguienteEvento =(TextView) findViewById(R.id.tvSiguienteEvento);
        tvSiguienteEventoLabel =(TextView) findViewById(R.id.tvSiguienteEventoLabel);
        btBack =(ImageView) findViewById(R.id.btBack);
        btImprimir =(NeumorphCardView) findViewById(R.id.btImprimir);
        btCapturarCodigo =(NeumorphCardView) findViewById(R.id.btCapturarCodigo);

        layoutEventosCapturados.setVisibility(View.GONE);
        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btCapturarEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ApplicationResourcesProvider.checkInternetConnection())
                {
                    layoutEventosCapturados.setVisibility(View.GONE);
                    bitacoraGlobal = "";
                    scannerEventos = true;
                    new IntentIntegrator(CremacionesActivity.this).initiateScan();

                }else
                    showErrorDialog("No hay conexión a internet");
            }
        });

        btBuscarBitacora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etBitacora.getText().toString().equals("") && etBitacora.getText().toString().length() > 8) {
                    if (!DatabaseAssistant.laBitacoraEstaActiva(etBitacora.getText().toString().toUpperCase().trim())) {

                        if(DatabaseAssistant.getLastTipoEventoCremacion(etBitacora.getText().toString().toUpperCase().trim().trim()).equals("3"))
                        {
                            layoutEventosCapturados.setVisibility(View.VISIBLE);
                            tvSiguienteEvento.setText("Eventos completos");
                            tvSiguienteEventoLabel.setVisibility(View.VISIBLE);
                            btCapturarCodigo.setEnabled(false);
                            btCapturarCodigo.setAlpha(0.2f);
                        }else {
                            layoutEventosCapturados.setVisibility(View.GONE);
                            tvSiguienteEvento.setVisibility(View.GONE);
                            tvSiguienteEventoLabel.setVisibility(View.GONE);
                            btCapturarCodigo.setEnabled(true);
                            btCapturarCodigo.setAlpha(1f);
                            searchBitacoraInWeb(etBitacora.getText().toString().toUpperCase().trim());
                        }

                    }else
                        Toast.makeText(getApplicationContext(), "Ya tienes esta bitácora activa.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Por favor verificar la bitácora a buscar.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        btCapturarCodigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ApplicationResourcesProvider.checkInternetConnection())
                {
                    scannerEventos = true;
                    new IntentIntegrator(CremacionesActivity.this).initiateScan();
                }else
                    showErrorDialog("No hay conexión a internet");
            }
        });


        btImprimir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBluetoothAdapter()) {

                    if(!bitacoraGlobal.equals("")) {
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        executeImpresionDeCodigoDeBitacora(
                                "" + bitacoraGlobal,
                                "" + DatabaseAssistant.getFinadoPorBitacora(tvBitacora.getText().toString()),
                                "" + dateFormat.format(new Date())
                        );
                    }
                    else
                        showErrorDialog("Debes seleccionar una bitácora para imprimir...");

                }else
                    showErrorDialog("Bluetooth desactivado, verifica nuevamente...");
            }
        });

        etBitacora.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                bitacoraGlobal = "";
                layoutBitacora.setVisibility(View.GONE);
                layoutBotonesQR.setVisibility(View.GONE);
                layoutEventosCapturados.setVisibility(View.GONE);
                btCapturarCodigo.setEnabled(true);
                btCapturarCodigo.setAlpha(1.0f);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }


    private void searchBitacoraInWeb(String bitacora)
    {
        if (ApplicationResourcesProvider.checkInternetConnection()) {
            showMyCustomDialog();
            try {
                String token = FirebaseInstanceId.getInstance().getToken();
                Log.d("FIREBASE", "Refresh Token: " + token);
                if (token != null)
                    DatabaseAssistant.insertarToken(token);
                else
                    DatabaseAssistant.insertarToken("Unknown");
            } catch (Throwable e) {
                Log.e(TAG, "downloadBitacoras: " + e.getMessage());
            }

            JSONObject params = new JSONObject();
            try {
                params.put("usuario", DatabaseAssistant.getUserNameFromSesiones());
                params.put("token_device", DatabaseAssistant.getTokenDeUsuario());
                params.put("isProveedor", DatabaseAssistant.getIsProveedor());
                params.put("isBunker", Preferences.getPreferenceIsbunker(getApplicationContext(), Preferences.PREFERENCE_ISBUNKER) ? "1" : "0");
                params.put("bitacora", bitacora);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_DOWNLOAD_BITACORA_INDIVIDUAL, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Bitacoras.deleteAll(Bitacoras.class);
                    JSONArray bitacorasArray = new JSONArray();

                    try {
                        if(!response.getBoolean("error"))
                        {
                            if (response.has("bitacoraDrivers"))
                            {
                                try {
                                    bitacorasArray = response.getJSONArray("bitacoraDrivers");

                                    if(bitacorasArray.length()>0)
                                    {
                                        errorStackTraceBitacoras = false;
                                        for (int i = 0; i <= bitacorasArray.length() - 1; i++)
                                        {
                                            DatabaseAssistant.insertarBitacoras(
                                                    "" + i,
                                                    "" + bitacorasArray.getJSONObject(i).getString("name"),
                                                    "" + bitacorasArray.getJSONObject(i).getString("secondName"),
                                                    "" + bitacorasArray.getJSONObject(i).getString("address"),
                                                    "" + bitacorasArray.getJSONObject(i).getString("phones"),
                                                    "" + bitacorasArray.getJSONObject(i).getString("chofer"),
                                                    "" + bitacorasArray.getJSONObject(i).getString("ayudante"),
                                                    "" + bitacorasArray.getJSONObject(i).getString("vehiculo"),
                                                    "" + bitacorasArray.getJSONObject(i).getString("salida"),
                                                    "" + bitacorasArray.getJSONObject(i).getString("destino"),
                                                    "" + bitacorasArray.getJSONObject(i).getString("destino_domicilio"),
                                                    "" + bitacorasArray.getJSONObject(i).getString("destino_latitud"),
                                                    "" + bitacorasArray.getJSONObject(i).getString("destino_longitud"),
                                                    "" + bitacorasArray.getJSONObject(i).getString("ataud"),
                                                    "" + bitacorasArray.getJSONObject(i).getString("panteon"),
                                                    "" + bitacorasArray.getJSONObject(i).getString("tipo_servicio"),
                                                    "" + bitacorasArray.getJSONObject(i).getString("inicio_velacion"),
                                                    "" + bitacorasArray.getJSONObject(i).getString("inicio_cortejo"),
                                                    "" + bitacorasArray.getJSONObject(i).getString("templo")

                                            );
                                        }
                                        Log.d(TAG, "onResponse: Bitacora guardada y consultada correctamente");
                                        String bitacoraCargada =bitacorasArray.getJSONObject(0).getString("name").toUpperCase();
                                        tvBitacora.setText(bitacoraCargada);
                                        bitacoraGlobal = bitacoraCargada;

                                        nombreFinado = bitacorasArray.getJSONObject(0).getString("secondName").toUpperCase();
                                        /*try {
                                            siguienteEvento = bitacorasArray.getJSONObject(0).getString("siguiente_evento");
                                        }catch (Throwable e){
                                            Log.e(TAG, "onResponse: el siugiente evento es nulo: " + e.getMessage());
                                            siguienteEvento = "Sin eventos por capturar.";
                                        }*/

                                        errorStackTraceBitacoras = false;
                                        layoutBotonesQR.setVisibility(View.VISIBLE);
                                        //tvSiguienteEvento.setText(siguienteEvento);
                                        tvSiguienteEvento.setVisibility(View.GONE);
                                        tvSiguienteEventoLabel.setVisibility(View.GONE);
                                        layoutBitacora.setVisibility(View.VISIBLE);

                                        if(response.has("bitacorasDetails")){
                                            JSONObject bitacorasDetails = response.getJSONArray("bitacorasDetails").getJSONObject(0);

                                            DatabaseAssistant.deleteFinadosFromBitacora("" + bitacorasDetails.getString("bitacora"));
                                            DatabaseAssistant.insertarFinado(
                                                    "" + bitacorasDetails.getString("finado"),
                                                    "" + bitacorasDetails.getString("bitacora")
                                            );
                                        }

                                        dismissMyCustomDialog();
                                    }
                                    else{
                                        codigoErrorStackTraceBitacoras = "No hay datos de bitacoras desde WS";
                                        layoutBitacora.setVisibility(View.GONE);
                                        Toast.makeText(getApplicationContext(), "No encontramos información de la bitácora que ingresaste, verifica nuevamente.", Toast.LENGTH_SHORT).show();
                                        dismissMyCustomDialog();
                                    }
                                } catch (Throwable e) {
                                    errorStackTraceBitacoras = true;
                                    Log.e(TAG, "onResponse: Error al consultar y guardar la bitacora individual: " + e.getMessage());
                                    layoutBitacora.setVisibility(View.GONE);
                                    dismissMyCustomDialog();
                                    Toast.makeText(getApplicationContext(), "No encontramos información de la bitácora que ingresaste, verifica nuevamente.", Toast.LENGTH_SHORT).show();
                                }

                            }else {
                                codigoErrorStackTraceBitacoras = "No hay datos de bitacoras desde WS";
                                layoutBitacora.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), "No encontramos información de la bitácora que ingresaste, verifica nuevamente.", Toast.LENGTH_SHORT).show();
                                dismissMyCustomDialog();
                            }

                        }else{
                            codigoErrorStackTraceBitacoras = "Error en WS error = true";
                            layoutBitacora.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), "No encontramos información de la bitácora que ingresaste, verifica nuevamente.", Toast.LENGTH_SHORT).show();
                            dismissMyCustomDialog();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        errorStackTraceBitacoras = true;
                        codigoErrorStackTraceBitacoras = e.getMessage();
                        layoutBitacora.setVisibility(View.GONE);
                        dismissMyCustomDialog();
                        Toast.makeText(getApplicationContext(), "No encontramos información de la bitácora que ingresaste, verifica nuevamente.", Toast.LENGTH_SHORT).show();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            errorStackTraceBitacoras = true;
                            codigoErrorStackTraceBitacoras = error.getMessage();
                            layoutBitacora.setVisibility(View.GONE);
                            dismissMyCustomDialog();
                            Toast.makeText(getApplicationContext(), "No encontramos información de la bitácora que ingresaste, verifica nuevamente.", Toast.LENGTH_SHORT).show();
                        }
                    }) {

            };

            postRequest.setRetryPolicy(new DefaultRetryPolicy(90000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);

        } else
            showErrorDialog("No hay conexión a internet");
    }

    private void showMyCustomDialog() {
        final FrameLayout flLoading = (FrameLayout) findViewById(R.id.layoutCargando);
        flLoading.setVisibility(View.VISIBLE);
    }

    private void dismissMyCustomDialog() {
        final FrameLayout flLoading = (FrameLayout) findViewById(R.id.layoutCargando);
        flLoading.setVisibility(View.GONE);
    }

    public void showErrorDialog(final String codeError) {
        final NeumorphButton btNo, btSi;
        TextView tvCodeError, tvBitacora;
        EditText etDescripcionPeticion;
        Dialog dialogoError = new Dialog(CremacionesActivity.this);
        dialogoError.setContentView(R.layout.layout_error);
        dialogoError.setCancelable(true);
        etDescripcionPeticion = (EditText) dialogoError.findViewById(R.id.etDescripcionPeticion);
        btNo = (NeumorphButton) dialogoError.findViewById(R.id.btNo);
        btSi = (NeumorphButton) dialogoError.findViewById(R.id.btSi);
        tvCodeError = (TextView) dialogoError.findViewById(R.id.tvCodeError);
        tvCodeError.setText(codeError);

        tvBitacora = (TextView) dialogoError.findViewById(R.id.tvBitacora);
        tvBitacora.setVisibility(View.GONE);


        if(!codeError.equals("¿Cerrar sesión?"))
            btSi.setVisibility(View.INVISIBLE);

        if (codeError.equals("No hay conexión a internet")
                || codeError.equals("Bluetooth desactivado, verifica nuevamente..."))
            btSi.setVisibility(View.GONE);



        btNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogoError.dismiss();
            }
        });

        btSi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogoError.dismiss();
            }
        });


        dialogoError.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogoError.show();

    }


    public boolean checkBluetoothAdapter()
    {
        boolean status = false;
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBluetoothAdapter == null) {
            Log.d(TAG, "checkBluetoothAdapter: Parece que el bluethoot no es soportado");
            Toast.makeText(getApplicationContext(), "Ocurrio un error con tu teléfono...", Toast.LENGTH_SHORT).show();
        }
        else if (!mBluetoothAdapter.isEnabled()) {
            Log.d(TAG, "checkBluetoothAdapter: Bluethooth no activado");
        }
        else {
            Log.d(TAG, "checkBluetoothAdapter: El bluethoot esta activado");
            status = true;
        }

        return status;
    }


    private void executeImpresionDeCodigoDeBitacora(final String bitacoraParameter, final String nombreFinado, final String fechaHora)
    {
        /** Create new Thread for printer and connection process**/

        showMyCustomDialog();
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            impresion(bitacoraParameter, nombreFinado, fechaHora);
                        }
                    });

                    synchronized (this) {
                        wait(1000);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dismissMyCustomDialog();
                            }
                        });

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Ocurrio un error, intenta de nuevo", Toast.LENGTH_SHORT).show();
                }
            }

            ;
        };
        thread.start();

    }


    public void impresion(final String bitacora, final String nombreFinado, final String fechaHora)
    {

        try {
            printer = BluetoothPrinter.getInstance();
            if (!BluetoothPrinter.printerConnected)
            {
                printer.connect();
            }
            printer.setup(width, 53, speed, density, sensorType, gapBlackMarkVerticalDistance, gapBlackMarkShiftDistance);

            printer.clearbuffer();
            int x = 150;
            printer.sendcommand("GAP 0,0\n");
            printer.sendcommand("CLS\n");
            printer.sendcommand("CODEPAGE UTF-8\n");
            printer.sendcommand("SET TEAR ON\n");
            printer.sendcommand("SET HEAD ON\n");
            printer.sendcommand("SET REPRINT OFF\n");
            printer.sendcommand("SET COUNTER @1 1\n");

            printer.printerfont(100, 100, "1", 0, 1, 1, "Latinoamericana Recinto Funeral");
            x=x+30;
            printer.printQRCode(bitacora);
            x=x+60;
            printer.printerfont(140, x + 10, "4", 0, 1, 1, bitacora);
            x=x+80;
            printer.printerfont(100, x + 10, "1", 0, 1, 1, nombreFinado);
            x=x+25;
            printer.printerfont(100, x + 10, "1", 0, 1, 1, fechaHora);
            printer.printlabel(1, 1);

        }
        catch(Exception e)
        {
            try
            {
                printer.disconnect();
            }
            catch (Exception e1)
            {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null)
        {
            if (result.getContents() != null)
            {
                if(scannerEventos)
                {
                    if(ApplicationResourcesProvider.checkInternetConnection())
                    {
                        try {
                            if (result.getContents().contains("VSA") && (result.getContents().toString().length() == 11 ||  result.getContents().toString().length() == 12))
                            {
                                showMyCustomDialog();

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ForegroundService.createJsonForSync();
                                    }
                                });

                                JSONObject parametrosParaObtenerLosEventos = new JSONObject();
                                parametrosParaObtenerLosEventos.put("bitacora", result.getContents());

                                JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST,
                                        ConstantsBitacoras.WS_GET_LAST_CREMACION_EVENT, parametrosParaObtenerLosEventos, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {

                                        try {
                                            if (response.has("error")) {
                                                Log.d(TAG, "onResponse: Ocurrio un error en la respuesta del WS de GetLAstCremacion: " + response.getString("error_message"));
                                                Toast.makeText(getApplicationContext(), "Parece que el la bitácora es incorrecta", Toast.LENGTH_SHORT).show();
                                                dismissMyCustomDialog();
                                            } else {
                                                if (response.has("result")) {

                                                    /**Validar primero si result tiene datos**/
                                                    if (response.getJSONObject("result").length()>0)
                                                    {
                                                        JSONObject eventoJson = response.getJSONObject("result");

                                                        try {

                                                                /** Guardar aqui los camposs del nuevo proceso de eventos de cremacion **/
                                                                /** Validar primero el "Finalizado" para saber si seguimos con la validacion o no, Si el WS no responde nada o vacio, vamos a crear el evento 1 que es entrada a cremacion
                                                                 * y mostrar el siguiente evento a realizar....**/

                                                                int finalizado = eventoJson.getInt("finalizado");

                                                                if(finalizado == 1) {
                                                                    /** Terminar con las validaciones y no generar nuevos eventos **/
                                                                    showErrorDialog("Los escaneos ya estan completos, no podemos generar otro evento.");
                                                                    layoutEventosCapturados.setVisibility(View.VISIBLE);
                                                                    tvSiguienteEvento.setText("Eventos completos");
                                                                    tvSiguienteEvento.setVisibility(View.VISIBLE);
                                                                    tvSiguienteEventoLabel.setVisibility(View.VISIBLE);
                                                                    btCapturarCodigo.setEnabled(false);
                                                                    btCapturarCodigo.setAlpha(0.2f);
                                                                }
                                                                else if(finalizado== 0)
                                                                {
                                                                    /** consultar los eventos previos para incrementar el tipo_evento
                                                                     * en este punto ya tiene guardado el tipo_evento 1 y guardaremos tipo_evento  + 1  **/
                                                                    layoutEventosCapturados.setVisibility(View.GONE);
                                                                    btCapturarCodigo.setEnabled(true);
                                                                    btCapturarCodigo.setAlpha(1.0f);

                                                                    @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                                    showBottomGuardarEventoDeCremacion(
                                                                            "" + eventoJson.getString("bitacora"),
                                                                            "" + eventoJson.getString("siguiente_evento_id"),
                                                                            "" + dateFormat.format(new Date()),
                                                                            "" + DatabaseAssistant.getUserNameFromSesiones(),
                                                                            "",
                                                                            "0",
                                                                            "" + eventoJson.getString("siguiente_evento"),
                                                                            "" + eventoJson.getString("ultimo_evento")
                                                                    );
                                                                    dismissMyCustomDialog();


                                                                    /*DatabaseAssistant.insertarCremacionesEventos(
                                                                            "" + eventoJson.getString("bitacora"),
                                                                            "" + eventoJson.getString("siguiente_evento_id"),
                                                                            "" + dateFormat.format(new Date()),
                                                                            "" + DatabaseAssistant.getUserNameFromSesiones(),
                                                                            "",
                                                                            "0"
                                                                    );
                                                                    try {
                                                                        runOnUiThread(new Runnable() {
                                                                            @Override
                                                                            public void run() {
                                                                                ForegroundService.createJsonForSync();
                                                                            }
                                                                        });
                                                                    }catch (Throwable e){
                                                                        Log.e(TAG, "onResponse: Error al sincronizar el CremacionesActivity: " + e.getMessage());
                                                                    }
                                                                    toastEventoGuardado();*/

                                                                }



                                                            dismissMyCustomDialog();
                                                        } catch (Throwable e) {
                                                            Log.e(TAG, "onActivityResult: Ocurrio un error" + e.getMessage());
                                                            dismissMyCustomDialog();
                                                            showErrorDialog("Ocurrio un error: " + e.getMessage());
                                                        }
                                                    }
                                                    else if (response.getJSONObject("result").length() == 0)
                                                    {
                                                        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                        showBottomGuardarEventoDeCremacion(
                                                                "" + result.getContents(),
                                                                "1",
                                                                "" + dateFormat.format(new Date()),
                                                                "" + DatabaseAssistant.getUserNameFromSesiones(),
                                                                "",
                                                                "0",
                                                                "Inicio de cremación",
                                                                ""
                                                        );


                                                        /*DatabaseAssistant.insertarCremacionesEventos(
                                                                "" + bitacoraGlobal,
                                                                "1",
                                                                "" + dateFormat.format(new Date()),
                                                                "" + DatabaseAssistant.getUserNameFromSesiones(),
                                                                "",
                                                                "0"
                                                        );
                                                        try {
                                                            runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    ForegroundService.createJsonForSync();
                                                                }
                                                            });
                                                        }catch (Throwable e){
                                                            Log.e(TAG, "onResponse: Error al sincronizar el CremacionesActivity: " + e.getMessage());
                                                        }

                                                        tvSiguienteEvento.setText("Se capturó el inicio de cremación");
                                                        tvSiguienteEvento.setVisibility(View.VISIBLE);
                                                        toastEventoGuardado();*/
                                                        dismissMyCustomDialog();
                                                    }
                                                    else
                                                        Toast.makeText(CremacionesActivity.this, "Parece que ocurrio un error con la bitácora.", Toast.LENGTH_SHORT).show();






                                                    /*JSONObject parametrosParaObtenerLosEventos = new JSONObject();
                                                    parametrosParaObtenerLosEventos.put("bitacora", bitacoraGlobal);
                                                    JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST,
                                                            ConstantsBitacoras.WS_GET_LAST_CREMACION_EVENT, parametrosParaObtenerLosEventos, new Response.Listener<JSONObject>() {
                                                        @Override
                                                        public void onResponse(JSONObject response) {
                                                            try{

                                                                if (response.has("result"))
                                                                {
                                                                    if (response.getJSONObject("result").length() > 0)
                                                                    {
                                                                        JSONObject eventoJson = response.getJSONObject("result");
                                                                        try {
                                                                            if(eventoJson.getString("siguiente_evento") == null || eventoJson.getString("siguiente_evento").equals("")){
                                                                                tvSiguienteEvento.setText("Eventos de bitácora finalizados.");
                                                                            }else {
                                                                                tvSiguienteEvento.setText(eventoJson.getString("siguiente_evento"));
                                                                            }
                                                                            tvSiguienteEvento.setVisibility(View.VISIBLE);
                                                                            tvSiguienteEventoLabel.setVisibility(View.VISIBLE);
                                                                        }catch (Throwable e){
                                                                            Log.e(TAG, "onResponse: Error al cargar el siguient evento, es nullo: " +  e.getMessage());
                                                                            tvSiguienteEvento.setVisibility(View.GONE);
                                                                            tvSiguienteEventoLabel.setVisibility(View.GONE);
                                                                        }
                                                                    }
                                                                }


                                                            }catch (Throwable e){
                                                                Log.e(TAG, "onResponse: asasdasd " + e.getMessage() );
                                                            }
                                                        }
                                                    }, new Response.ErrorListener() {
                                                        @Override
                                                        public void onErrorResponse(VolleyError error) {
                                                            error.printStackTrace();
                                                            Log.e(TAG, "onErrorResponse: Error: " + error.getMessage());
                                                            dismissMyCustomDialog();
                                                            Toast.makeText(getApplicationContext(), "Ocurrio un error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                                                        }
                                                    }){

                                                    };
                                                    postRequest.setRetryPolicy(new DefaultRetryPolicy(90000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                                    VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);*/







                                                }
                                                else{
                                                    Log.d(TAG, "onResponse: No hay datos de la bitacora");
                                                    Toast.makeText(getApplicationContext(), "No se encontraros datos de la bitácora", Toast.LENGTH_SHORT).show();
                                                    dismissMyCustomDialog();
                                                }
                                            }
                                        } catch (JSONException e) {
                                            Log.e(TAG, "onErrorResponse: Error: " + e.getMessage());
                                            e.printStackTrace();
                                            Toast.makeText(getApplicationContext(), "Ocurrio un error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                            dismissMyCustomDialog();
                                        }
                                    }
                                },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                error.printStackTrace();
                                                Log.e(TAG, "onErrorResponse: Error: " + error.getMessage());
                                                dismissMyCustomDialog();
                                                Toast.makeText(getApplicationContext(), "Ocurrio un error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        }) {

                                };
                                postRequest.setRetryPolicy(new DefaultRetryPolicy(90000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);

                            }else {
                                showErrorDialog("Parece que el código es incorrecto");
                            }

                        } catch (Exception e) {
                            Log.e(TAG, "Error en creacion de json para status de bitacora");
                            Toast.makeText(this, "Ocurrio un error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }


                    }else
                        showErrorDialog("No hay conexión a internet");
                }
            }
            else
                Toast.makeText(getApplicationContext(), "Parece que el código es incorrecto", Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(getApplicationContext(), "Parece que el código es incorrecto", Toast.LENGTH_SHORT).show();
    }


    public void toastEventoGuardado(){
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.custom_toast_layout, (ViewGroup)findViewById(R.id.relativeLayout1));
        LottieAnimationView lottieAnimationView = view.findViewById(R.id.imageView1);
        lottieAnimationView.setAnimation("success_toast.json");
        lottieAnimationView.loop(false);
        lottieAnimationView.playAnimation();
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(view);
        toast.show();
    }



    public void showBottomGuardarEventoDeCremacion(String bitacora, String siguienteEventoID, String fecha, String usuario, String reciveCenizas, String sync, String siguienteEvento, String eventoAnterior)
    {
        try {
            Button btCancelar, btGuardar;
            TextView tvAnterior, tvSiguiente;
            View viewAnterior, viewSiguiente;
            LinearLayout layoutCenizas;
            AutoCompleteTextView etCenizas;

            BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(CremacionesActivity.this);
            mBottomSheetDialog.setContentView(R.layout.bottom_layout_confirmar_evento_cremacion);
            mBottomSheetDialog.setCancelable(false);
            mBottomSheetDialog.setCanceledOnTouchOutside(false);

            tvAnterior = (TextView) mBottomSheetDialog.findViewById(R.id.tvAnterior);
            tvSiguiente = (TextView) mBottomSheetDialog.findViewById(R.id.tvSiguiente);
            btCancelar = (Button) mBottomSheetDialog.findViewById(R.id.btCancelar);
            btGuardar = (Button) mBottomSheetDialog.findViewById(R.id.btGuardar);
            viewAnterior = (View) mBottomSheetDialog.findViewById(R.id.view);
            viewSiguiente = (View) mBottomSheetDialog.findViewById(R.id.view2);
            layoutCenizas = (LinearLayout) mBottomSheetDialog.findViewById(R.id.layoutCenizas);
            etCenizas = (AutoCompleteTextView) mBottomSheetDialog.findViewById(R.id.etCenizas);

            if(siguienteEvento.equals("Entrega de cenizas")){
                layoutCenizas.setVisibility(View.VISIBLE);
            }else
                layoutCenizas.setVisibility(View.GONE);

            if(eventoAnterior.equals("")){
                tvAnterior.setVisibility(View.INVISIBLE);
                viewAnterior.setVisibility(View.INVISIBLE);
            }

            if(siguienteEvento.equals("")){
                tvSiguienteEvento.setVisibility(View.GONE);
                viewSiguiente.setVisibility(View.INVISIBLE);
            }

            tvAnterior.setText("Evento anterior: " + eventoAnterior);
            tvSiguiente.setText("Siguiente evento: " + siguienteEvento);


            btCancelar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mBottomSheetDialog.dismiss();
                }
            });

            btGuardar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatabaseAssistant.insertarCremacionesEventos(
                            "" + bitacora,
                            "" + siguienteEventoID,
                            "" + fecha,
                            "" + usuario,
                            "" + etCenizas.getText().toString(),
                            "" + sync
                    );
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ForegroundService.createJsonForSync();
                            }
                        });
                    }catch (Throwable e){
                        Log.e(TAG, "onResponse: Error al sincronizar el CremacionesActivity: " + e.getMessage());
                    }
                    toastEventoGuardado();
                    mBottomSheetDialog.dismiss();
                }
            });
            mBottomSheetDialog.show();

        } catch (Throwable e) {
            Log.e(TAG, "Error en showDownloadsPDF(): " + e.toString());
        }
    }
}









