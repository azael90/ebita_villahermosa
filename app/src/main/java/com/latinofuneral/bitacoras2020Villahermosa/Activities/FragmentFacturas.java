package com.latinofuneral.bitacoras2020Villahermosa.Activities;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.kyanogen.signatureview.SignatureView;
import com.latinofuneral.bitacoras2020Villahermosa.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import soup.neumorphism.NeumorphButton;


public class FragmentFacturas extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_facturas);

        SignatureView signatureView = (SignatureView) findViewById(R.id.signature_view);
        NeumorphButton btLimpiarFirma = (NeumorphButton) findViewById(R.id.btLimpiarFirma);
        NeumorphButton btGuardarFirma = (NeumorphButton) findViewById(R.id.btGuardarFirma);

        btLimpiarFirma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signatureView.clearCanvas();
            }
        });

        btGuardarFirma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    guardarImagenCargadaEnCarpetaEbita(signatureView.getSignatureBitmap(), "" + imageName);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private static String MEDIA_DIRECTORY = "EBITA_IMAGES";
    String imageName;

    private void guardarImagenCargadaEnCarpetaEbita(Bitmap bitmap, @NonNull String name) throws IOException {
        boolean saved;
        OutputStream fos;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ContentResolver resolver = getContentResolver();
            ContentValues contentValues = new ContentValues();
            contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name);
            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/png");
            contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, "DCIM/" + MEDIA_DIRECTORY);
            Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
            fos = resolver.openOutputStream(imageUri);
        } else {
            String imagesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + MEDIA_DIRECTORY + File.separator + imageName;

            File file = new File(imagesDir);

            if (!file.exists()) {
                file.mkdirs();
            }

            File image = new File(imagesDir, name + ".jpg");
            try
            {
                image.createNewFile();
            }
            catch (IOException e)
            {
                Log.e("Error: ",  e.getMessage());
            }
            fos = new FileOutputStream(image);

        }

        try {
            saved = bitmap.compress(Bitmap.CompressFormat.JPEG, 10, fos);
        }catch (Throwable e){
            Log.e("TAG", "guardarImagenCargadaEnCarpetaEbita: Error al guaedar la imagen" + e.getMessage());
        }

        fos.flush();
        fos.close();
    }
}