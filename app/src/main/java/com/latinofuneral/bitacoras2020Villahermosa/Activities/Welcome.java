package com.latinofuneral.bitacoras2020Villahermosa.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.latinofuneral.bitacoras2020Villahermosa.R;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.ApplicationResourcesProvider;

import soup.neumorphism.NeumorphButton;

public class Welcome extends AppCompatActivity {

    private static final String TAG = "WELCOME";
    Button btLatino;
    TextView btProveedor;
    Dialog dialogoError;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        btLatino =(Button) findViewById(R.id.btLatino);
        btProveedor =(TextView) findViewById(R.id.btProveedor);
        dialogoError = new Dialog(Welcome.this);

        if(!ApplicationResourcesProvider.checkInternetConnection())
            showErrorDialog("No hay conexión a internet");

        btLatino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), Login.class);
                startActivity(intent);
            }
        });

        btProveedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), Login_Proveedor.class);
                startActivity(intent);
            }
        });
    }

    public void showErrorDialog(final String codeError) {
        final NeumorphButton btNo, btSi;
        TextView tvCodeError;
        dialogoError.setContentView(R.layout.layout_error);
        dialogoError.setCancelable(false);
        btNo = (NeumorphButton) dialogoError.findViewById(R.id.btNo);
        btSi = (NeumorphButton) dialogoError.findViewById(R.id.btSi);
        tvCodeError = (TextView) dialogoError.findViewById(R.id.tvCodeError);
        tvCodeError.setText(codeError);

        if (codeError.equals("No hay conexión a internet")){
            btSi.setVisibility(View.GONE);
            btNo.setText("Salir");
        }

        btNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogoError.dismiss();
                finishAffinity();
            }
        });

        dialogoError.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogoError.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}