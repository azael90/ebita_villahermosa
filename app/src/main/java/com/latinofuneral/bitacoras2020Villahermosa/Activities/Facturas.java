package com.latinofuneral.bitacoras2020Villahermosa.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.kyanogen.signatureview.SignatureView;
import com.latinofuneral.bitacoras2020Villahermosa.BuildConfig;
import com.latinofuneral.bitacoras2020Villahermosa.Database.DatabaseAssistant;
import com.latinofuneral.bitacoras2020Villahermosa.R;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.ApplicationResourcesProvider;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.ConstantsBitacoras;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.Preferences;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import soup.neumorphism.NeumorphButton;
import soup.neumorphism.NeumorphCardView;

public class Facturas extends AppCompatActivity {

    private static final String TAG = "FACTURAS";
    private String anioSeleccionado="", mesSeleccionado ="", bitacoraSeleccionada = "", movimientoSeleccionado ="";
    private LinearLayout layoutDatos;
    private CheckBox cbSolicitudFactura, cbSolicitudDesglose, cbAmbas;
    private ImageView imgCredencialAtras, imgCredencialFrente, imgFirma;
    private boolean isFrontINE = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facturas);

        NeumorphButton btGuardarFormulario =(NeumorphButton) findViewById(R.id.btGuardarFormulario);
        NeumorphCardView btManual =(NeumorphCardView) findViewById(R.id.btManual);
        NeumorphCardView btEscanearParaFacturar =(NeumorphCardView) findViewById(R.id.btEscanearParaFacturar);
        AutoCompleteTextView etBitacora = (AutoCompleteTextView) findViewById(R.id.etBitacora);
        cbSolicitudDesglose = (CheckBox) findViewById(R.id.cbSolicitudDesglose);
        cbSolicitudFactura = (CheckBox) findViewById(R.id.cbSolicitudFactura);
        cbAmbas = (CheckBox) findViewById(R.id.cbAmbas);
        imgCredencialAtras = (ImageView) findViewById(R.id.imgCredencialAtras);
        imgCredencialFrente = (ImageView) findViewById(R.id.imgCredencialFrente);
        imgFirma = (ImageView) findViewById(R.id.imgFirma);
        layoutDatos = (LinearLayout) findViewById(R.id.layoutDatos);




        ImageView btBack = (ImageView) findViewById(R.id.btBack);
        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btEscanearParaFacturar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //openDialogForRequestBinnacle("sdfsdfsdfsdf");

                if(ApplicationResourcesProvider.checkInternetConnection()) {
                    new IntentIntegrator(Facturas.this).initiateScan();
                }else{
                    AlertDialog.Builder pDialog = new AlertDialog.Builder(Facturas.this);
                    pDialog.setTitle("No tienes internet");
                    pDialog.setMessage("Verifica tu conexión a internet para continuar");
                    pDialog.setCancelable(true);
                    pDialog.show();
                }
            }
        });


        btManual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(ApplicationResourcesProvider.checkInternetConnection()) {
                    LinearLayout layoutManual = (LinearLayout) findViewById(R.id.layoutManual);
                    if(layoutManual.getVisibility()==View.VISIBLE) {
                        layoutManual.setVisibility(View.GONE);
                        ImageView imgArrow = (ImageView) findViewById(R.id.imgArrow);
                        imgArrow.setImageResource(R.drawable.ic_arrow_dowwn);
                    }else{
                        layoutManual.setVisibility(View.VISIBLE);
                        ImageView imgArrow = (ImageView) findViewById(R.id.imgArrow);
                        imgArrow.setImageResource(R.drawable.ic_arrow_up);
                    }
                }else{
                    AlertDialog.Builder pDialog = new AlertDialog.Builder(Facturas.this);
                    pDialog.setTitle("No tienes internet");
                    pDialog.setMessage("Verifica tu conexión a internet para continuar");
                    pDialog.setCancelable(true);
                    pDialog.show();
                }



            }
        });


        imgCredencialFrente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!bitacoraSeleccionada.equals("")) {
                    isFrontINE = true;
                    openCamera(bitacoraSeleccionada);
                }else
                    ApplicationResourcesProvider.showToastInformativo(Facturas.this, "Debes seleccionar una bitácora");
            }
        });

        imgCredencialAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!bitacoraSeleccionada.equals("")) {
                    isFrontINE = false;
                    openCamera(bitacoraSeleccionada);
                }else
                    ApplicationResourcesProvider.showToastInformativo(Facturas.this, "Debes seleccionar una bitácora");
            }
        });

        TextView tvAbrirFirma = (TextView) findViewById(R.id.tvAbrirFirma);
        tvAbrirFirma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Dialog or activity
                firmaTomada = false;
                openDialogSignature();
            }
        });

        TextView tvNoEsLaBitacora = (TextView) findViewById(R.id.tvNoEsLaBitacora);

        SpannableString mitextoU = new SpannableString("Buscar otra bitácora");
        mitextoU.setSpan(new UnderlineSpan(), 0, mitextoU.length(), 0);
        tvNoEsLaBitacora.setText(mitextoU);

        tvNoEsLaBitacora.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View view) {
                AutoCompleteTextView etBitacora = (AutoCompleteTextView) findViewById(R.id.etBitacora);
                LinearLayout layoutDatos = (LinearLayout) findViewById(R.id.layoutDatos);
                LinearLayout layoutManual = (LinearLayout) findViewById(R.id.layoutManual);
                NeumorphButton btBuscarBitacora = (NeumorphButton) findViewById(R.id.btBuscarBitacora);
                EditText etDigitosBitacora = (EditText) findViewById(R.id.etDigitosBitacora);
                TextView tvBitacoraEncontrada = (TextView) findViewById(R.id.tvBitacoraEncontrada);

                layoutDatos.setVisibility(View.GONE);
                layoutManual.setVisibility(View.VISIBLE);
                bitacoraSeleccionada = null;
                btBuscarBitacora.setEnabled(true);
                etBitacora.setText("");
                etDigitosBitacora.setText("");
                tvBitacoraEncontrada.setText("");
                cbSolicitudDesglose.setChecked(false);
                cbSolicitudFactura.setChecked(false);
                cbAmbas.setChecked(false);
                btManual.setEnabled(true);
                NeumorphCardView btEscanearParaFacturar =(NeumorphCardView) findViewById(R.id.btEscanearParaFacturar);
                btEscanearParaFacturar.setEnabled(true);

                NeumorphButton btGuardarFormulario =(NeumorphButton) findViewById(R.id.btGuardarFormulario);
                btGuardarFormulario.setVisibility(View.VISIBLE);

                imgFirma.setVisibility(View.GONE);

                imgCredencialFrente.setEnabled(true);
                imgCredencialAtras.setEnabled(true);
                imgFirma.setEnabled(true);


                imgCredencialFrente.setImageDrawable(null);
                imgCredencialAtras.setImageDrawable(null);
                imgFirma.setImageDrawable(null);

                imgCredencialFrente.setImageDrawable(getResources().getDrawable(R.drawable.credencial));
                imgCredencialAtras.setImageDrawable(getResources().getDrawable(R.drawable.credencial));

                tvAbrirFirma.setVisibility(View.VISIBLE);


                LinearLayout frameBotones = (LinearLayout) findViewById(R.id.frameBotones);
                frameBotones.setVisibility(View.VISIBLE);


            }
        });

        cbSolicitudFactura.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    cbSolicitudDesglose.setChecked(false);
                    cbAmbas.setChecked(false);
                }
            }
        });


        cbSolicitudDesglose.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    cbSolicitudFactura.setChecked(false);
                    cbAmbas.setChecked(false);
                }
            }
        });

        cbAmbas.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    cbSolicitudFactura.setChecked(false);
                    cbSolicitudDesglose.setChecked(false);
                    cbAmbas.setChecked(true);
                }
            }
        });



        btGuardarFormulario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btGuardarFormulario.setEnabled(false);

                try {

                    if(fotoFrenteTomada && fotoAtrasTomada && firmaTomada){

                        ProgressDialog pDialog = new ProgressDialog(Facturas.this);
                        pDialog.setMessage("Guardando imagenes");
                        pDialog.setCancelable(false);
                        pDialog.show();

                        imgFirma.invalidate();
                        BitmapDrawable drawable = (BitmapDrawable) imgFirma.getDrawable();
                        Bitmap bitmapFirma = drawable.getBitmap();


                        /** ivalidate imgIneFrente **/
                        imgCredencialFrente.invalidate();
                        BitmapDrawable credencialFrenteDrawable = (BitmapDrawable) imgCredencialFrente.getDrawable();
                        Bitmap bitmapCredencialFrente = credencialFrenteDrawable.getBitmap();


                        /** ivalidate imgIneFrente **/
                        imgCredencialAtras.invalidate();
                        BitmapDrawable credencialAtrasDrawable = (BitmapDrawable) imgCredencialAtras.getDrawable();
                        Bitmap bitmapCredencialAtras = credencialAtrasDrawable.getBitmap();


                        JSONObject jsonOpcionesDeFacturacion = new JSONObject();
                        jsonOpcionesDeFacturacion.put("solicitud_factura", cbSolicitudFactura.isChecked() ? "1" : "0");
                        jsonOpcionesDeFacturacion.put("solicitud_carta_desglose", cbSolicitudDesglose.isChecked() ? "1" : "0");
                        jsonOpcionesDeFacturacion.put("ambas", cbAmbas.isChecked() ? "1" : "0");


                        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                        DatabaseAssistant.insertarFotografiasDeINEYFirma(
                                "" + getStringImagen(bitmapCredencialFrente),
                                "" + getStringImagen(bitmapCredencialAtras),
                                "" + getStringImagen(bitmapFirma),
                                "" + dateFormat.format(new Date()),
                                "" + jsonOpcionesDeFacturacion.toString(),
                                bitacoraSeleccionada
                        );

                        guardarImagenCargadaEnCarpetaEbita(bitmapFirma, "firma_" + imageName);
                        guardarImagenCargadaEnCarpetaEbita(bitmapCredencialFrente, "credencial_frente_" + imageName);
                        guardarImagenCargadaEnCarpetaEbita(bitmapCredencialAtras, "credencial_atras_" + imageName);



                        //createRequestOnlineHere

                        /**********Creamos el request*************/

                        JSONObject jsonparams = new JSONObject();
                        jsonparams.put("bitacora", bitacoraSeleccionada);
                        jsonparams.put("ine_frente", getStringImagen(bitmapCredencialFrente));
                        jsonparams.put("ine_atras", getStringImagen(bitmapCredencialAtras));
                        jsonparams.put("firma", getStringImagen(bitmapFirma));
                        jsonparams.put("fecha_captura", dateFormat.format(new Date()));
                        jsonparams.put("tipos_movimiento", jsonOpcionesDeFacturacion.toString());
                        jsonparams.put("usuario", DatabaseAssistant.getUserNameFromSesiones());


                        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_SAVE_FACTURA_DOCUMENTS, jsonparams, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                if (response.has("error")){
                                    try {
                                        ApplicationResourcesProvider.showToastError(Facturas.this, response.getString("error"));
                                        pDialog.dismiss();
                                        pDialog.cancel();
                                        btGuardarFormulario.setEnabled(true);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                else{
                                    LayoutInflater inflater = getLayoutInflater();
                                    View viewAux = inflater.inflate(R.layout.custom_toast_layout, (ViewGroup) findViewById(R.id.relativeLayout1));
                                    LottieAnimationView lottieAnimationView = viewAux.findViewById(R.id.imageView1);
                                    lottieAnimationView.setAnimation("success_toast.json");
                                    lottieAnimationView.loop(false);
                                    lottieAnimationView.playAnimation();

                                    Toast toast = new Toast(getApplicationContext());
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.setDuration(Toast.LENGTH_LONG);
                                    toast.setView(viewAux);
                                    toast.show();

                                    try{
                                        pDialog.dismiss();
                                        pDialog.cancel();
                                    }catch (Throwable e){
                                        Log.e(TAG, "onClick: Error en ProgressDialog" + e.getMessage());
                                    }

                                    finish();
                                }

                            }
                        },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        error.printStackTrace();
                                        errorStackTraceBitacoras = true;
                                        ApplicationResourcesProvider.showToastError(Facturas.this, "Ocurio un error, vuelve a intentarlo mas tarde.");
                                        pDialog.dismiss();
                                        pDialog.cancel();
                                    }
                                }) {

                        };

                        postRequest.setRetryPolicy(new DefaultRetryPolicy(90000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        VolleySingleton.getIntanciaVolley(Facturas.this).addToRequestQueue(postRequest);

                        /*******************************************/



                    }else{
                        ApplicationResourcesProvider.showToastError(Facturas.this, "No se capturó ningúna foto y firma.");
                        btGuardarFormulario.setEnabled(true);
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    btGuardarFormulario.setEnabled(true);
                }



            }
        });



        Spinner spAno = (Spinner) findViewById(R.id.spAno);
        Spinner spMes = (Spinner) findViewById(R.id.spMes);

        String[] anos = {"21", "22", "23", "24", "25", "26", "27", "28", "29", "30"};
        ArrayAdapter<String> adapterAnos = new ArrayAdapter<String>(getApplicationContext().getApplicationContext(), R.layout.style_spinner, anos);
        spAno.setAdapter(adapterAnos);

        String[] meses = {"ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"};
        ArrayAdapter<String> adapterMeses = new ArrayAdapter<String>(getApplicationContext().getApplicationContext(), R.layout.style_spinner, meses);
        spMes.setAdapter(adapterMeses);


        Date date = new Date();
        LocalDate localDate = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            try {
                localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                int month = localDate.getMonthValue();

                int year = localDate.getYear();

                spMes.setSelection(month - 1);


                String anioString = String.valueOf(year).substring(2, 4);
                if (anioString.equals("21"))
                    spAno.setSelection(0);
                else if (anioString.equals("22"))
                    spAno.setSelection(1);
                else if (anioString.equals("23"))
                    spAno.setSelection(2);
                else
                    spAno.setSelection(1);


            } catch (Throwable e) {
                Log.e("TAG", "onCreate: Error en asignacion de meses: " + e.getMessage());
            }
        }

        NeumorphButton btBuscarBitacora = (NeumorphButton) findViewById(R.id.btBuscarBitacora);
        spAno.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View views, int position, long id) {
                anioSeleccionado = parent.getItemAtPosition(position).toString();
                bitacoraSeleccionada = null;
                layoutDatos.setVisibility(View.GONE);
                btBuscarBitacora.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                anioSeleccionado = "";
                bitacoraSeleccionada = null;
                layoutDatos.setVisibility(View.GONE);
                btBuscarBitacora.setEnabled(true);
            }
        });

        spMes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mesSeleccionado = parent.getItemAtPosition(position).toString();
                bitacoraSeleccionada = null;
                layoutDatos.setVisibility(View.GONE);
                btBuscarBitacora.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mesSeleccionado = "";
                bitacoraSeleccionada = null;
                layoutDatos.setVisibility(View.GONE);
                btBuscarBitacora.setEnabled(true);
            }
        });



        btBuscarBitacora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    closeTeclado();
                } catch (Throwable e) {
                    Log.e("TAG", "onClick: ");
                }

                EditText etDigitosBitacora = (EditText) findViewById(R.id.etDigitosBitacora);
                String concatedo = "";

                if (etDigitosBitacora.getText().toString().length() == 1) {
                    Log.d("TAG", "onClick: 00" + etDigitosBitacora.getText());
                    concatedo = concatedo + "00";
                } else if (etDigitosBitacora.getText().toString().length() == 2) {
                    Log.d("TAG", "onClick: 0" + etDigitosBitacora.getText());
                    concatedo = concatedo + "0";
                } else if (etDigitosBitacora.getText().toString().length() >= 3) {
                    Log.d("TAG", "onClick: " + etDigitosBitacora.getText());
                    concatedo = concatedo + "";
                }


                String bitacoraGeneral = "VSA" + anioSeleccionado + mesSeleccionado + concatedo + etDigitosBitacora.getText().toString();
                if (!anioSeleccionado.equals("") && !mesSeleccionado.equals("") && bitacoraGeneral.length() > 8 && !etDigitosBitacora.getText().toString().equals("") && cbSolicitudDesglose.isChecked() || cbSolicitudFactura.isChecked() || cbAmbas.isChecked()) {
                    etBitacora.setText(bitacoraGeneral);
                    bitacoraSeleccionada = etBitacora.getText().toString();

                    try {
                        JSONObject jsonOpcionesDeFacturacion = new JSONObject();
                        jsonOpcionesDeFacturacion.put("solicitud_factura", cbSolicitudFactura.isChecked() ? "1" : "0");
                        jsonOpcionesDeFacturacion.put("solicitud_carta_desglose", cbSolicitudDesglose.isChecked() ? "1" : "0");
                        jsonOpcionesDeFacturacion.put("ambas", cbAmbas.isChecked() ? "1" : "0");
                        searchBitacoraInWeb(etBitacora.getText().toString().toUpperCase(), jsonOpcionesDeFacturacion.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    ApplicationResourcesProvider.showToastAdvertencia(Facturas.this, "Verifica los datos de la bitácora así como el tipo de trámite a realizar.");
                }
            }
        });



    }//end onCreate

    AutoCompleteTextView etBitacora;
    private static String MEDIA_DIRECTORY = "EBITA_IMAGES";
    String imageName;
    Bitmap bitmap;
    private String mPath;
    private final int PHOTO_CODE = 200;
    int PICK_IMAGE_REQUEST = 1;
    private final int SELECT_PICTURE = 300;

    private void guardarImagenCargadaEnCarpetaEbita(Bitmap bitmap, @NonNull String name) throws IOException {
        boolean saved;
        OutputStream fos;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ContentResolver resolver = getContentResolver();
            ContentValues contentValues = new ContentValues();
            contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name);
            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/png");
            contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, "DCIM/" + MEDIA_DIRECTORY);
            Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
            fos = resolver.openOutputStream(imageUri);
        } else {
            String imagesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + MEDIA_DIRECTORY + File.separator + imageName;

            File file = new File(imagesDir);

            if (!file.exists()) {
                file.mkdirs();
            }

            File image = new File(imagesDir, name + ".jpg");
            try
            {
                image.createNewFile();
            }
            catch (IOException e)
            {
                Log.e("Error: ",  e.getMessage());
            }
            fos = new FileOutputStream(image);
        }

        try {
            saved = bitmap.compress(Bitmap.CompressFormat.JPEG, 10, fos);
        }catch (Throwable e){
            Log.e("TAG", "guardarImagenCargadaEnCarpetaEbita: Error al guaedar la imagen" + e.getMessage());
        }

        fos.flush();
        fos.close();
    }

    private void closeTeclado() {
        try {
            View view = this.getCurrentFocus();
            view.clearFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Throwable e) {
            Log.e("TAG", "closeTeclado: Error en cerrar teclado" + e.getMessage());
        }

    }






































    private void searchBitacoraInWeb(String bitacora, String jsonOpcionesTramites) {

        if (ApplicationResourcesProvider.checkInternetConnection())
        {
            showMyCustomDialog();
            Thread thread = new Thread() {
                @Override
                public void run()
                {
                    try
                    {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                /**Muestra el dialogo de carga loading**/



                                /*********Creamos los parametros del WS de getSingleBitacora*********/
                                JSONObject params = new JSONObject();
                                try {
                                    params.put("usuario", DatabaseAssistant.getUserNameFromSesiones());
                                    params.put("token_device", DatabaseAssistant.getTokenDeUsuario());
                                    params.put("isProveedor", DatabaseAssistant.getIsProveedor());
                                    params.put("isBunker", Preferences.getPreferenceIsbunker(getApplicationContext(), Preferences.PREFERENCE_ISBUNKER) ? "1" : "0");
                                    params.put("bitacora", bitacora);
                                    params.put("tramite_a_realizar", jsonOpcionesTramites);
                                    params.put("isFactura", "1");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                /**********Creamos el request*************/
                                JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_DOWNLOAD_BITACORA_INDIVIDUAL, params, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {


                                        try {


                                            if(!response.getBoolean("error")){
                                                if(response.has("result")){

                                                    try {
                                                        if (response.has("bitacora")) {
                                                            try {
                                                                errorStackTraceBitacoras = false;
                                                                bitacoraSeleccionada = response.getString("bitacora");
                                                                Toast.makeText(getApplicationContext(), "Encontrada correctamente", Toast.LENGTH_SHORT).show();
                                                                layoutDatos.setVisibility(View.VISIBLE);
                                                                LinearLayout layoutManual = (LinearLayout) findViewById(R.id.layoutManual);
                                                                layoutManual.setVisibility(View.GONE);
                                                                NeumorphButton btBuscarBitacora = (NeumorphButton) findViewById(R.id.btBuscarBitacora);
                                                                btBuscarBitacora.setEnabled(false);
                                                                NeumorphCardView btManual =(NeumorphCardView) findViewById(R.id.btManual);
                                                                btManual.setEnabled(false);

                                                                TextView tvBitacoraEncontrada = (TextView) findViewById(R.id.tvBitacoraEncontrada);
                                                                tvBitacoraEncontrada.setText(bitacoraSeleccionada);
                                                                errorStackTraceBitacoras = false;

                                                            } catch (Throwable e) {
                                                                errorStackTraceBitacoras = true;
                                                                ApplicationResourcesProvider.showToastError(Facturas.this, "Ocurrio un error en la búsqueda de bitácora: " + e.getMessage());
                                                                Log.e("TAG", "onResponse: Error al consultar y guardar la bitacora individual: " + e.getMessage());
                                                            }
                                                        }

                                                        dismissMyCustomDialog();

                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                        errorStackTraceBitacoras = true;
                                                        ApplicationResourcesProvider.showToastError(Facturas.this, "Ocurrio un error en la búsqueda de bitácora: " + e.getMessage());
                                                    }
                                                }
                                                else{
                                                    //No tiene result, ya tiene informacion capturada, para mostrar los datos y las imagenes, ademas bloqueamos los botones para no guardar
                                                    String bitacoraEncontrada = response.getString("bitacora");
                                                    int tramiteRealizadoEncontrado = response.getInt("tramite_a_realizar");
                                                    String base64_ine_frente = response.getString("ine_frente");
                                                    String base64_ine_atras = response.getString("ine_atras");
                                                    String base64_firma = response.getString("firma");

                                                    LinearLayout layoutManual = (LinearLayout) findViewById(R.id.layoutManual);
                                                    layoutManual.setVisibility(View.GONE);
                                                    layoutDatos.setVisibility(View.VISIBLE);
                                                    NeumorphButton btBuscarBitacora = (NeumorphButton) findViewById(R.id.btBuscarBitacora);
                                                    btBuscarBitacora.setEnabled(false);

                                                    NeumorphCardView btManual =(NeumorphCardView) findViewById(R.id.btManual);
                                                    btManual.setEnabled(false);

                                                    NeumorphCardView btEscanearParaFacturar =(NeumorphCardView) findViewById(R.id.btEscanearParaFacturar);
                                                    btEscanearParaFacturar.setEnabled(false);

                                                    TextView tvBitacoraEncontrada = (TextView) findViewById(R.id.tvBitacoraEncontrada);
                                                    tvBitacoraEncontrada.setText(bitacoraEncontrada);

                                                    NeumorphButton btGuardarFormulario =(NeumorphButton) findViewById(R.id.btGuardarFormulario);
                                                    btGuardarFormulario.setVisibility(View.GONE);

                                                    switch (tramiteRealizadoEncontrado){
                                                        case 1:
                                                            cbSolicitudFactura.setChecked(true);
                                                            cbSolicitudDesglose.setChecked(false);
                                                            break;
                                                        case 2:
                                                            cbSolicitudDesglose.setChecked(true);
                                                            cbSolicitudFactura.setChecked(false);
                                                            break;
                                                    }

                                                    imgCredencialFrente.setImageBitmap(getBitmapDesdeBase64(base64_ine_frente));
                                                    imgCredencialAtras.setImageBitmap(getBitmapDesdeBase64(base64_ine_atras));
                                                    imgFirma.setVisibility(View.VISIBLE);
                                                    imgCredencialFrente.setEnabled(false);
                                                    imgCredencialAtras.setEnabled(false);
                                                    imgFirma.setEnabled(false);
                                                    imgFirma.setImageBitmap(getBitmapDesdeBase64(base64_firma));

                                                    TextView tvAbrirFirma = (TextView) findViewById(R.id.tvAbrirFirma);
                                                    tvAbrirFirma.setVisibility(View.GONE);

                                                    LinearLayout frameBotones = (LinearLayout) findViewById(R.id.frameBotones);
                                                    frameBotones.setVisibility(View.GONE);

                                                    dismissMyCustomDialog();
                                                }
                                            }
                                            else{
                                                dismissMyCustomDialog();
                                                if (response.has("error_message")){
                                                    ApplicationResourcesProvider.showToastError(Facturas.this, response.getString("error_message"));
                                                }
                                                Log.d("error", response.toString());
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            dismissMyCustomDialog();
                                            ApplicationResourcesProvider.showToastError(Facturas.this, "Ocurrio un error en la búsqueda de bitácora: " + e.getMessage());
                                        }

                                    }
                                },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                error.printStackTrace();
                                                dismissMyCustomDialog();
                                                errorStackTraceBitacoras = true;
                                                ApplicationResourcesProvider.showToastError(Facturas.this, "Ocurrio un error en la búsqueda de bitácora: " + error.getMessage());
                                            }
                                        }) {

                                };

                                postRequest.setRetryPolicy(new DefaultRetryPolicy(90000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                VolleySingleton.getIntanciaVolley(Facturas.this).addToRequestQueue(postRequest);


                                /*******************************************/
                            }
                        });


                    } catch (Throwable e) {
                        e.printStackTrace();
                        dismissMyCustomDialog();
                        ApplicationResourcesProvider.showToastError(Facturas.this, "Ocurrio un error en la búsqueda de bitácora: " + e.getMessage());
                    }
                };
            };
            thread.start();



        } else
            showErrorDialog("No hay conexión a internet");

    }

    private Bitmap getBitmapDesdeBase64(String base64){
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }


    boolean errorStackTraceBitacoras = false;

    private void showMyCustomDialog() {
        final FrameLayout flLoading = (FrameLayout) findViewById(R.id.layoutCargando);
        flLoading.setVisibility(View.VISIBLE);
    }

    private void dismissMyCustomDialog() {
        final FrameLayout flLoading = (FrameLayout) findViewById(R.id.layoutCargando);
        flLoading.setVisibility(View.GONE);
    }
    public void showErrorDialog(final String codeError) {
        final NeumorphButton btNo, btSi;
        TextView tvCodeError, tvBitacora;
        Dialog dialogoError = new Dialog(Facturas.this);
        dialogoError.setContentView(R.layout.layout_error);
        dialogoError.setCancelable(true);
        btNo = (NeumorphButton) dialogoError.findViewById(R.id.btNo);
        btSi = (NeumorphButton) dialogoError.findViewById(R.id.btSi);
        tvCodeError = (TextView) dialogoError.findViewById(R.id.tvCodeError);
        tvBitacora = (TextView) dialogoError.findViewById(R.id.tvBitacora);
        tvCodeError.setText(codeError);

        if (codeError.equals("No se puede crear la bitácora por que la zona dónde estás, no corresponde a tu lugar seleccionado.") ||
                codeError.equals("No podemos crear la bitacora porque el destino debe ser diferente origen")) {
            btNo.setVisibility(View.GONE);
            btSi.setVisibility(View.VISIBLE);
        }

        btNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogoError.dismiss();
            }
        });

        btSi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogoError.dismiss();
            }
        });

        dialogoError.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogoError.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK) {
            switch (requestCode) {
                case PHOTO_CODE:
                    Bitmap imageBitmap = BitmapFactory.decodeFile(mPath);

                    Bitmap rotatedBitmap = null;
                    try {
                        ExifInterface ei = new ExifInterface(mPath);
                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

                        switch (orientation) {

                            case ExifInterface.ORIENTATION_ROTATE_90:
                                rotatedBitmap = rotateImage(imageBitmap, 90);
                                break;

                            case ExifInterface.ORIENTATION_ROTATE_180:
                                rotatedBitmap = rotateImage(imageBitmap, 180);
                                break;

                            case ExifInterface.ORIENTATION_ROTATE_270:
                                rotatedBitmap = rotateImage(imageBitmap, 270);
                                break;

                            case ExifInterface.ORIENTATION_NORMAL:
                            default:
                                rotatedBitmap = imageBitmap;
                        }

                        if(isFrontINE) {
                            imgCredencialFrente.setImageBitmap(rotatedBitmap);
                            fotoFrenteTomada = true;
                        }
                        else {
                            imgCredencialAtras.setImageBitmap(rotatedBitmap);
                            fotoAtrasTomada = true;
                        }
                        //Delete here photo
                        File fdelete = new File(mPath);
                        if (fdelete.exists()) {
                            if (fdelete.delete()) {
                                Log.v(TAG, "Archivo borrado correctamente: " + mPath);
                            } else {
                                Log.v(TAG, "El archivo no fue borrado: " + mPath);
                            }
                        }
                        break;

                    } catch (IOException e) {
                        if(isFrontINE) {
                            fotoFrenteTomada = true;
                            imgCredencialFrente.setImageBitmap(imageBitmap);
                        }
                        else {
                            fotoAtrasTomada = true;
                            imgCredencialAtras.setImageBitmap(imageBitmap);
                        }
                        e.printStackTrace();
                    }

            }
        }



        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() != null) {


                if(ApplicationResourcesProvider.checkInternetConnection()) {

                    try {
                        if (result.getContents().contains("VSA") && (result.getContents().toString().length() == 11 ||  result.getContents().toString().length() == 12))
                        {
                            //etBitacora.setText(result.getContents().toString());
                            if (!result.getContents().equals("") && result.getContents().length() > 8) {
                                openDialogForRequestBinnacle(result.getContents());
                            } else {
                                Toast.makeText(getApplicationContext(), "Por favor verificar la bitácora a buscar.", Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            ApplicationResourcesProvider.showToastAdvertencia(Facturas.this ,"Parece que el código es incorrecto");
                        }

                    } catch (Exception e) {
                        Log.d("TIMER", "getStatusBinaccle: error en creacion de json para status de bitacora");
                        Toast.makeText(this, "Ocurrio un error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else
                    ApplicationResourcesProvider.showToastSinConexion(Facturas.this,"Necesitas conexión a internet para escanear la bitácora");
            }
        }


    }

    private boolean fotoFrenteTomada = false, fotoAtrasTomada = false, firmaTomada = false;


    public String getStringImagen(Bitmap bmp) {
        String encodedImage="";
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 10, baos);
            byte[] imageBytes = baos.toByteArray();
            encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
            return encodedImage;
        }catch (Throwable e){
            Log.e("TAG", "getStringImagen: " +  e.getMessage());
            Toast.makeText(this, "Coloca el dispositivo de forma vertical para guardar imagenes.", Toast.LENGTH_LONG).show();
        }
        return encodedImage;
    }


    private void openCamera(String bitacoraEncontrada)
    {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), MEDIA_DIRECTORY);
        boolean isDirectoryCreated = file.exists();

        if(!isDirectoryCreated)
            isDirectoryCreated = file.mkdirs();


        if(isDirectoryCreated) {
            Long timestamp = System.currentTimeMillis() / 1000;
            imageName =  DatabaseAssistant.getUserNameFromSesiones() +"_"+ bitacoraEncontrada + "_" + timestamp.toString() + ".jpg";

            mPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + MEDIA_DIRECTORY + File.separator + imageName;
            File newFile = new File(mPath);

            try {
                newFile.createNewFile();
            }
            catch (IOException e) {
                Log.v("CAMARA", "Error al abrir camara: " + e.getMessage());
            }

            try {
                Log.d("CAMARA", "onActivityResult:");
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", newFile));
                startActivityForResult(intent, PHOTO_CODE);
            }catch (Throwable e){
                Log.e("asdad", "openCamera: dsadadasdasdasd" );
            }
        }
    }



    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    @Override
    public void onResume() { super.onResume();
        Log.d("onRESUME", "onResume: Entro a onResume()");
    }


    public void openDialogSignature() {
        Dialog dialogFirma = new Dialog(Facturas.this);
        dialogFirma.setContentView(R.layout.fragment_facturas);
        dialogFirma.setCancelable(false);



        SignatureView signatureView = (SignatureView) dialogFirma.findViewById(R.id.signature_view);
        TextView btLimpiarFirma = (TextView) dialogFirma.findViewById(R.id.btLimpiarFirma);
        TextView btGuardarFirma = (TextView) dialogFirma.findViewById(R.id.btGuardarFirma);
        ImageView imgClose = (ImageView) dialogFirma.findViewById(R.id.imgClose);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firmaTomada = false;
                try {
                    dialogFirma.dismiss();
                    dialogFirma.cancel();
                }catch (Throwable w){
                    Log.e("TAG", "onClick: asdasdassadasd" );
                }
            }
        });

        btLimpiarFirma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signatureView.clearCanvas();
                firmaTomada = false;
            }
        });


        btGuardarFirma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(signatureView.isBitmapEmpty()) {
                    Toast.makeText(Facturas.this, "Necesitas la firma para guardar", Toast.LENGTH_SHORT).show();
                }else {
                    imgFirma.setVisibility(View.VISIBLE);
                    imgFirma.setImageBitmap(signatureView.getSignatureBitmap());
                    firmaTomada = true;
                    try {
                        dialogFirma.dismiss();
                        dialogFirma.cancel();
                    } catch (Throwable e) {
                        Log.e("TAG", "onClick: asdasdassadasd");
                    }
                }
            }
        });

        dialogFirma.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogFirma.show();

    }


    public void openDialogForRequestBinnacle(String bitacora)
    {
        BottomSheetDialog dialogSolicitudes = new BottomSheetDialog(Facturas.this);
        dialogSolicitudes.setContentView(R.layout.layout_request_tipo_solicitud);
        dialogSolicitudes.setCancelable(false);
        dialogSolicitudes.setCanceledOnTouchOutside(false);

        NeumorphButton btConsultar = (NeumorphButton) dialogSolicitudes.findViewById(R.id.btConsultar);
        CheckBox cbSolicitudFacturaDialog = (CheckBox) dialogSolicitudes.findViewById(R.id.cbSolicitudFactura);
        CheckBox cbSolicitudDesgloseDialog = (CheckBox) dialogSolicitudes.findViewById(R.id.cbSolicitudDesglose);
        CheckBox cbAmbas = (CheckBox) dialogSolicitudes.findViewById(R.id.cbAmbas);

        TextView btCerrar = (TextView) dialogSolicitudes.findViewById(R.id.btCerrar);
        TextView tvBitacora = (TextView) dialogSolicitudes.findViewById(R.id.tvBitacora);


        tvBitacora.setText(bitacora);


        cbSolicitudFacturaDialog.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    cbSolicitudFactura.setChecked(true);
                    cbSolicitudDesglose.setChecked(false);
                    cbSolicitudDesgloseDialog.setChecked(false);
                    cbAmbas.setChecked(false);
                }
            }
        });


        cbSolicitudDesgloseDialog.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    cbSolicitudFactura.setChecked(false);
                    cbSolicitudDesglose.setChecked(true);
                    cbSolicitudFacturaDialog.setChecked(false);
                }
            }
        });

        cbAmbas.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    cbSolicitudFacturaDialog.setChecked(false);
                    cbSolicitudDesgloseDialog.setChecked(false);
                    cbAmbas.setChecked(true);
                }
            }
        });


        btCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    dialogSolicitudes.dismiss();
                    dialogSolicitudes.cancel();
                }catch (Throwable e){
                    Log.e(TAG, "onClick: Error: " + e.getMessage());
                }
            }
        });


        btConsultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cbSolicitudDesglose.isChecked() || cbSolicitudFactura.isChecked() || cbAmbas.isChecked()) {

                    try {
                        JSONObject jsonOpcionesDeFacturacion = new JSONObject();
                        jsonOpcionesDeFacturacion.put("solicitud_factura", cbSolicitudFactura.isChecked() ? "1" : "0");
                        jsonOpcionesDeFacturacion.put("solicitud_carta_desglose", cbSolicitudDesglose.isChecked() ? "1" : "0");
                        jsonOpcionesDeFacturacion.put("ambas", cbAmbas.isChecked() ? "1" : "0");
                        searchBitacoraInWeb(bitacora.toUpperCase(), jsonOpcionesDeFacturacion.toString());
                        try{
                            dialogSolicitudes.dismiss();
                            dialogSolicitudes.cancel();
                        }catch (Throwable e){
                            Log.e(TAG, "onClick: Error: " + e.getMessage());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    ApplicationResourcesProvider.showToastAdvertencia(Facturas.this, "Verifica los datos de la bitácora así como el tipo de trámite a realizar.");
                }



            }
        });

        dialogSolicitudes.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogSolicitudes.show();
    }



}