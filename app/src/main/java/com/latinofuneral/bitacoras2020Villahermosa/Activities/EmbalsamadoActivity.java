package com.latinofuneral.bitacoras2020Villahermosa.Activities;

import static com.latinofuneral.bitacoras2020Villahermosa.Utils.ApplicationResourcesProvider.getContext;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputLayout;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.latinofuneral.bitacoras2020Villahermosa.Adapters.AdapterEmbalsamado;
import com.latinofuneral.bitacoras2020Villahermosa.Database.DatabaseAssistant;
import com.latinofuneral.bitacoras2020Villahermosa.Database.EmbalsamadoEvents;
import com.latinofuneral.bitacoras2020Villahermosa.R;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.ApplicationResourcesProvider;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.ConstantsBitacoras;
import com.latinofuneral.bitacoras2020Villahermosa.Utils.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import soup.neumorphism.NeumorphButton;
import soup.neumorphism.NeumorphFloatingActionButton;

public class EmbalsamadoActivity extends AppCompatActivity {

        boolean eventEmb = false;
        String TAG = "EmbalsamadoActivity";
        RecyclerView rvEmb;
        NeumorphFloatingActionButton fab;
        AdapterEmbalsamado adapterEmbalsamado = null;

        @Override
        protected void onCreate (Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_embalsamado);

            initialize();
            functions();

            ImageView btBack = (ImageView) findViewById(R.id.btBack);
            btBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

        }

        public void initialize () {
            rvEmb = findViewById(R.id.rvEmb);
            fab = findViewById(R.id.fab);

            GridLayoutManager gridLayoutManager;
            rvEmb.setHasFixedSize(true);
            gridLayoutManager = new GridLayoutManager(this, 1);
            rvEmb.setLayoutManager(gridLayoutManager);
            chargeList();
        }

        public void chargeList () {
            List<EmbalsamadoEvents> embEvents = EmbalsamadoEvents.findWithQuery(EmbalsamadoEvents.class, "SELECT * FROM EMBALSAMADO_EVENTS ORDER BY fechaevento DESC");
            if (embEvents.size() > 0) {
                rvEmb.setVisibility(View.VISIBLE);

                adapterEmbalsamado = new AdapterEmbalsamado(
                        EmbalsamadoActivity.this,
                        embEvents);
                rvEmb.setAdapter(adapterEmbalsamado);
            }
        }

        public void functions () {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ApplicationResourcesProvider.checkInternetConnection()) {
                        eventEmb = true;
                        new IntentIntegrator(EmbalsamadoActivity.this).initiateScan();
                    } else
                        showErrorDialog("No hay conexión a internet");
                }
            });
        }
        public void getEmbSaved () {
        }

        private void showMyCustomDialog () {
            final FrameLayout flLoading = (FrameLayout) findViewById(R.id.layoutCargando);
            flLoading.setVisibility(View.VISIBLE);
        }

        private void dismissMyCustomDialog () {
            final FrameLayout flLoading = (FrameLayout) findViewById(R.id.layoutCargando);
            flLoading.setVisibility(View.GONE);
        }


        public void showErrorDialog ( final String codeError){
            final NeumorphButton btNo, btSi;
            TextView tvCodeError;
            TextInputLayout etDescripcionPeticion2;
            Dialog dialogoError = new Dialog(EmbalsamadoActivity.this);
            dialogoError.setContentView(R.layout.layout_error);
            dialogoError.setCancelable(true);
            etDescripcionPeticion2 = (TextInputLayout) dialogoError.findViewById(R.id.etDescripcionPeticion2);
            btNo = (NeumorphButton) dialogoError.findViewById(R.id.btNo);
            btSi = (NeumorphButton) dialogoError.findViewById(R.id.btSi);
            tvCodeError = (TextView) dialogoError.findViewById(R.id.tvCodeError);
            tvCodeError.setText(codeError);
            etDescripcionPeticion2.setVisibility(View.GONE);
            btNo.setVisibility(View.GONE);

            btSi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogoError.dismiss();
                }
            });


            dialogoError.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialogoError.show();

        }

    public void showBottomSaveEventEmb(String bitacora, String siguienteEventoID, String fecha, String usuario, String reciveCenizas, String sync, String siguienteEvento, String eventoAnterior, String finado)
    {
        try {
            Button btCancelar, btGuardar;
            TextView tvEvent, tvFinado, tvBitacora, tvUsuario;
            BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(EmbalsamadoActivity.this);
            mBottomSheetDialog.setContentView(R.layout.bottom_layout_confirmar_evento);
            mBottomSheetDialog.setCancelable(false);
            mBottomSheetDialog.setCanceledOnTouchOutside(false);
            tvEvent = (TextView) mBottomSheetDialog.findViewById(R.id.tvEvent);
            btCancelar = (Button) mBottomSheetDialog.findViewById(R.id.btCancelar);
            btGuardar = (Button) mBottomSheetDialog.findViewById(R.id.btGuardar);

            tvFinado = (TextView) mBottomSheetDialog.findViewById(R.id.tvNombreFinado);
            tvUsuario = (TextView) mBottomSheetDialog.findViewById(R.id.tvUsuario);
            tvBitacora = (TextView) mBottomSheetDialog.findViewById(R.id.tvBitacora);

            tvUsuario.setText(usuario);

            tvBitacora.setText(bitacora);

            tvFinado.setText(finado);


            tvEvent.setText(siguienteEvento);
            btCancelar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mBottomSheetDialog.dismiss();
                }
            });
            btGuardar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        JSONObject embalsamado_eventos = new JSONObject();
                        JSONArray embalsamadosArray = new JSONArray();
                        JSONObject jsonEventoEmbalsamado = new JSONObject();
                        jsonEventoEmbalsamado.put("bitacora", bitacora);
                        jsonEventoEmbalsamado.put("tipo_evento", siguienteEventoID);
                        jsonEventoEmbalsamado.put("nombre_evento", siguienteEvento);
                        jsonEventoEmbalsamado.put("fecha_evento", fecha);
                        jsonEventoEmbalsamado.put("usuario", usuario);
                        jsonEventoEmbalsamado.put("quien_recibe_cenizas", "");
                        embalsamadosArray.put(jsonEventoEmbalsamado);
                        embalsamado_eventos.put("recoleccion_proveedor_eventos", embalsamadosArray);

                        cargaDeEventosDeRecoleccion(embalsamado_eventos);

                    }catch (JSONException e){

                    }
                    mBottomSheetDialog.dismiss();
                }
            });
            mBottomSheetDialog.show();
        } catch (Throwable e) {
            Log.e(TAG, "Error en showDownloadsPDF(): " + e.toString());
        }
    }


        public void cargaDeEventosDeRecoleccion (JSONObject jsonParams)
        {
            JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_STORE_RECOLECCION_AND_PROVEEDOR_EVENT_URL,
                    jsonParams, new Response.Listener<JSONObject>() {
                @SuppressLint("LongLogTag")
                @Override
                public void onResponse(JSONObject response)
                {

                    try {
                        JSONObject jsonRespuesta = response.getJSONObject("success");
                        //JSONObject jsonFail = response.getJSONObject("fail");
                        try {
                            if (jsonRespuesta.has("recoleccion_proveedor_eventos"))
                            {

                                JSONArray jsonEmbEventos = jsonRespuesta.getJSONArray("recoleccion_proveedor_eventos");
                                if(jsonEmbEventos.length()>0){
                                    Toast.makeText(getContext(), getContext().getString(R.string.emb_sync), Toast.LENGTH_SHORT).show();
                                    for(int i =0; i<= jsonEmbEventos.length()-1; i++){
                                        if (DatabaseAssistant.existEmb(jsonEmbEventos.getJSONObject(i))){

                                            String query = "UPDATE EMBALSAMADO_EVENTS SET sync = '1', sync = '"+ jsonEmbEventos.getJSONObject(i).getString("nombre_evento")+"' WHERE bitacora = '"
                                                    + jsonEmbEventos.getJSONObject(i).getString("bitacora") + "' and fechaevento = '"
                                                    + jsonEmbEventos.getJSONObject(i).getString("fecha_evento") +"'";
                                            EmbalsamadoEvents.executeQuery(query);
                                            Log.v(TAG, "Evento de embalsamado sincronizado correctamente");


                                        }
                                        else{
                                            Log.d(TAG, "no existe");

                                            DatabaseAssistant.insertarEmbalsamadoEventos(
                                                    "" + jsonEmbEventos.getJSONObject(i).getString("bitacora"),
                                                    "" + jsonEmbEventos.getJSONObject(i).getString("tipo_evento"),
                                                    "" + jsonEmbEventos.getJSONObject(i).getString("nombre_evento"),
                                                    "" + jsonEmbEventos.getJSONObject(i).getString("fecha_evento"),
                                                    "" + jsonEmbEventos.getJSONObject(i).getString("usuario"),
                                                    "" ,
                                                    "" + "1"
                                            );

                                        }

                                    }

                                }
                                else{
                                    JSONObject jsonRespuestaError = response.getJSONObject("fail");
                                    if (jsonRespuestaError.has("recoleccion_proveedor_eventos")){
                                        if (jsonRespuestaError.getJSONArray("recoleccion_proveedor_eventos").length()>0){
                                            if (jsonRespuestaError.getJSONArray("recoleccion_proveedor_eventos").getJSONObject(0).has("error"))
                                                Toast.makeText(getApplicationContext(), jsonRespuestaError.getJSONArray("recoleccion_proveedor_eventos").getJSONObject(0).getString("error"), Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                }

                            } else
                                Log.w(TAG, "Carga de datos, No se registro");

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getContext(), "Ocurrio un error STORE_RECOLECCION_EVENT: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }catch (Throwable e){

                        try {
                            DatabaseAssistant.insertarEmbalsamadoEventos(
                                    "" + jsonParams.getJSONArray("recoleccion_proveedor_eventos").getJSONObject(0).getString("bitacora"),
                                    "" + jsonParams.getJSONArray("recoleccion_proveedor_eventos").getJSONObject(0).getString("tipo_evento"),
                                    "" + jsonParams.getJSONArray("recoleccion_proveedor_eventos").getJSONObject(0).getString("nombre_evento"),
                                    "" + jsonParams.getJSONArray("recoleccion_proveedor_eventos").getJSONObject(0).getString("fecha_evento"),
                                    "" + jsonParams.getJSONArray("recoleccion_proveedor_eventos").getJSONObject(0).getString("usuario"),
                                    "" ,
                                    "" + "0"
                            );
                        }
                        catch (JSONException e1){

                        }

                        Log.e(TAG, "onResponse: Ocurrio un error al sincronizar eventos de RECOLECCION: " + e.getMessage());
                        Toast.makeText(getContext(), "Ocurrio un error al sincronizar STORE_RECOLECCION_EVENT:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    chargeList();
                }
            },
                    new Response.ErrorListener() {
                        @SuppressLint("LongLogTag")
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            Log.e(TAG, "onResponse: Ocurrio un error RecoleccionAndSupplierEvent: " + error.getMessage());
                            try {

                                DatabaseAssistant.insertarEmbalsamadoEventos(
                                        "" + jsonParams.getJSONArray("recoleccion_proveedor_eventos").getJSONObject(0).getString("bitacora"),
                                        "" + jsonParams.getJSONArray("recoleccion_proveedor_eventos").getJSONObject(0).getString("tipo_evento"),
                                        "" + jsonParams.getJSONArray("recoleccion_proveedor_eventos").getJSONObject(0).getString("nombre_evento"),
                                        "" + jsonParams.getJSONArray("recoleccion_proveedor_eventos").getJSONObject(0).getString("fecha_evento"),
                                        "" + jsonParams.getJSONArray("recoleccion_proveedor_eventos").getJSONObject(0).getString("usuario"),
                                        "" ,
                                        "" + "0"
                                );
                            }
                            catch (JSONException e1){
                                Log.d(TAG, e1.getMessage().toString());
                            }
                            chargeList();
                        }
                    });
            postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getIntanciaVolley(getContext()).addToRequestQueue(postRequest);
        }


        @Override
        protected void onActivityResult ( int requestCode, int resultCode, Intent data){
            super.onActivityResult(requestCode, resultCode, data);

            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result != null && resultCode == RESULT_OK) {

                if (eventEmb) {
                    if (ApplicationResourcesProvider.checkInternetConnection()) {
                        try {
                            if (result.getContents().contains("VSA") && (result.getContents().toString().length() == 11 || result.getContents().toString().length() == 12)) {

                                showMyCustomDialog();

                                JSONObject parametrosParaObtenerLosEventos = new JSONObject();
                                parametrosParaObtenerLosEventos.put("bitacora", result.getContents());
                                parametrosParaObtenerLosEventos.put("usuario", DatabaseAssistant.getUserNameFromSesiones());

                                JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST,
                                        ConstantsBitacoras.WS_GET_LAST_RECOLECCION_AND_PROVEEDOR_EVENT, parametrosParaObtenerLosEventos, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {

                                        try {
                                            if (response.has("error")) {
                                                Log.d(TAG, "onResponse: Ocurrio un error en la respuesta del WS de GetLAstRecoleccion: " + response.getString("error_message"));
                                                if(response.has("error_message"))
                                                    Toast.makeText(getApplicationContext(), response.getString("error_message"), Toast.LENGTH_SHORT).show();
                                                else
                                                    Toast.makeText(getApplicationContext(), "Parece que el la bitácora es incorrecta", Toast.LENGTH_SHORT).show();
                                                dismissMyCustomDialog();
                                            } else {
                                                if (response.has("result")) {

                                                    /**Validar primero si result tiene datos**/
                                                    if (response.getJSONObject("result").length() > 0) {
                                                        JSONObject eventoJson = response.getJSONObject("result");

                                                        try {

                                                            /** Guardar aqui los camposs del nuevo proceso de eventos de recoleccion **/
                                                            /** Validar primero el "Fin de embalsamado y maquillaje" para saber si seguimos con la validacion o no, Si el WS no responde nada o vacio, vamos a crear el evento 1 que es entrada a embalsamado
                                                             * y mostrar el siguiente evento a realizar....**/

                                                            int finalizado = eventoJson.getInt("finalizado");

                                                            if (finalizado == 1) {
                                                                /** Terminar con las validaciones y no generar nuevos eventos **/
                                                                showErrorDialog(getString(R.string.emb_event_finish));

                                                            } else if (finalizado == 0) {
                                                                /** consultar los eventos previos para incrementar el tipo_evento
                                                                 * en este punto ya tiene guardado el tipo_evento 1 y guardaremos tipo_evento  + 1  **/

                                                                DatabaseAssistant.deleteFinadosFromBitacora("" + eventoJson.getString("bitacora"));
                                                                DatabaseAssistant.insertarFinado(
                                                                        "" + eventoJson.getString("finado"),
                                                                        "" + eventoJson.getString("bitacora")
                                                                );

                                                                @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                                showBottomSaveEventEmb(
                                                                        "" + eventoJson.getString("bitacora"),
                                                                        "" + eventoJson.getString("siguiente_evento_id"),
                                                                        "" + dateFormat.format(new Date()),
                                                                        "" + DatabaseAssistant.getUserNameFromSesiones(),
                                                                        "",
                                                                        "0",
                                                                        "" + eventoJson.getString("siguiente_evento"),
                                                                        "" + eventoJson.getString("ultimo_evento"),
                                                                        "" + eventoJson.getString("finado")
                                                                );
                                                                dismissMyCustomDialog();
                                                            }


                                                            dismissMyCustomDialog();
                                                        } catch (Throwable e) {
                                                            Log.e(TAG, "onActivityResult: Ocurrio un error" + e.getMessage());
                                                            dismissMyCustomDialog();
                                                            showErrorDialog("Ocurrio un error: " + e.getMessage());
                                                        }
                                                    } else if (response.getJSONObject("result").length() == 0) {
                                                        String finado = "Sin nombre de finado";
                                                        if (response.has("finado")){
                                                            if (!response.getString("finado").equals("")){
                                                                finado = response.getString("finado");
                                                            }
                                                        }

                                                        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                        showBottomSaveEventEmb(
                                                                "" + result.getContents(),
                                                                "1",
                                                                "" + dateFormat.format(new Date()),
                                                                "" + DatabaseAssistant.getUserNameFromSesiones(),
                                                                "",
                                                                "0",
                                                                getString(R.string.pickup_body),
                                                                "",
                                                                "" + finado
                                                        );

                                                        dismissMyCustomDialog();
                                                    } else
                                                        Toast.makeText(EmbalsamadoActivity.this, "Parece que ocurrio un error con la bitácora.", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    Log.d(TAG, "onResponse: No hay datos de la bitacora");
                                                    Toast.makeText(getApplicationContext(), "No se encontraros datos de la bitácora", Toast.LENGTH_SHORT).show();
                                                    dismissMyCustomDialog();
                                                }
                                            }
                                        } catch (JSONException e) {
                                            Log.e(TAG, "onErrorResponse: Error: " + e.getMessage());
                                            e.printStackTrace();
                                            Toast.makeText(getApplicationContext(), "Ocurrio un error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                            dismissMyCustomDialog();
                                        }
                                    }
                                },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                error.printStackTrace();
                                                Log.e(TAG, "onErrorResponse: Error: " + error.getMessage());
                                                dismissMyCustomDialog();
                                                Toast.makeText(getApplicationContext(), "Ocurrio un error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        }) {

                                };
                                postRequest.setRetryPolicy(new DefaultRetryPolicy(90000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);

                            /*
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            showBottomSaveEventEmb(
                                    "" + "bitacora",
                                    "" + "siguiente_evento_id",
                                    "" + dateFormat.format(new Date()),
                                    "" + DatabaseAssistant.getUserNameFromSesiones(),
                                    "",
                                    "0",
                                    "" + result.getContents(),
                                    "" + "ultimo_evento"
                            );
                            */
                            } else {
                                dismissMyCustomDialog();
                                showErrorDialog("Parece que el código es incorrecto");
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Error en creacion de json para status de bitacora");
                            Toast.makeText(this, "Ocurrio un error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else
                        showErrorDialog("No hay conexión a internet");
                    eventEmb = false;
                }
            }
        }


    }
