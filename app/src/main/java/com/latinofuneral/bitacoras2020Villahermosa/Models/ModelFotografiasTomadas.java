package com.latinofuneral.bitacoras2020Villahermosa.Models;

import android.graphics.Bitmap;

public class ModelFotografiasTomadas {
    Bitmap foto;

    public ModelFotografiasTomadas(Bitmap foto) {
        this.foto = foto;
    }

    public Bitmap getFoto() {
        return foto;
    }

    public void setFoto(Bitmap foto) {
        this.foto = foto;
    }
}
